package com.longlinkislong.mvk.tests.ext

import org.joml.Vector3fc
import java.nio.ByteBuffer

internal fun ByteBuffer.putVector3f(vec: Vector3fc) = apply {
    putFloat(vec.x()).putFloat(vec.y()).putFloat(vec.z())
}