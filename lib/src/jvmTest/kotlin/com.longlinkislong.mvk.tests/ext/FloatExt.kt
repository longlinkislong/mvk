package com.longlinkislong.mvk.tests.ext

val Float.Companion.SIZE_BYTES: Int
    get() = java.lang.Float.BYTES

val Float.Companion.SIZE_BITS: Int
    get() = java.lang.Float.SIZE