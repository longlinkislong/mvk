package com.longlinkislong.mvk.tests

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.ext.*
import com.longlinkislong.mvk.tests.ext.SIZE_BYTES
import com.longlinkislong.mvk.tests.ext.putVector3f
import org.joml.Matrix4f
import org.joml.Matrix4fc
import org.joml.Vector3f
import org.joml.Vector3fc
import org.junit.Test
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.system.MemoryUtil
import org.slf4j.LoggerFactory
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.*

class Triangle {
    private data class Vertex(
            val position: Vector3fc,
            val color: Vector3fc) {

        companion object {
            val SIZE_BYTES = 3 * Float.SIZE_BYTES + 3 * Float.SIZE_BYTES
        }
    }

    private fun ByteBuffer.putVertex(vtx: Vertex) = apply {
        this.putVector3f(vtx.position)
        this.putVector3f(vtx.color)
    }

    private val logger = LoggerFactory.getLogger("Triangle")

    private lateinit var projectionMatrix: Matrix4fc
    private lateinit var modelMatrix: Matrix4fc
    private lateinit var viewMatrix: Matrix4fc

    private lateinit var vertices: Buffer
    private lateinit var indices: Buffer
    private lateinit var uniforms: Buffer

    private lateinit var swapchain: Swapchain
    private lateinit var depthStencil: Image
    private lateinit var depthStencilView: ImageView
    private lateinit var renderPass: RenderPass
    private lateinit var descriptorSet: DescriptorSet
    private val framebuffers = ArrayList<Framebuffer>()
    private var isReady = false

    private lateinit var graphicsPipeline: GraphicsPipeline

    private data class Garbage(
            val fence: Fence,
            val buffers: List<Buffer> = emptyList(),
            val images: List<Image> = emptyList(),
            val semaphores: List<Semaphore> = emptyList(),
            val commandBuffers: List<CommandBuffer> = emptyList(),
            val onCollect: (() -> Unit)? = null) {

        fun gc() {
            this.fence.waitFor().reset()

            onCollect?.invoke()

            this.buffers.forEach(Buffer::free)
            this.images.forEach(Image::free)
            this.semaphores.forEach(Semaphore::release)
            this.commandBuffers.forEach(CommandBuffer::release)
        }
    }

    private data class FrameInfo(
            val commandBuffer: CommandBuffer,
            val fence: Fence)

    private val queueFamily: QueueFamily
    private val useStaging = true;
    private val device: Device
    private val surface: Long
    private val window: Long
    private val garbage = LinkedList<Garbage>()
    private lateinit var frameInfos: Array<FrameInfo?>
    private var zoom = -2.5F
    private var lastTime: Double = 0.0
    private var frames = 0
    private var width = 640
    private var height = 480

    init {
        GLFWErrorCallback.createPrint(System.err).set()

        if (!GLFW.glfwInit()) {
            throw RuntimeException("Failed to initialize GLFW!")
        }

        GLFW.glfwDefaultWindowHints()
        GLFW.glfwWindowHint(GLFW.GLFW_CLIENT_API, GLFW.GLFW_NO_API)
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE)

        this.window = GLFW.glfwCreateWindow(width, height, "Triangle Test", 0, 0)

        if (MemoryUtil.NULL == this.window) {
            throw RuntimeException("Failed to create GLFW window")
        }

        GLFW.glfwShowWindow(this.window)

        Instance.enabledLayers += LunarGLayer.STANDARD_VALIDATION
        Instance.enableRequiredGLFWExtensions()

        val instance = Instance.current

        this.surface = instance.createWindowSurface(this.window)

        val physicalDevice = Instance.current.physicalDevices
                .firstOrNull { it.physicalDeviceType == PhysicalDeviceType.DISCRETE_GPU }
                ?: instance.physicalDevices[0]

        this.device = physicalDevice.device {
            this.enabledExtensions += StandardDeviceExtensions.KHR_SWAPCHAIN
        }

        this.queueFamily = this.device.queueFamilies.asSequence()
                .filter { it.info.queueFlags.contains(QueueFlag.GRAPHICS) }
                .filter { it.canPresent(this.surface) }
                .minBy { it.info.queueFlags.size }
                ?: throw UnsupportedOperationException("Vulkan PhysicalDevice: ${device.physicalDevice} does not support a general Queue!")

        this.initVertexBuffers(useStaging)
        this.initUniformBuffers()
        this.initSwapchain()
        this.initPipelines()
        this.initDescriptorSets()

        this.lastTime = GLFW.glfwGetTime()
    }

    private fun garbageCollect() {
        this.garbage.removeIf {
            if (it.fence.isSignaled) {
                it.gc()
                true
            } else false
        }
    }

    private fun initUniformBuffers() {
        this.uniforms = device.buffer {
            this.usage += BufferUsageFlag.UNIFORM_BUFFER
            this.size = 16 * 4 * 3
            this.memoryProperties += StandardMemoryProperties.SYSTEM
        }

        this.updateUniformBuffers()
    }

    private fun initDescriptorSets() {
        this.descriptorSet = this.graphicsPipeline.descriptorSetLayouts[0].allocate().apply {
            writeBuffer(DescriptorType.UNIFORM_BUFFER, 0, this@Triangle.uniforms)
        }
    }

    private fun updateUniformBuffers() {
        val aspectRatio = this.width.toFloat() / this.height.toFloat()

        this.projectionMatrix = Matrix4f().perspective(Math.toRadians(60.0).toFloat(), aspectRatio, 0.1F, 100.0F)
        this.viewMatrix = Matrix4f().translate(0.0F, 0.0F, zoom)
        this.modelMatrix = Matrix4f()

        val map = this.uniforms.map()

        this.projectionMatrix.get(0, map)
        this.viewMatrix.get(16 * Float.SIZE_BYTES, map)
        this.modelMatrix.get(16 * Float.SIZE_BYTES * 2, map)

        this.uniforms.unmap()
    }

    private fun initSwapchain() {
        this.swapchain = device.swapchain {
            this.width = this@Triangle.width
            this.height = this@Triangle.height
            this.surface = this@Triangle.surface
            this.surfaceFormat = SurfaceFormat.DEFAULT
        }

        this.depthStencil = device.image {
            this.extent = Extent3D(this@Triangle.width, this@Triangle.height, 1)
            this.imageType = ImageType.IMAGE_2D
            this.format = device.physicalDevice.preferredDepthStencilFormat
            this.usage += ImageUsageFlag.DEPTH_STENCIL_ATTACHMENT
            this.memoryProperties += StandardMemoryProperties.DEVICE
        }

        this.depthStencilView = ImageView(this.depthStencil)

        this.renderPass = device.renderPass {
            attachment {
                this.format = this@Triangle.swapchain.images[0].info.format
                this.samples = 1
                this.loadOp = LoadOp.CLEAR
                this.storeOp = StoreOp.STORE
                this.layout = ImageLayout.UNDEFINED transitionTo ImageLayout.PRESENT_SRC_KHR
            }

            attachment {
                this.format = this@Triangle.depthStencil.info.format
                this.samples = 1
                this.loadOp = LoadOp.CLEAR
                this.layout = ImageLayout.UNDEFINED transitionTo ImageLayout.DEPTH_STENCIL_ATTACHMENT
            }

            dependency {
                this.subpass = SubpassDependency.SUBPASS_EXTERNAL to 0
                this.stage = PipelineStageFlag.BOTTOM_OF_PIPE until PipelineStageFlag.COLOR_ATTACHMENT_OUTPUT
                this.access = AccessFlag.MEMORY_READ transitionTo StandardAccessFlags.COLOR_ATTACHMENT_READ_WRITE
                this.dependencyFlags += DependencyFlag.BY_REGION
            }

            dependency {
                this.subpass = 0 to SubpassDependency.SUBPASS_EXTERNAL
                this.stage = PipelineStageFlag.COLOR_ATTACHMENT_OUTPUT until PipelineStageFlag.BOTTOM_OF_PIPE
                this.access = StandardAccessFlags.COLOR_ATTACHMENT_READ_WRITE transitionTo AccessFlag.MEMORY_READ
                this.dependencyFlags += DependencyFlag.BY_REGION
            }

            subpass {
                colorAttachment {
                    this.attachment = 0
                    this.layout = ImageLayout.COLOR_ATTACHMENT
                }

                depthStencilAttachment {
                    this.attachment = 1
                    this.layout = ImageLayout.DEPTH_STENCIL_ATTACHMENT
                }
            }
        }

        this.swapchain.images.forEach {
            val imageView = ImageView(it)
            it.userData = imageView

            this.framebuffers += Framebuffer(this.renderPass, FramebufferCreateInfo(this.width, this.height), listOf(imageView, this@Triangle.depthStencilView))
        }

        this.frameInfos = arrayOfNulls(this.swapchain.images.size)
    }

    private fun initPipelines() {
        this.graphicsPipeline = device.graphicsPipeline(this.renderPass) {
            layoutInfo {
                setLayoutInfo {
                    binding(0) {
                        this.descriptorType = DescriptorType.UNIFORM_BUFFER
                        this.stages += ShaderStage.VERTEX
                    }
                }
            }

            inputAssembly(PrimitiveTopology.TRIANGLE_LIST)

            colorBlend {
                attachment { }
            }

            vertexInput {
                binding(0) {
                    this.stride = Vertex.SIZE_BYTES.toInt()
                }

                attribute(0) {
                    this.binding = 0
                    this.format = Format.R32G32B32_SFLOAT
                    this.offset = 0
                }

                attribute(1) {
                    this.binding = 0
                    this.format = Format.R32G32B32_SFLOAT
                    this.offset = 3 * Float.SIZE_BYTES
                }
            }

            val vertexShader = loadShader(Paths.get("data", "shaders", "triangle", "triangle.vert.spv"))
            val fragmentShader = loadShader(Paths.get("data", "shaders", "triangle", "triangle.frag.spv"))

            stage(ShaderStage.VERTEX, vertexShader)
            stage(ShaderStage.FRAGMENT, fragmentShader)
        }
    }

    private fun loadShader(path: Path) : ByteBuffer {
        return FileChannel.open(path, StandardOpenOption.READ).use {
            it.map(FileChannel.MapMode.READ_ONLY, 0, it.size())
        }
    }

    private fun initVertexBuffers(useStaging: Boolean) {
        val vertexBuffer = listOf(
                Vertex(Vector3f(1.0F, 1.0F, 0.0F), Vector3f(1.0F, 0.0F, 0.0F)),
                Vertex(Vector3f(-1.0F, 1.0F, 0.0F), Vector3f(0.0F, 1.0F, 0.0F)),
                Vertex(Vector3f(0.0F, -1.0F, 0.0F), Vector3f(0.0F, 0.0F, 1.0F)))

        val indexBuffer = listOf(0, 1, 2)

        if (useStaging) {
            val stagingBuffers = object {
                lateinit var vertices: Buffer
                lateinit var indices: Buffer
            }

            stagingBuffers.vertices = device.buffer {
                this.usage += BufferUsageFlag.TRANSFER_SRC
                this.size = (vertexBuffer.size * Vertex.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.SYSTEM

                onCreation {
                    val mem = map()
                    vertexBuffer.forEach { mem.putVertex(it) }
                    unmap()
                }
            }

            this.vertices = device.buffer {
                this.usage += StandardBufferUsageFlags.VERTEX_BUFFER_FROM_TRANSFER
                this.size = (vertexBuffer.size * Vertex.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.DEVICE
            }

            stagingBuffers.indices = device.buffer {
                this.usage += BufferUsageFlag.TRANSFER_SRC
                this.size = (indexBuffer.size * Int.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.SYSTEM

                onCreation {
                    val mem = map()
                    indexBuffer.forEach { mem.putInt(it) }
                    unmap()
                }
            }

            this.indices = device.buffer {
                this.usage += StandardBufferUsageFlags.INDEX_BUFFER_FROM_TRANSFER
                this.size = (indexBuffer.size * Int.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.DEVICE
            }

            val cmd = this.queueFamily.currentCommandPool.allocate(CommandBufferLevel.PRIMARY).apply {
                begin(StandardCommandBufferUsageFlags.ONE_TIME_SUBMIT)

                copyBuffer {
                    this.transfer = stagingBuffers.vertices copyTo this@Triangle.vertices
                    this.regions += BufferCopy(size = (vertexBuffer.size * Vertex.SIZE_BYTES).toLong())
                }

                copyBuffer {
                    this.transfer = stagingBuffers.indices copyTo this@Triangle.indices
                    this.regions += BufferCopy(size = (indexBuffer.size * Int.SIZE_BYTES).toLong())
                }

                end()
            }

            val fence = device.acquireFence()

            this.queueFamily.queues[0].submit {
                this.commandBuffers += cmd
                this.fence = fence
            }

            // submit the buffers for garbage collection
            this.garbage += Garbage(
                    buffers = listOf(stagingBuffers.indices, stagingBuffers.vertices),
                    commandBuffers = listOf(cmd),
                    fence = fence,
                    onCollect = { this.isReady = true })
        } else {
            this.vertices = device.buffer {
                this.usage += BufferUsageFlag.VERTEX_BUFFER
                this.size = (vertexBuffer.size * Vertex.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.SYSTEM

                onCreation {
                    val mem = map()
                    vertexBuffer.forEach { mem.putVertex(it) }
                    unmap()
                }
            }

            this.indices = device.buffer {
                this.usage += BufferUsageFlag.INDEX_BUFFER
                this.size = (indexBuffer.size * Int.SIZE_BYTES).toLong()
                this.memoryProperties += StandardMemoryProperties.SYSTEM

                onCreation {
                    val mem = map()
                    indexBuffer.forEach { mem.putInt(it) }
                    unmap()
                }
            }
        }
    }

    private fun draw() {
        garbageCollect()

        if (!this.isReady) {
            logger.info("Waiting on buffer staging...")
            return
        }

        val backbuffer = this.swapchain.acquireNextImage()
        var frameInfo = this.frameInfos[backbuffer.index]

        if (null == frameInfo) {
            frameInfo = FrameInfo(
                    commandBuffer = device.queueFamilies[0].currentCommandPool.allocate(CommandBufferLevel.PRIMARY),
                    fence = device.acquireFence())
            this.frameInfos[backbuffer.index] = frameInfo
        } else {
            frameInfo.fence.waitFor().reset()
        }

        frameInfo.commandBuffer.apply {
            begin(StandardCommandBufferUsageFlags.ONE_TIME_SUBMIT)

            beginRenderPass {
                this.framebuffer = this@Triangle.framebuffers[backbuffer.index]
                this.renderPass = this@Triangle.renderPass
                this.renderArea = Rect2D(Offset2D.ZERO, Extent2D(this@Triangle.width, this@Triangle.height))
                this.clearValues += ClearValue(color = Color(0.0F, 0.0F, 0.2F, 1.0F))
                this.clearValues += ClearValue(depthStencil = DepthStencil(1.0F, 0))
            }

            setScissor(0, 0, this@Triangle.width, this@Triangle.height)
            setViewport(0F, 0F, this@Triangle.width.toFloat(), this@Triangle.height.toFloat())

            bindPipeline(this@Triangle.graphicsPipeline)
            bindDescriptorSet(this@Triangle.graphicsPipeline, 0, this@Triangle.descriptorSet)
            bindVertexBuffer(0, this@Triangle.vertices, 0)
            bindIndexBuffer(this@Triangle.indices, 0, IndexType.UINT32)

            drawIndexed(3, 1, 0, 0, 1)

            endRenderPass()
            end()
        }

        this.queueFamily.queues[0].submit {
            this.commandBuffers += frameInfo.commandBuffer
            this.fence = frameInfo.fence
        }

        this.queueFamily.queues[0].present {
            this.swapchains += this@Triangle.swapchain
            this.imageIndices += backbuffer.index
        }

        frames++

        val now = GLFW.glfwGetTime()
        val dTime = now - lastTime

        if (dTime > 1.0) {
            val fps = frames / dTime

            logger.info("Fps: {}", fps)
            frames = 0
            lastTime = now
        }
    }

    @Test
    fun runTest() {
        while (!GLFW.glfwWindowShouldClose(this.window)) {
            this.draw()
            GLFW.glfwPollEvents()
        }

        this.device.waitIdle()
        this.garbage.forEach {
            it.fence.waitFor()
            it.buffers
        }

        this.queueFamily.detach()

        this.graphicsPipeline.free()

        this.vertices.free()
        this.indices.free()
        this.uniforms.free()
        this.framebuffers.forEach(Framebuffer::free)
        this.renderPass.free()
        this.depthStencilView.free()
        this.depthStencil.free()
        this.swapchain.images.forEach { (it.userData as? ImageView)?.free() }
        this.swapchain.free()

        GLFW.glfwDestroyWindow(this.window)
        GLFW.glfwTerminate()

        device.free()
        Instance.current.free()
    }
}