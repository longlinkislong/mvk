package com.longlinkislong.mvk

data class PipelineRasterizationStateCreateInfo(
        val flags: Int = 0,
        val depthClampEnable: Boolean = false,
        val rasterizerDiscardEnable: Boolean = false,
        val polygonMode: PolygonMode = PolygonMode.FILL,
        val cullMode: CullMode = CullMode.NONE,
        val frontFace: FrontFace = FrontFace.COUNTER_CLOCKWISE,
        val depthBiasEnable: Boolean = false,
        val depthBiasConstantFactor: Float = 0.0f,
        val depthBiasClamp: Float = 0.0f,
        val depthBiasSlopeFactor: Float = 0.0f,
        val lineWidth: Float = 1.0f) {

    companion object {

        val DEFAULT = PipelineRasterizationStateCreateInfo()
    }
}
