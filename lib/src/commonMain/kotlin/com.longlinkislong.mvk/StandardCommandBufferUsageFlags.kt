package com.longlinkislong.mvk

object StandardCommandBufferUsageFlags {
    val NONE: CommandBufferUsageFlags = emptySet()
    val ONE_TIME_SUBMIT: CommandBufferUsageFlags = setOf(CommandBufferUsageFlag.ONE_TIME_SUBMIT)
    val RENDER_PASS_CONTINUE: CommandBufferUsageFlags = setOf(CommandBufferUsageFlag.RENDER_PASS_CONTINUE)
    val SIMULTANEOUS_USE: CommandBufferUsageFlags = setOf(CommandBufferUsageFlag.SIMULTANEOUS_USE)
}