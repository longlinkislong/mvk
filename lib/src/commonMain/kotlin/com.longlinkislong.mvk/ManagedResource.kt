package com.longlinkislong.mvk

interface ManagedResource {
    fun release()
}
