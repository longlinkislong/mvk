package com.longlinkislong.mvk

enum class PipelineBindPoint(internal val value: Int) {
    GRAPHICS(0),
    COMPUTE(1)
}
