package com.longlinkislong.mvk

class SpecializationMapEntry(
        val constantID: Int,
        val offset: Int,
        val size: Long)