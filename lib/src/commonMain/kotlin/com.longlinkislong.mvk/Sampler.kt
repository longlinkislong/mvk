package com.longlinkislong.mvk

expect class Sampler : ManagedResource {
    val info: SamplerCreateInfo
    val device: Device
}