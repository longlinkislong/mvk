package com.longlinkislong.mvk

/**
 * Information used to describe how to execute Commands.
 */
data class QueueSubmitInfo(
        val waitInfos: List<QueueSubmitWaitInfo> = emptyList(),
        val signalSemaphores: List<Semaphore> = emptyList(),
        val commandBuffers: List<CommandBuffer> = emptyList())