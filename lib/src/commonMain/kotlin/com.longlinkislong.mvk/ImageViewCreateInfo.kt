package com.longlinkislong.mvk

data class ImageViewCreateInfo(
        val viewType: ImageViewType,
        val format: Format,
        val subresourceRange: ImageSubresourceRange)