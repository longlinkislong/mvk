package com.longlinkislong.mvk

enum class StandardExtensions(override val value: String): InstanceExtension {
    EXT_ACQUIRE_XLIB_DISP("VK_EXT_acquire_xlib_display"),
    EXT_DEBUG_REPORT("VK_EXT_debug_report"),
    EXT_DIRECT_MODE_DISPLAY("VK_EXT_direct_mode_display"),
    EXT_DISPLAY_SURFACE_COUNTER("VK_EXT_display_surface_counter"),
    KHR_DISPLAY("VK_KHR_display"),
    KHR_EXTERNAL_FENCE_CAPABILITIES("VK_KHR_external_fence_capabilities"),
    KHR_EXTERNAL_MEMORY_CAPABILITIES("VK_KHR_external_memory_capabilities"),
    KHR_EXTERNAL_SEMAPHORE_CAPABILITIES("VK_KHR_external_semaphore_capabilities"),
    KHR_GET_PHYSICAL_DEVICE_PROPERTIES2("VK_KHR_get_physical_device_properties2"),
    KHR_GET_SURFACE_CAPABILITIES2("VK_KHR_get_surface_capabilities2"),
    KHR_SURFACE("VK_KHR_surface"),
    KHR_XCB_SURFACE("VK_KHR_xcb_surface"),
    KHR_XLIB_SURFACE("VK_KHR_xlib_surface")
}
