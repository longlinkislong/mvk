package com.longlinkislong.mvk

enum class CommandBufferLevel(internal val value: Int) {
    PRIMARY(0),
    SECONDARY(1)
}
