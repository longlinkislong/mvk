package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.BufferImageCopy
import com.longlinkislong.mvk.Extent3D
import com.longlinkislong.mvk.ImageSubresourceLayers
import com.longlinkislong.mvk.Offset3D

@MvkDsl
class BufferImageCopyDsl {
    var bufferOffset: Long = 0L
    var bufferRowLength: Int = 0
    var bufferImageHeight: Int = 0
    lateinit var imageSubresource: ImageSubresourceLayers
    var imageOffset: Offset3D = Offset3D.ZERO
    lateinit var imageExtent: Extent3D

    inline fun imageSubresource(block: ImageSubresourceLayersDsl.() -> Unit) {
        this.imageSubresource = ImageSubresourceLayersDsl().apply(block).toImageSubresourceLayers()
    }

    fun imageOffset(x: Int, y: Int = 0, z: Int = 0) {
        this.imageOffset = Offset3D(x, y, z)
    }

    fun imageExtent(width: Int, height: Int = 1, depth: Int = 1) {
        this.imageExtent = Extent3D(width, height, depth)
    }

    fun toBufferImageCopy() = BufferImageCopy(
            bufferOffset = bufferOffset,
            bufferRowLength = bufferRowLength,
            bufferImageHeight = bufferImageHeight,
            imageSubresource = imageSubresource,
            imageOffset = imageOffset,
            imageExtent = imageExtent)
}