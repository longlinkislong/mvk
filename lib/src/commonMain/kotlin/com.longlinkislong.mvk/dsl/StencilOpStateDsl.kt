package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CompareOp
import com.longlinkislong.mvk.StencilOp
import com.longlinkislong.mvk.StencilOpState

@MvkDsl
class StencilOpStateDsl {
    var failOp = StencilOp.REPLACE
    var passOp = StencilOp.REPLACE
    var depthFailOp = StencilOp.REPLACE
    var compareOp = CompareOp.ALWAYS
    var compareMask = 0.inv()
    var writeMask = 0.inv()
    var reference = 0.inv()

    fun toCreateInfo() = StencilOpState(
            failOp = failOp,
            passOp = passOp,
            depthFailOp = depthFailOp,
            compareOp = compareOp,
            compareMask = compareMask,
            writeMask = writeMask,
            reference = reference)
}