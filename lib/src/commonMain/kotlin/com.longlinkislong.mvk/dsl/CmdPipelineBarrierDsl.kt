package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.PipelineStageTransition

@MvkDsl
class CmdPipelineBarrierDsl {
    lateinit var stage: PipelineStageTransition
    val dependencyFlags = HashSet<DependencyFlag>()
    val memoryBarriers = ArrayList<MemoryBarrier>()
    val bufferMemoryBarriers = ArrayList<BufferMemoryBarrier>()
    val imageMemoryBarriers = ArrayList<ImageMemoryBarrier>()

    inline fun memoryBarrier(block: MemoryBarrierDsl.() -> Unit) {
        this.memoryBarriers += MemoryBarrierDsl().apply(block).toMemoryBarrier()
    }

    inline fun bufferMemoryBarrier(block: BufferMemoryBarrierDsl.() -> Unit) {
        this.bufferMemoryBarriers += BufferMemoryBarrierDsl().apply(block).toBufferMemoryBarrier()
    }

    inline fun imageMemoryBarrier(block: ImageMemoryBarrierDsl.() -> Unit) {
        this.imageMemoryBarriers += ImageMemoryBarrierDsl().apply(block).toImageMemoryBarrier()
    }
}