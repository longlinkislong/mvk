package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class SamplerCreateInfoDsl {
    var flags: Int = 0
    var minFilter: Filter = Filter.NEAREST
    var magFilter: Filter = Filter.NEAREST
    var mipmapFilter: SamplerMipmapFilter = SamplerMipmapFilter.NEAREST
    var addressModeU: SamplerAddressMode = SamplerAddressMode.REPEAT
    var addressModeV: SamplerAddressMode = SamplerAddressMode.REPEAT
    var addressModeW: SamplerAddressMode = SamplerAddressMode.REPEAT
    var mipLodBias: Float = 0.0f
    var maxAnisotropy: Float = 1.0f
    var compareEnable: Boolean = false
    var compareOp: CompareOp = CompareOp.ALWAYS
    var maxLod: Float = 1000.0f
    var minLod: Float = -1000.0f
    var borderColor: BorderColor = BorderColor.FLOAT_TRANSPARENT_BLACK
    var unnormalizedCoordinates: Boolean = false

    fun toSamplerCreateInfo() = SamplerCreateInfo(
            flags = flags,
            minFilter = minFilter,
            magFilter = magFilter,
            mipmapFilter = mipmapFilter,
            addressModeU = addressModeU,
            addressModeV = addressModeV,
            addressModeW = addressModeW,
            mipLodBias = mipLodBias,
            anistropyEnable = maxAnisotropy > 1.0F,
            compareEnable = compareEnable,
            compareOp = compareOp,
            maxLod = maxLod,
            minLod = minLod,
            borderColor = borderColor,
            unnormalizedCoordinates = unnormalizedCoordinates)
}