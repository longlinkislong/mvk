package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class QueuePresentInfoDsl {
    val swapchains = ArrayList<Swapchain>()
    val imageIndices = ArrayList<Int>()
    val waitSemaphores = ArrayList<Semaphore>()

    fun toQueuePresentInfo() = QueuePresentInfo(
            swapchains = swapchains.toList(),
            imageIndices = imageIndices.toList(),
            waitSemaphores = waitSemaphores.toList())
}