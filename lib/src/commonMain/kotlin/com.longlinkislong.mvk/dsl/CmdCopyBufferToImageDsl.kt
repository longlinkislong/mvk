package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.BufferToImageCopy

@MvkDsl
class CmdCopyBufferToImageDsl {
    lateinit var transfer: BufferToImageCopy
    var layout = ImageLayout.TRANSFER_DST
    val regions = ArrayList<BufferImageCopy>()

    inline fun region(block: BufferImageCopyDsl.() -> Unit) {
        this.regions += BufferImageCopyDsl().apply(block).toBufferImageCopy()
    }

    fun copyBufferToImage(cmd: CommandBuffer) {
        cmd.copyBufferToImage(transfer.src, transfer.dst, layout, regions)
    }
}