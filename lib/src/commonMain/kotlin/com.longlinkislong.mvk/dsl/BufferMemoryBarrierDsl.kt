package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.AccessTransition
import com.longlinkislong.mvk.util.QueueFamilyTransfer

@MvkDsl
class BufferMemoryBarrierDsl {
    lateinit var access: AccessTransition
    var queueFamily: QueueFamilyTransfer? = null
    lateinit var buffer: Buffer
    var offset: Long = 0L
    var size = -1L

    fun toBufferMemoryBarrier() = BufferMemoryBarrier(
            srcAccessMask = access.initial,
            dstAccessMask = access.final,
            srcQueueFamily = queueFamily?.initial,
            dstQueueFamily = queueFamily?.final,
            buffer = buffer,
            offset = offset,
            size = size)
}