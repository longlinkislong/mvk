package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Buffer
import com.longlinkislong.mvk.CommandBuffer
import com.longlinkislong.mvk.ComputePipeline

@MvkDsl
class ComputePipelineDsl(val cmd: CommandBuffer, val pipeline: ComputePipeline) {
    inline fun descriptorSet(firstSet: Int, block: BindDescriptorSetDsl.() -> Unit) {
        BindDescriptorSetDsl(firstSet).apply(block).bindDescriptorSets(this.cmd, this.pipeline)
    }

    inline fun pushConstants(block: PushConstantDsl.() -> Unit) {
        PushConstantDsl().apply(block).pushConstants(this.cmd, this.pipeline)
    }

    fun dispatch(x: Int, y: Int = 1, z: Int = 1) {
        cmd.dispatch(x, y, z)
    }

    fun dispatchIndirect(buffer: Buffer, offset: Long) {
        cmd.dispatchIndirect(buffer, offset)
    }
}