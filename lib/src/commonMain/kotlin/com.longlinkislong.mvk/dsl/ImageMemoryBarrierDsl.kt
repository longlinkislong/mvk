package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.AccessTransition
import com.longlinkislong.mvk.util.ImageLayoutTransition
import com.longlinkislong.mvk.util.QueueFamilyTransfer

@MvkDsl
class ImageMemoryBarrierDsl {
    lateinit var access: AccessTransition
    lateinit var layout: ImageLayoutTransition
    lateinit var image: Image
    var queueFamily: QueueFamilyTransfer? = null
    var subresourceRange: ImageSubresourceRange? = null

    inline fun subresourceRange(block: ImageSubresourceRangeDsl.() -> Unit) {
        this.subresourceRange = ImageSubresourceRangeDsl().apply(block).toImageSubresourceRange()
    }

    fun toImageMemoryBarrier() = ImageMemoryBarrier(
            srcAccessMask = access.initial.toSet(),
            dstAccessMask = access.final.toSet(),
            oldLayout = layout.initial,
            newLayout = layout.final,
            srcQueueFamily = queueFamily?.initial,
            dstQueueFamily = queueFamily?.final,
            image = image,
            subresourceRange = subresourceRange ?: image.fullRange)
}

