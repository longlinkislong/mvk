package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class BufferCreateInfoDsl {
    var flags: Int = 0
    var size: Long= 0L
    val usage: MutableSet<BufferUsageFlag> = HashSet()
    var sharingMode = SharingMode.EXCLUSIVE
    val queueFamilies: MutableSet<QueueFamily> = HashSet()
    var handleType: ExternalMemoryHandleType? = null
    val memoryProperties: MutableSet<MemoryProperty> = HashSet()
    var init: ((Buffer) -> Unit)? = null

    fun toCreateInfo() = BufferCreateInfo(
            flags = flags,
            size = size,
            usage = usage.toSet(),
            sharingMode = sharingMode,
            queueFamilies = queueFamilies.toSet(),
            handleType = handleType)

    fun onCreation(block: Buffer.() -> Unit) {
        init = block
    }
}