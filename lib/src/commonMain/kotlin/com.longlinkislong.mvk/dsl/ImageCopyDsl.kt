package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Extent3D
import com.longlinkislong.mvk.ImageCopy
import com.longlinkislong.mvk.ImageSubresourceLayers
import com.longlinkislong.mvk.Offset3D

@MvkDsl
class ImageCopyDsl {
    var srcOffset = Offset3D.ZERO
    var dstOffset = Offset3D.ZERO
    lateinit var extent: Extent3D
    lateinit var srcSubresource: ImageSubresourceLayers
    lateinit var dstSubresource: ImageSubresourceLayers

    inline fun srcSubresource(block: ImageSubresourceLayersDsl.() -> Unit) {
        this.srcSubresource = ImageSubresourceLayersDsl().apply(block).toImageSubresourceLayers()
    }

    inline fun dstSubresource(block: ImageSubresourceLayersDsl.() -> Unit) {
        this.dstSubresource = ImageSubresourceLayersDsl().apply(block).toImageSubresourceLayers()
    }

    fun toImageCopy() = ImageCopy(
            srcOffset = srcOffset,
            dstOffset = dstOffset,
            extent = extent,
            srcSubresource = srcSubresource,
            dstSubresource = dstSubresource)
}