package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class GraphicsPipelineCreateInfoDsl() {
    var flags: Int = 0
    val stages = ArrayList<PipelineShaderStageCreateInfo>()
    lateinit var layoutInfo: PipelineLayoutCreateInfo
    var colorBlendState: PipelineColorBlendStateCreateInfo? = null
    var depthStencilState: PipelineDepthStencilStateCreateInfo = PipelineDepthStencilStateCreateInfo.DEFAULT
    lateinit var inputAssemblyState: PipelineInputAssemblyStateCreateInfo
    var multisampleState: PipelineMultisampleStateCreateInfo = PipelineMultisampleStateCreateInfo.DEFAULT
    var rasterizationState: PipelineRasterizationStateCreateInfo = PipelineRasterizationStateCreateInfo.DEFAULT
    var tessellationState: PipelineTessellationStateCreateInfo? = null
    var vertexInputState: PipelineVertexInputStateCreateInfo = PipelineVertexInputStateCreateInfo.DEFAULT

    var subpass: Int = 0

    inline fun layoutInfo(block: PipelineLayoutCreateInfoDsl.() -> Unit) {
        this.layoutInfo = PipelineLayoutCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun inputAssembly(topology: PrimitiveTopology, block: PipelineInputAssemblyStateCreateInfoDsl.() -> Unit) {
        this.inputAssemblyState = PipelineInputAssemblyStateCreateInfoDsl(topology).apply(block).toCreateInfo()
    }

    fun inputAssembly(topology: PrimitiveTopology) {
        this.inputAssemblyState = PipelineInputAssemblyStateCreateInfo(topology = topology)
    }

    inline fun stage(stage: ShaderStage, size: Int, pData: Long, block: PipelineShaderStageCreateInfoDsl.() -> Unit) {
        this.stages += PipelineShaderStageCreateInfoDsl(stage, size, pData).apply(block).toCreateInfo()
    }

    fun stage(stage: ShaderStage, size: Int, pData: Long) {
        this.stages += PipelineShaderStageCreateInfo(
                stage = stage,
                moduleInfo = ShaderModuleCreateInfo(
                        pData = pData,
                        size = size))
    }

    inline fun colorBlend(block: PipelineColorBlendStateCreateInfoDsl.() -> Unit) {
        this.colorBlendState = PipelineColorBlendStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun depthStencil(block: PipelineDepthStencilStateCreateInfoDsl.() -> Unit) {
        this.depthStencilState = PipelineDepthStencilStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun vertexInput(block: PipelineVertexInputStateCreateInfoDsl.() -> Unit) {
        this.vertexInputState = PipelineVertexInputStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun multisample(block: PipelineMultisampleStateCreateInfoDsl.() -> Unit) {
        this.multisampleState = PipelineMultisampleStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun rasterization(block: PipelineRasterizationStateCreateInfoDsl.() -> Unit) {
        this.rasterizationState = PipelineRasterizationStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    inline fun tessellation(block: PipelineTessellationStateCreateInfoDsl.() -> Unit) {
        this.tessellationState = PipelineTessellationStateCreateInfoDsl().apply(block).toCreateInfo()
    }

    fun toCreateInfo() = GraphicsPipelineCreateInfo(
            flags = flags,
            stages = stages.toList(),
            layoutInfo = layoutInfo,
            colorBlendState = colorBlendState,
            depthStencilState = depthStencilState,
            inputAssemblyState = inputAssemblyState,
            multisampleState = multisampleState,
            rasterizationState = rasterizationState,
            tessellationState = tessellationState,
            vertexInputState = vertexInputState,
            subpass = subpass)
}