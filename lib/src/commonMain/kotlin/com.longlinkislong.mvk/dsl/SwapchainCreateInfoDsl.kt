package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.QueueFamily
import com.longlinkislong.mvk.SurfaceFormat
import com.longlinkislong.mvk.SwapchainCreateInfo

@MvkDsl
class SwapchainCreateInfoDsl {
    var surface: Long = 0L
    var surfaceFormat = SurfaceFormat.DEFAULT
    var queueFamily: QueueFamily? = null
    var width: Int = 640
    var height: Int = 480

    fun toSwapchainCreateInfo() = SwapchainCreateInfo(
            surface = surface,
            surfaceFormat = surfaceFormat,
            queueFamily = queueFamily,
            width = width,
            height = height)
}