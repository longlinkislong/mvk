package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.AttachmentReference
import com.longlinkislong.mvk.ImageLayout

@MvkDsl
class AttachmentReferenceDsl {
    var attachment: Int = 0
    lateinit var layout: ImageLayout

    fun toAttachmentReference() = AttachmentReference(
            attachment = attachment,
            layout = layout
    )
}