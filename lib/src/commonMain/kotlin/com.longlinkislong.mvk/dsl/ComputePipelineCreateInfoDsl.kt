package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class ComputePipelineCreateInfoDsl {
    var flags: Int = 0
    lateinit var stage: PipelineShaderStageCreateInfo
    lateinit var layoutInfo: PipelineLayoutCreateInfo

    inline fun stage(size: Int, pData: Long, block: PipelineShaderStageCreateInfoDsl.() -> Unit) {
        this.stage = PipelineShaderStageCreateInfoDsl(ShaderStage.COMPUTE, size, pData).apply(block).toCreateInfo()
    }

    fun stage(size: Int, pData: Long) {
        this.stage = PipelineShaderStageCreateInfo(
                stage = ShaderStage.COMPUTE,
                moduleInfo = ShaderModuleCreateInfo(
                        pData = pData,
                        size = size))
    }

    inline fun layoutInfo(block: PipelineLayoutCreateInfoDsl.() -> Unit) {
        this.layoutInfo = PipelineLayoutCreateInfoDsl().apply(block).toCreateInfo()
    }

    fun toCreateInfo() = ComputePipelineCreateInfo(
            flags = flags,
            stage = stage,
            layoutInfo = layoutInfo)
}