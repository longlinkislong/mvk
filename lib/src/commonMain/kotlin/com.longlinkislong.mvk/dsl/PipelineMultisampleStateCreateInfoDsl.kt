package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineMultisampleStateCreateInfo

@MvkDsl
class PipelineMultisampleStateCreateInfoDsl {
    var flags: Int = 0
    var rasterizationSamples = 0x1
    var sampleShadingEnable = false
    var minSampleShading = 0.0F
    var pSampleMask: Long = 0L
    var alphaToCoverageEnable = false
    var alphaToOneEnable = false

    fun toCreateInfo() = PipelineMultisampleStateCreateInfo(
            flags = flags,
            rasterizationSamples = rasterizationSamples,
            sampleShadingEnable = sampleShadingEnable,
            minSampleShading = minSampleShading,
            pSampleMask = pSampleMask,
            alphaToCoverageEnable = alphaToCoverageEnable,
            alphaToOneEnable = alphaToOneEnable)
}