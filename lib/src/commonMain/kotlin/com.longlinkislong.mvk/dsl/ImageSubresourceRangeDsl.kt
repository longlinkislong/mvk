package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.AspectFlag
import com.longlinkislong.mvk.ImageSubresourceRange

@MvkDsl
class ImageSubresourceRangeDsl {
    val aspectMask = HashSet<AspectFlag>()
    var baseMipLevel = 0
    var levelCount = 0
    var baseArrayLayer = 0
    var layerCount = 1

    fun toImageSubresourceRange() = ImageSubresourceRange(
            aspectMask = aspectMask.toSet(),
            baseMipLevel = baseMipLevel,
            levelCount = levelCount,
            baseArrayLayer = baseArrayLayer,
            layerCount = layerCount)
}