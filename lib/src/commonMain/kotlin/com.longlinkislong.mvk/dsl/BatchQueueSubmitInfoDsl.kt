package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Fence
import com.longlinkislong.mvk.QueueSubmitInfo

@MvkDsl
class BatchQueueSubmitInfoDsl {
    var fence: Fence? = null
    val submitInfos = ArrayList<QueueSubmitInfo>()

    inline fun submitInfo(block: QueueSubmitInfoDsl.() -> Unit) {
        this.submitInfos += QueueSubmitInfoDsl().apply(block).toQueueSubmitInfo()
    }
}