package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CullMode
import com.longlinkislong.mvk.FrontFace
import com.longlinkislong.mvk.PipelineRasterizationStateCreateInfo
import com.longlinkislong.mvk.PolygonMode

@MvkDsl
class PipelineRasterizationStateCreateInfoDsl {
    var flags: Int = 0
    var depthClampEnable = false
    var rasterizationDiscardEnable = false
    var polygonMode = PolygonMode.FILL
    var cullMode = CullMode.NONE
    var frontFace = FrontFace.COUNTER_CLOCKWISE
    var depthBiasEnable = false
    var depthBiasConstantFactor = 0.0F
    var depthBiasClamp = 0.0F
    var depthBiasSlopeFactor = 0.0F
    var lineWidth = 1.0F

    fun toCreateInfo() = PipelineRasterizationStateCreateInfo(
            flags = flags,
            depthClampEnable = depthClampEnable,
            rasterizerDiscardEnable = rasterizationDiscardEnable,
            polygonMode = polygonMode,
            cullMode = cullMode,
            frontFace = frontFace,
            depthBiasEnable = depthBiasEnable,
            depthBiasConstantFactor = depthBiasConstantFactor,
            depthBiasClamp = depthBiasClamp,
            depthBiasSlopeFactor = depthBiasSlopeFactor,
            lineWidth = lineWidth)
}