package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.VertexInputBindingDescription
import com.longlinkislong.mvk.VertexInputRate

@MvkDsl
class VertexInputBindingDescriptionDsl(val binding: Int) {
    var inputRate: VertexInputRate = VertexInputRate.PER_VERTEX
    var stride: Int = 0

    fun toCreateInfo() = VertexInputBindingDescription(
            binding = binding,
            stride = stride,
            inputRate = inputRate
    )
}