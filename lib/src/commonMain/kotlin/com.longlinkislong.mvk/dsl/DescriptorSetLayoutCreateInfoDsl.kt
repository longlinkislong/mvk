package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.DescriptorSetLayoutBinding
import com.longlinkislong.mvk.DescriptorSetLayoutCreateInfo

@MvkDsl
class DescriptorSetLayoutCreateInfoDsl {
    var flags: Int = 0
    val bindings = ArrayList<DescriptorSetLayoutBinding>()

    inline fun binding(binding: Int, block: DescriptorSetLayoutBindingDsl.() -> Unit) {
        this.bindings += DescriptorSetLayoutBindingDsl(binding).apply(block).toCreateInfo()
    }

    fun toCreateInfo() = DescriptorSetLayoutCreateInfo(
            flags = flags,
            bindings = bindings.toList()
    )
}