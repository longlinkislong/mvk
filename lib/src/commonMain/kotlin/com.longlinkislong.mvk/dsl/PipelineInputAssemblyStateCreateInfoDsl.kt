package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineInputAssemblyStateCreateInfo
import com.longlinkislong.mvk.PrimitiveTopology

@MvkDsl
class PipelineInputAssemblyStateCreateInfoDsl(val topology: PrimitiveTopology) {
    var flags: Int = 0
    var primitiveRestartEnable: Boolean = false

    fun toCreateInfo() = PipelineInputAssemblyStateCreateInfo(
            flags = flags,
            topology = topology,
            primitiveRestartEnable = primitiveRestartEnable
    )
}