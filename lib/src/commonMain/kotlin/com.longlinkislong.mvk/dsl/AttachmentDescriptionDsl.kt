package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.ImageLayoutTransition

@MvkDsl
class AttachmentDescriptionDsl {
    var flags: Int = 0
    lateinit var format: Format
    var samples: Int = 0x1
    var loadOp: LoadOp = LoadOp.CLEAR
    var storeOp: StoreOp = StoreOp.STORE
    var stencilLoadOp: LoadOp = LoadOp.DONT_CARE
    var stencilStoreOp: StoreOp = StoreOp.DONT_CARE
    lateinit var layout: ImageLayoutTransition

    fun toAttachmentDescription() = AttachmentDescription(
            flags = flags,
            format = format,
            samples = samples,
            loadOp = loadOp,
            storeOp = storeOp,
            stencilLoadOp = stencilLoadOp,
            stencilStoreOp = stencilStoreOp,
            initialLayout = layout.initial,
            finalLayout = layout.final)
}