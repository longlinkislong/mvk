package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class RenderPassCreateInfoDsl {
    var flags: Int = 0
    val attachments = ArrayList<AttachmentDescription>()
    val subpasses = ArrayList<SubpassDescription>()
    val dependencies = ArrayList<SubpassDependency>()

    inline fun attachment(block: AttachmentDescriptionDsl.() -> Unit) {
        this.attachments += AttachmentDescriptionDsl().apply(block).toAttachmentDescription()
    }

    inline fun subpass(block: SubpassDescriptionDsl.() -> Unit) {
        this.subpasses += SubpassDescriptionDsl().apply(block).toSubpassDescription()
    }

    inline fun dependency(block: SubpassDependencyDsl.() -> Unit) {
        this.dependencies += SubpassDependencyDsl().apply(block).toSubpassDependency()
    }

    fun toRenderPassCreateInfo() = RenderPassCreateInfo(
            flags = flags,
            attachments = attachments.toList(),
            subpasses = subpasses.toList(),
            dependencies = dependencies.toList())
}