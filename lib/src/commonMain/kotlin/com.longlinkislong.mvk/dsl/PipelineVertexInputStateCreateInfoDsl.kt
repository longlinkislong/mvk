package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineVertexInputStateCreateInfo
import com.longlinkislong.mvk.VertexInputAttributeDescription
import com.longlinkislong.mvk.VertexInputBindingDescription

@MvkDsl
class PipelineVertexInputStateCreateInfoDsl {
    var flags: Int = 0
    val vertexBindingDescriptions = ArrayList<VertexInputBindingDescription>()
    val vertexAttributeDescriptions = ArrayList<VertexInputAttributeDescription>()

    inline fun binding(binding: Int, block: VertexInputBindingDescriptionDsl.() -> Unit) {
        this.vertexBindingDescriptions += VertexInputBindingDescriptionDsl(binding).apply(block).toCreateInfo()
    }

    inline fun attribute(location: Int, block: VertexInputAttributeDescriptionDsl.() -> Unit) {
        this.vertexAttributeDescriptions += VertexInputAttributeDescriptionDsl(location).apply(block).toCreateInfo()
    }

    fun toCreateInfo() = PipelineVertexInputStateCreateInfo(
            flags = flags,
            vertexBindingDescriptions = vertexBindingDescriptions.toList(),
            vertexAttributeDescriptions = vertexAttributeDescriptions.toList())
}