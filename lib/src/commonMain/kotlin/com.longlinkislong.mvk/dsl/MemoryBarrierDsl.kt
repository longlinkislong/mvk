package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.MemoryBarrier
import com.longlinkislong.mvk.util.AccessTransition

@MvkDsl
class MemoryBarrierDsl {
    lateinit var access: AccessTransition

    fun toMemoryBarrier() = MemoryBarrier(
            srcAccessMask = access.initial.toSet(),
            dstAccessMask = access.final.toSet())
}