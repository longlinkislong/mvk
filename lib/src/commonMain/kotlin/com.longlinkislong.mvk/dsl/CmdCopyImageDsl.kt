package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.ImageCopy
import com.longlinkislong.mvk.ImageLayout
import com.longlinkislong.mvk.util.ImageToImageCopy

@MvkDsl
class CmdCopyImageDsl {
    lateinit var transfer: ImageToImageCopy
    var srcLayout: ImageLayout = ImageLayout.TRANSFER_SRC
    var dstLayout: ImageLayout = ImageLayout.TRANSFER_DST
    val regions = ArrayList<ImageCopy>()

    inline fun region(block: ImageCopyDsl.() -> Unit) {
        this.regions += ImageCopyDsl().apply(block).toImageCopy()
    }
}