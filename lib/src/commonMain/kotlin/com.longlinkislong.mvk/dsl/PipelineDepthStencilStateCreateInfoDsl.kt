package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CompareOp
import com.longlinkislong.mvk.PipelineDepthStencilStateCreateInfo
import com.longlinkislong.mvk.StencilOpState

@MvkDsl
class PipelineDepthStencilStateCreateInfoDsl {
    var flags: Int = 0
    var depthTestEnabled = false
    var depthWriteEnabled = false
    var depthCompareOp = CompareOp.LESS
    var depthBoundsTestEnable = false
    var stencilTestEnable = false
    var front = StencilOpState()
    var back = StencilOpState()
    var minDepthBounds = 0.0F
    var maxDepthBounds = 0.0F

    inline fun front(block: StencilOpStateDsl.() -> Unit) {
        this.front = StencilOpStateDsl().apply(block).toCreateInfo()
    }

    inline fun back(block: StencilOpStateDsl.() -> Unit) {
        this.back = StencilOpStateDsl().apply(block).toCreateInfo()
    }

    fun toCreateInfo() = PipelineDepthStencilStateCreateInfo(
            flags = flags,
            depthTestEnable = depthTestEnabled,
            depthWriteEnable = depthWriteEnabled,
            depthCompareOp = depthCompareOp,
            depthBoundsTestEnable = depthBoundsTestEnable,
            stencilTestEnable = stencilTestEnable,
            front = front,
            back = back,
            minDepthBounds = minDepthBounds,
            maxDepthBounds = maxDepthBounds)
}