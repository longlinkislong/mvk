package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.ImageBlit
import com.longlinkislong.mvk.ImageSubresourceLayers
import com.longlinkislong.mvk.Offset3D

@MvkDsl
class ImageBlitDsl {
    var srcOffset0 = Offset3D.ZERO
    var dstOffset0 = Offset3D.ZERO
    lateinit var srcOffset1: Offset3D
    lateinit var dstOffset1: Offset3D
    lateinit var srcSubresource: ImageSubresourceLayers
    lateinit var dstSubresource: ImageSubresourceLayers

    inline fun srcSubresource(block: ImageSubresourceLayersDsl.() -> Unit) {
        this.srcSubresource = ImageSubresourceLayersDsl().apply(block).toImageSubresourceLayers()
    }

    inline fun dstSubresource(block: ImageSubresourceLayersDsl.() -> Unit) {
        this.dstSubresource = ImageSubresourceLayersDsl().apply(block).toImageSubresourceLayers()
    }

    fun toImageBlit() = ImageBlit(
            srcOffset0 = srcOffset0,
            srcOffset1 = srcOffset1,
            dstOffset0 = dstOffset0,
            dstOffset1 = dstOffset1,
            srcSubresource = srcSubresource,
            dstSubresource = dstSubresource)
}