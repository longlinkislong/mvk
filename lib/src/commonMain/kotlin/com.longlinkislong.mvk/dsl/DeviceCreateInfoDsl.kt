package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.DeviceCreateInfo
import com.longlinkislong.mvk.DeviceExtension
import com.longlinkislong.mvk.QueueFlags

@MvkDsl
class DeviceCreateInfoDsl {
    val enabledExtensions = HashSet<DeviceExtension>()
    var queueCreationRules: ((index: Int, flags: QueueFlags) -> FloatArray)? = null

    fun toCreateInfo() = DeviceCreateInfo(
            enabledExtensions = enabledExtensions.toSet(),
            queueCreationRules = queueCreationRules)
}