package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.AttachmentReference
import com.longlinkislong.mvk.PipelineBindPoint
import com.longlinkislong.mvk.SubpassDescription

@MvkDsl
class SubpassDescriptionDsl {
    var flags: Int = 0
    var pipelineBindpoint = PipelineBindPoint.GRAPHICS
    val inputAttachments = ArrayList<AttachmentReference>()
    val colorAttachments = ArrayList<AttachmentReference>()
    val resolveAttachments = ArrayList<AttachmentReference>()
    var depthStencilAttachment: AttachmentReference? = null
    val preserveAttachments = ArrayList<Int>()

    inline fun inputAttachment(block: AttachmentReferenceDsl.() -> Unit) {
        this.inputAttachments += AttachmentReferenceDsl().apply(block).toAttachmentReference()
    }

    inline fun colorAttachment(block: AttachmentReferenceDsl.() -> Unit) {
        this.colorAttachments += AttachmentReferenceDsl().apply(block).toAttachmentReference()
    }

    inline fun resolveAttachment(block: AttachmentReferenceDsl.() -> Unit) {
        this.resolveAttachments += AttachmentReferenceDsl().apply(block).toAttachmentReference()
    }

    inline fun depthStencilAttachment(block: AttachmentReferenceDsl.() -> Unit) {
        this.depthStencilAttachment = AttachmentReferenceDsl().apply(block).toAttachmentReference()
    }

    fun toSubpassDescription() = SubpassDescription(
            flags = flags,
            pipelineBindpoint = pipelineBindpoint,
            inputAttachments = inputAttachments.toList(),
            colorAttachments = colorAttachments.toList(),
            resolveAttachments = resolveAttachments.toList(),
            depthStencilAttachment = depthStencilAttachment,
            preserveAttachments = preserveAttachments.toList())

}