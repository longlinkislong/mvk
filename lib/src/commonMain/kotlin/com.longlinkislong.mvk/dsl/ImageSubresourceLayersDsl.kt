package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.AspectFlag
import com.longlinkislong.mvk.ImageSubresourceLayers

@MvkDsl
class ImageSubresourceLayersDsl {
    val aspectMask = HashSet<AspectFlag>()
    var mipLevel: Int = 0
    var baseArrayLayer: Int = 0
    var layerCount: Int = 1

    fun toImageSubresourceLayers() = ImageSubresourceLayers(
            aspectMask = aspectMask.toSet(),
            mipLevel = mipLevel,
            baseArrayLayer = baseArrayLayer,
            layerCount = layerCount)
}