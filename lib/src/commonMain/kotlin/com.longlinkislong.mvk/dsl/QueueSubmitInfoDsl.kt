package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class QueueSubmitInfoDsl {
    val waitInfos = ArrayList<QueueSubmitWaitInfo>()
    val signalSemaphores = ArrayList<Semaphore>()
    val commandBuffers = ArrayList<CommandBuffer>()
    var fence: Fence? = null

    inline fun waitInfo(block: QueueSubmitWaitInfoDsl.() -> Unit) {
        this.waitInfos += QueueSubmitWaitInfoDsl().apply(block).toQueueSubmitWaitInfo()
    }

    fun toQueueSubmitInfo() = QueueSubmitInfo(
            waitInfos = waitInfos.toList(),
            signalSemaphores = signalSemaphores.toList(),
            commandBuffers = commandBuffers.toList())
}