package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PushConstantRange
import com.longlinkislong.mvk.ShaderStage

@MvkDsl
class PushConstantRangeCreateInfoDsl(val size: Int) {
    val stages = HashSet<ShaderStage>()
    var offset: Int = 0

    fun toCreateInfo() = PushConstantRange(
            stages = stages.toSet(),
            offset = offset,
            size = size
    )
}