package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class DescriptorSetLayoutBindingDsl(val binding: Int) {
    lateinit var descriptorType: DescriptorType
    var descriptorCount: Int = 1
    val stages = HashSet<ShaderStage>()
    val immutableSamplers = ArrayList<Sampler>()

    fun toCreateInfo() = DescriptorSetLayoutBinding(
            binding = binding,
            descriptorType = descriptorType,
            descriptorCount = descriptorCount,
            stages = stages.toSet(),
            immutableSamplers = immutableSamplers.toList())
}