package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CommandBuffer
import com.longlinkislong.mvk.DescriptorSet
import com.longlinkislong.mvk.Pipeline

@MvkDsl
class BindDescriptorSetDsl(val firstSet: Int) {
    val sets = ArrayList<DescriptorSet>()
    val dynamicOffsets = ArrayList<Int>()

    fun bindDescriptorSets(cmd: CommandBuffer, pipeline: Pipeline) {
        cmd.bindDescriptorSets(pipeline, firstSet, sets, dynamicOffsets)
    }
}