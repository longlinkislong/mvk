package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CommandBufferInheritanceInfo
import com.longlinkislong.mvk.CommandBufferUsageFlag

@MvkDsl
class CmdBeginDsl {
    val flags = HashSet<CommandBufferUsageFlag>()
    var inheritanceInfo: CommandBufferInheritanceInfo? = null

    fun inheritanceInfo(block: InheritanceInfoDsl.() -> Unit) {
        this.inheritanceInfo = InheritanceInfoDsl().apply(block).toInheritanceInfo()
    }
}