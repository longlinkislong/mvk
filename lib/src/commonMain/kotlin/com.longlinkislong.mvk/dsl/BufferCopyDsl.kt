package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.BufferCopy

@MvkDsl
class BufferCopyDsl {
    var srcOffset: Long = 0L
    var dstOffset: Long = 0L
    var size: Long = 0.inv()

    fun toBufferCopy() = BufferCopy(
            srcOffset = srcOffset,
            dstOffset = dstOffset,
            size = size)
}