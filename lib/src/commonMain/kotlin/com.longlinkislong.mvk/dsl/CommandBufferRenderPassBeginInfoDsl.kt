package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class CommandBufferRenderPassBeginInfoDsl {
    lateinit var framebuffer: Framebuffer
    lateinit var renderPass: RenderPass
    var subpassContents = SubpassContents.INLINE
    lateinit var renderArea: Rect2D
    val clearValues = ArrayList<ClearValue>()

    fun toCommandBufferRenderPassBeginInfo() = CommandBufferRenderPassBeginInfo(
            framebuffer = framebuffer,
            renderPass = renderPass,
            subpassContents = subpassContents,
            renderArea = renderArea,
            clearValues = clearValues.toList())
}