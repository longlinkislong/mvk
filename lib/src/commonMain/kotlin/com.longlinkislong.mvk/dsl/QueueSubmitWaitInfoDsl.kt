package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineStageFlag
import com.longlinkislong.mvk.QueueSubmitWaitInfo
import com.longlinkislong.mvk.Semaphore

@MvkDsl
class QueueSubmitWaitInfoDsl {
    lateinit var semaphore: Semaphore
    val stageFlags = HashSet<PipelineStageFlag>()

    fun toQueueSubmitWaitInfo() = QueueSubmitWaitInfo(
            semaphore = semaphore,
            stageFlags = stageFlags.toSet())
}