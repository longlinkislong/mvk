package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineShaderStageCreateInfo
import com.longlinkislong.mvk.ShaderModuleCreateInfo
import com.longlinkislong.mvk.ShaderStage

class PipelineShaderStageCreateInfoDsl(val stage: ShaderStage, size: Int, pData: Long) {
    var flags: Int = 0
    val moduleInfo = ShaderModuleCreateInfo(
            pData = pData,
            size = size)
    var name = "main"
    //TODO: Specialization info

    fun toCreateInfo() = PipelineShaderStageCreateInfo(
            flags = flags,
            stage = stage,
            moduleInfo = moduleInfo,
            name = name)
}