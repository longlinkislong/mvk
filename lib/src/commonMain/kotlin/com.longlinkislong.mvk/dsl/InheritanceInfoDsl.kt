package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CommandBufferInheritanceInfo
import com.longlinkislong.mvk.Framebuffer
import com.longlinkislong.mvk.RenderPass

@MvkDsl
class InheritanceInfoDsl {
    var renderPass: RenderPass? = null
    var subpass: Int = 0
    var framebuffer: Framebuffer? = null
    var occlusionQueryEnable: Boolean = false
    var queryFlags: Int = 0
    var pipelineStatistics: Int = 0

    fun toInheritanceInfo() = CommandBufferInheritanceInfo(
            renderPass = renderPass,
            subpass = subpass,
            framebuffer = framebuffer,
            occlusionQueryEnable = occlusionQueryEnable,
            queryFlags = queryFlags,
            pipelineStatistics = pipelineStatistics)
}