package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.PipelineTessellationStateCreateInfo

@MvkDsl
class PipelineTessellationStateCreateInfoDsl {
    var flags: Int = 0
    var patchControlPoints: Int = 0

    fun toCreateInfo() = PipelineTessellationStateCreateInfo(
            flags = flags,
            patchControlPoints = patchControlPoints
    )
}