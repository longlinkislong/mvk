package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*

@MvkDsl
class ImageCreateInfoDsl {
    var flags: Int = 0
    lateinit var imageType: ImageType
    lateinit var format: Format
    lateinit var extent: Extent3D
    var mipLevels: Int = 1
    var arrayLayers: Int = 1
    var samples: Int = 0x1
    var tiling: ImageTiling = ImageTiling.OPTIMAL
    val usage: MutableSet<ImageUsageFlag> = HashSet()
    var sharingMode: SharingMode = SharingMode.EXCLUSIVE
    val queueFamilies: MutableSet<QueueFamily> = HashSet()
    var initialLayout: ImageLayout = ImageLayout.UNDEFINED
    var handleType: ExternalMemoryHandleType? = null
    var init: ((Image) -> Unit)? = null
    val memoryProperties: MutableSet<MemoryProperty> = HashSet()

    fun extent(width: Int, height: Int = 1, depth: Int = 1) {
        this.extent = Extent3D(width, height, depth)
    }

    fun onCreation(block: Image.() -> Unit) {
        this.init = block
    }

    fun toCreateInfo() = ImageCreateInfo(
            flags = flags,
            imageType = imageType,
            format = format,
            extent = extent,
            mipLevels = mipLevels,
            arrayLayers = arrayLayers,
            samples = samples,
            tiling = tiling,
            usage = usage.toSet(),
            sharingMode = sharingMode,
            queueFamilies = queueFamilies.toSet(),
            initialLayout = initialLayout,
            handleType = handleType)
}