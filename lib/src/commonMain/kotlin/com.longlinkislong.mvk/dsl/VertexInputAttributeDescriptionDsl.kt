package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Format
import com.longlinkislong.mvk.VertexInputAttributeDescription

@MvkDsl
class VertexInputAttributeDescriptionDsl(val location: Int) {
    var binding: Int = 0
    lateinit var format: Format
    var offset: Int = 0

    fun toCreateInfo() = VertexInputAttributeDescription(
            location = location,
            binding = binding,
            format = format,
            offset = offset)
}