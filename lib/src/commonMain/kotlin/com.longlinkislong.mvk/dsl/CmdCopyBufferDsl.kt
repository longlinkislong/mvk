package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.BufferCopy
import com.longlinkislong.mvk.util.BufferToBufferCopy

@MvkDsl
class CmdCopyBufferDsl {
    lateinit var transfer: BufferToBufferCopy
    val regions = ArrayList<BufferCopy>()

    inline fun region(block: BufferCopyDsl.() -> Unit) {
        this.regions += BufferCopyDsl().apply(block).toBufferCopy()
    }
}