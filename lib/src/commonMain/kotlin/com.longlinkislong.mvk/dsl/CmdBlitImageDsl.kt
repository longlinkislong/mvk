package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Filter
import com.longlinkislong.mvk.ImageBlit
import com.longlinkislong.mvk.ImageLayout
import com.longlinkislong.mvk.util.ImageToImageCopy

@MvkDsl
class CmdBlitImageDsl {
    lateinit var transfer: ImageToImageCopy
    var srcLayout = ImageLayout.TRANSFER_SRC
    var dstLayout = ImageLayout.TRANSFER_DST
    val regions = ArrayList<ImageBlit>()
    var filter = Filter.LINEAR

    inline fun region(block: ImageBlitDsl.() -> Unit) {
        this.regions += ImageBlitDsl().apply(block).toImageBlit()
    }
}