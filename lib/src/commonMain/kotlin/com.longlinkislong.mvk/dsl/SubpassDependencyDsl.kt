package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.util.AccessTransition
import com.longlinkislong.mvk.util.PipelineStageTransition

@MvkDsl
class SubpassDependencyDsl {
    lateinit var subpass: Pair<Int, Int>
    lateinit var stage: PipelineStageTransition
    lateinit var access: AccessTransition
    val dependencyFlags = HashSet(StandardDependencyFlags.BY_REGION)

    fun toSubpassDependency() = SubpassDependency(
            srcSubpass = subpass.first,
            srcStageMask = stage.initial.toSet(),
            srcAccessMask = access.initial.toSet(),
            dstSubpass = subpass.second,
            dstStageMask = stage.final.toSet(),
            dstAccessMask = access.final.toSet(),
            dependencyFlags = dependencyFlags.toSet())
}