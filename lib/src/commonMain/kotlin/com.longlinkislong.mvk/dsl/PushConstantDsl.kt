package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.CommandBuffer
import com.longlinkislong.mvk.Pipeline
import com.longlinkislong.mvk.ShaderStage

@MvkDsl
class PushConstantDsl {
    val stages = HashSet<ShaderStage>()
    var offset: Int = 0
    var size: Int = 0
    var pData: Long = 0L

    fun pushConstants(cmd: CommandBuffer, pipeline: Pipeline) {
        cmd.pushConstants(pipeline.pipelineLayout, stages, offset, size, pData)
    }
}