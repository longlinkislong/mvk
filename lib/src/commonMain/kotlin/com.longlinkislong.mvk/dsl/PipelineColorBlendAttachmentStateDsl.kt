package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.BlendFactor
import com.longlinkislong.mvk.BlendOp
import com.longlinkislong.mvk.PipelineColorBlendAttachmentState

@MvkDsl
class PipelineColorBlendAttachmentStateDsl {
    var blendEnable: Boolean = false
    var srcColorBlendFactor: BlendFactor = BlendFactor.ONE
    var dstColorBlendFactor: BlendFactor = BlendFactor.ZERO
    var colorBlendOp: BlendOp = BlendOp.ADD
    var srcAlphaBlendFactor: BlendFactor = BlendFactor.ONE
    var dstAlphaBlendFactor: BlendFactor = BlendFactor.ZERO
    var alphaBlendOp: BlendOp = BlendOp.ADD
    var colorWriteMask: Int = 0xF

    fun toCreateInfo() = PipelineColorBlendAttachmentState(
            blendEnable = blendEnable,
            srcColorBlendFactor = srcColorBlendFactor,
            dstColorBlendFactor = dstColorBlendFactor,
            colorBlendOp = colorBlendOp,
            srcAlphaBlendFactor = srcAlphaBlendFactor,
            dstAlphaBlendFactor = dstAlphaBlendFactor,
            alphaBlendOp = alphaBlendOp,
            colorWriteMask = colorWriteMask)
}