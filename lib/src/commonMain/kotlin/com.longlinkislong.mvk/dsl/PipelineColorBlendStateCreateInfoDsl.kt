package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.Color
import com.longlinkislong.mvk.LogicOp
import com.longlinkislong.mvk.PipelineColorBlendAttachmentState
import com.longlinkislong.mvk.PipelineColorBlendStateCreateInfo

@MvkDsl
class PipelineColorBlendStateCreateInfoDsl {
    var flags: Int = 0
    var logicOpEnable: Boolean = false
    var logicOp: LogicOp = LogicOp.COPY
    val attachments = ArrayList<PipelineColorBlendAttachmentState>()
    var blendConstants = Color.OPAQUE_WHITE

    inline fun attachment(block: PipelineColorBlendAttachmentStateDsl.() -> Unit) {
        this.attachments += PipelineColorBlendAttachmentStateDsl().apply(block).toCreateInfo()
    }

    fun toCreateInfo() = PipelineColorBlendStateCreateInfo(
            flags = flags,
            logicOpEnable = logicOpEnable,
            logicOp = logicOp,
            attachments = attachments.toList(),
            blendConstants = blendConstants)
}