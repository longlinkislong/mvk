package com.longlinkislong.mvk.dsl

import com.longlinkislong.mvk.DescriptorSetLayoutCreateInfo
import com.longlinkislong.mvk.PipelineLayoutCreateInfo
import com.longlinkislong.mvk.PushConstantRange

@MvkDsl
class PipelineLayoutCreateInfoDsl {
    var flags: Int = 0
    val pushConstantRanges = ArrayList<PushConstantRange>()
    val setLayoutInfos = ArrayList<DescriptorSetLayoutCreateInfo>()

    inline fun pushConstantRange(size: Int, block: PushConstantRangeCreateInfoDsl.() -> Unit) {
        this.pushConstantRanges += PushConstantRangeCreateInfoDsl(size).apply(block).toCreateInfo()
    }

    inline fun setLayoutInfo(block: DescriptorSetLayoutCreateInfoDsl.() -> Unit) {
        this.setLayoutInfos += DescriptorSetLayoutCreateInfoDsl().apply(block).toCreateInfo()
    }

    fun toCreateInfo() = PipelineLayoutCreateInfo(
            flags = flags,
            pushConstantRanges = pushConstantRanges.toList(),
            setLayoutInfos = setLayoutInfos.toList())
}