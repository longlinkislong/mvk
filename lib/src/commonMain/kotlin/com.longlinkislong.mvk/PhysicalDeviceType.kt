package com.longlinkislong.mvk

enum class PhysicalDeviceType(internal val value: Int) {
    OTHER(0),
    INTEGRATED_GPU(1),
    DISCRETE_GPU(2),
    VIRTUAL_GPU(3),
    CPU(4)
}
