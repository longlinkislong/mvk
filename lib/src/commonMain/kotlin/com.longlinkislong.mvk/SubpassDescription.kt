package com.longlinkislong.mvk

data class SubpassDescription(
        val flags: Int = 0,
        val pipelineBindpoint: PipelineBindPoint = PipelineBindPoint.GRAPHICS,
        val inputAttachments: List<AttachmentReference> = emptyList(),
        val colorAttachments: List<AttachmentReference> = emptyList(),
        val resolveAttachments: List<AttachmentReference> = emptyList(),
        val depthStencilAttachment: AttachmentReference? = null,
        val preserveAttachments: List<Int> = emptyList())