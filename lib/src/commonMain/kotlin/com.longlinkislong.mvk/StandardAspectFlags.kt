package com.longlinkislong.mvk

object StandardAspectFlags {
    val NONE: AspectFlags = emptySet()
    val COLOR: AspectFlags = setOf(AspectFlag.COLOR)
    val DEPTH: AspectFlags = setOf(AspectFlag.DEPTH)
    val STENCIL: AspectFlags = setOf(AspectFlag.STENCIL)
    val DEPTH_STENCIL: AspectFlags = setOf(AspectFlag.DEPTH, AspectFlag.STENCIL)
}