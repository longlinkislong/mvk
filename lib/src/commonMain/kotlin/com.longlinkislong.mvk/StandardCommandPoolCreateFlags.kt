package com.longlinkislong.mvk

object StandardCommandPoolCreateFlags {
    val NONE: CommandPoolCreateFlags = emptySet()
    val CREATE_PROTECTED: CommandPoolCreateFlags = setOf(CommandPoolCreateFlag.CREATE_PROTECTED)
    val CREATE_RESET_COMMAND_BUFFER: CommandPoolCreateFlags = setOf(CommandPoolCreateFlag.CREATE_RESET_COMMAND_BUFFER)
    val CREATE_TRANSIENT: CommandPoolCreateFlags = setOf(CommandPoolCreateFlag.CREATE_TRANSIENT)
}