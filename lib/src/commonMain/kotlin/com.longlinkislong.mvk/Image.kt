package com.longlinkislong.mvk

expect class Image : Resource {
    val device: Device
    val fullRange: ImageSubresourceRange
    val isArrayed: Boolean
    val hasMipmap: Boolean
    val fullViewCreateInfo: ImageViewCreateInfo
    val info: ImageCreateInfo

    /**
     * User-defined data
     */
    var userData: Any?

    fun getMipAllLayers(mipLevel: Int): ImageSubresourceLayers
    fun getSubresourceLayers(mipLevel: Int, baseArrayLayer: Int, layerCount: Int): ImageSubresourceLayers
    fun getSubresourceLayer(mipLevel: Int, baseArrayLayer: Int): ImageSubresourceLayers
    fun getSubresourceRange(baseMipLevel: Int, levelCount: Int, baseArrayLayer: Int, layerCount: Int): ImageSubresourceRange
}