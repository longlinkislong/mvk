package com.longlinkislong.mvk

data class DepthStencil(
        val depth: Float,
        val stencil: Int)