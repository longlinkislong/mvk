package com.longlinkislong.mvk

object StandardImageUsageFlags {
    val NONE: ImageUsageFlags = emptySet()

    val SAMPLED_IMAGE_FROM_TRANSFER = setOf(ImageUsageFlag.SAMPLED, ImageUsageFlag.TRANSFER_DST)
    val STORAGE_IMAGE_FROM_TRANSFER = setOf(ImageUsageFlag.STORAGE, ImageUsageFlag.TRANSFER_DST)
    val SAMPLED_IMAGE_FROM_RENDERPASS = setOf(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.SAMPLED)

    val COLOR_ATTACHMENT: ImageUsageFlags = setOf(ImageUsageFlag.COLOR_ATTACHMENT)
    val SAMPLED: ImageUsageFlags = setOf(ImageUsageFlag.SAMPLED)
    val DEPTH_STENCIL_ATTACHMENT: ImageUsageFlags = setOf(ImageUsageFlag.DEPTH_STENCIL_ATTACHMENT)
    val TRANSFER_SRC: ImageUsageFlags = setOf(ImageUsageFlag.TRANSFER_SRC)
    val TRANSFER_DST: ImageUsageFlags = setOf(ImageUsageFlag.TRANSFER_DST)
    val TRANSFER: ImageUsageFlags = setOf(ImageUsageFlag.TRANSFER_DST, ImageUsageFlag.TRANSFER_SRC)
    val STORAGE: ImageUsageFlags = setOf(ImageUsageFlag.STORAGE)
    val INPUT_ATTACHMENT: ImageUsageFlags = setOf(ImageUsageFlag.INPUT_ATTACHMENT)
    val TRANSIENT_ATTACHMENT: ImageUsageFlags = setOf(ImageUsageFlag.TRANSIENT_ATTACHMENT)
}