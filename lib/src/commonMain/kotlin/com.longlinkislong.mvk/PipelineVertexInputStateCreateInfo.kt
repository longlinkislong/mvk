package com.longlinkislong.mvk

data class PipelineVertexInputStateCreateInfo(
        val flags: Int = 0,
        val vertexBindingDescriptions: List<VertexInputBindingDescription> = emptyList(),
        val vertexAttributeDescriptions: List<VertexInputAttributeDescription> = emptyList()) {

    companion object {
        val DEFAULT = PipelineVertexInputStateCreateInfo()
    }
}
