package com.longlinkislong.mvk

expect class BufferView(buffer: Buffer, info: BufferViewCreateInfo) : Resource {
    val buffer: Buffer
    val device: Device
    val info: BufferViewCreateInfo

    /**
     * User-defined data
     */
    var userData: Any?
}