package com.longlinkislong.mvk

typealias AspectFlags = Set<AspectFlag>

val AspectFlags.bitfield: Int
    get() = AspectFlag.toBitfield(this)

