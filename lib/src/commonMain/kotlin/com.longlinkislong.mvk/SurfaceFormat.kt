package com.longlinkislong.mvk

data class SurfaceFormat(
        val format: Format,
        val colorSpace: ColorSpace) {

    companion object {
        val DEFAULT = SurfaceFormat(Format.B8G8R8A8_UNORM, ColorSpace.SRGB_NONLINEAR)
    }
}
