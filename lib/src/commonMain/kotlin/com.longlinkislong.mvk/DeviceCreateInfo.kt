package com.longlinkislong.mvk

/**
 * The construction information for creating a logical Device.
 *
 * @param enabledExtensions is the set of extensions to enable for the device.
 * @param queueCreationRules is user-defined rules for specifying the number of queues to allocate
 */
data class DeviceCreateInfo(
        val enabledExtensions: Set<DeviceExtension>,
        val queueCreationRules: ((index: Int, flags: QueueFlags) -> FloatArray)? = null)