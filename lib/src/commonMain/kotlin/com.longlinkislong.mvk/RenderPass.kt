package com.longlinkislong.mvk

expect class RenderPass : Resource {
    val info: RenderPassCreateInfo
    val device: Device

    /**
     * User-defined data
     */
    var userData: Any?
}