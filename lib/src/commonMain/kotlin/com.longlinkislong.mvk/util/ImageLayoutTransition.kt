package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.ImageLayout

data class ImageLayoutTransition(
        val initial: ImageLayout,
        val final: ImageLayout)