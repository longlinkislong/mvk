package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.AccessFlags


data class AccessTransition(
        val initial: AccessFlags,
        val final: AccessFlags)

