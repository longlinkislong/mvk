package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.Buffer

data class BufferToBufferCopy(
        val src: Buffer,
        val dst: Buffer)

