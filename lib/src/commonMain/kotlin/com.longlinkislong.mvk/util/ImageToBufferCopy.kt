package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.Buffer
import com.longlinkislong.mvk.Image

data class ImageToBufferCopy(
        val src: Image,
        val dst: Buffer)