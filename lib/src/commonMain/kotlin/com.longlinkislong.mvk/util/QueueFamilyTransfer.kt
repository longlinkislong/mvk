package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.QueueFamily

data class QueueFamilyTransfer(
        val initial: QueueFamily,
        val final: QueueFamily)