package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.Image

data class ImageToImageCopy(
        val src: Image,
        val dst: Image)