package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.PipelineStageFlags

data class PipelineStageTransition(
        val initial: PipelineStageFlags,
        val final: PipelineStageFlags)