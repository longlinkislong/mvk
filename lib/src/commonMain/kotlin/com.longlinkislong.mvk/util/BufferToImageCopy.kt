package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.Buffer
import com.longlinkislong.mvk.Image

data class BufferToImageCopy(
        val src: Buffer,
        val dst: Image)