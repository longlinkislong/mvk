package com.longlinkislong.mvk

data class ImageCreateInfo(
        val flags: Int = 0,
        val imageType: ImageType,
        val format: Format,
        val extent: Extent3D = Extent3D.ZERO,
        val mipLevels: Int = 1,
        val arrayLayers: Int = 1,
        val samples: Int = 0x1,
        val tiling: ImageTiling = ImageTiling.OPTIMAL,
        val usage: ImageUsageFlags = StandardImageUsageFlags.NONE,
        val sharingMode: SharingMode = SharingMode.EXCLUSIVE,
        val queueFamilies: Set<QueueFamily> = emptySet(),
        val initialLayout: ImageLayout = ImageLayout.UNDEFINED,
        val handleType: ExternalMemoryHandleType? = null)