package com.longlinkislong.mvk

expect class CommandPool : Resource {
    val flags: CommandPoolCreateFlags
    val queueFamily: QueueFamily
    val device: Device

    fun reset(flags: Int)
    fun allocate(level: CommandBufferLevel): CommandBuffer
    fun allocate(level: CommandBufferLevel, count: Int): List<CommandBuffer>
}
