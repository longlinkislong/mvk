package com.longlinkislong.mvk

object StandardQueueFlags {
    val NONE: QueueFlags = emptySet()
    val GRAPHICS: QueueFlags = setOf(QueueFlag.GRAPHICS)
    val COMPUTE: QueueFlags = setOf(QueueFlag.COMPUTE)
    val TRANSFER: QueueFlags = setOf(QueueFlag.TRANSFER)
    val SPARSE_BINDING: QueueFlags = setOf(QueueFlag.SPARSE_BINDING)
    val PROTECTED: QueueFlags = setOf(QueueFlag.PROTECTED)
    val GENERAL: QueueFlags = setOf(QueueFlag.GRAPHICS, QueueFlag.COMPUTE, QueueFlag.TRANSFER)
}