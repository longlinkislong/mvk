package com.longlinkislong.mvk

data class PipelineTessellationStateCreateInfo(
        val flags: Int,
        val patchControlPoints: Int)