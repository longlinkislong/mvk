package com.longlinkislong.mvk

enum class LunarGLayer(override val value: String) : InstanceLayer {
    API_DUMP("VK_LAYER_LUNARG_api_dump"),
    ASSISTANT_LAYER("VK_LAYER_LUNARG_assistant_layer"),
    CORE_VALIDATION("VK_LAYER_LUNARG_core_validation"),
    DEVICE_SIMULATION("VK_LAYER_LUNARG_device_simulation"),
    MONITOR("VK_LAYER_LUNARG_monitor"),
    OBJECT_TRACKER("VK_LAYER_LUNARG_object_tracker"),
    PARAMETER_VALIDATION("VK_LAYER_LUNARG_parameter_validation"),
    SCREENSHOT("VK_LAYER_LUNARG_screenshot"),
    STANDARD_VALIDATION("VK_LAYER_LUNARG_standard_validation"),
    VKTRACE("VK_LAYER_LUNARG_vktrace")
}
