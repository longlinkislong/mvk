package com.longlinkislong.mvk

enum class FrontFace(internal val value: Int) {
    COUNTER_CLOCKWISE(0),
    CLOCKWISE(1)
}
