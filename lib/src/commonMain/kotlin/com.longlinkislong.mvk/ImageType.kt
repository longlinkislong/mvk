package com.longlinkislong.mvk

enum class ImageType(internal val value: Int) {
    IMAGE_1D(0),
    IMAGE_2D(1),
    IMAGE_3D(2)
}
