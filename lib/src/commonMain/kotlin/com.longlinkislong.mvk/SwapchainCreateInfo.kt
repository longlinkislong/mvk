package com.longlinkislong.mvk

data class SwapchainCreateInfo(
        val surface: Long = 0L,
        val surfaceFormat: SurfaceFormat = SurfaceFormat.DEFAULT,
        val queueFamily: QueueFamily? = null,
        val width: Int = 640, val height: Int = 480)