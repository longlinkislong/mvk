package com.longlinkislong.mvk

enum class ImageUsageFlag(internal val value: Int) {
    TRANSFER_SRC(0x1),
    TRANSFER_DST(0x2),
    SAMPLED(0x4),
    STORAGE(0x8),
    COLOR_ATTACHMENT(0x10),
    DEPTH_STENCIL_ATTACHMENT(0x20),
    TRANSIENT_ATTACHMENT(0x40),
    INPUT_ATTACHMENT(0x80);

    fun toImageUsageFlags(): ImageUsageFlags = setOf(this)

    companion object {

        internal fun toBitfield(flags: Set<ImageUsageFlag>): Int = flags.sumBy { it.value }
    }
}
