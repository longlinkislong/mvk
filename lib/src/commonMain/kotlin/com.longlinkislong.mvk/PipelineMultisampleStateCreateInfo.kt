package com.longlinkislong.mvk

data class PipelineMultisampleStateCreateInfo(
        val flags: Int = 0,
        val rasterizationSamples: Int = 0x1,
        val sampleShadingEnable: Boolean = false,
        val minSampleShading: Float = 0.0f,
        val pSampleMask: Long = 0L,
        val alphaToCoverageEnable: Boolean = false,
        val alphaToOneEnable: Boolean = false) {

    companion object {
        val DEFAULT = PipelineMultisampleStateCreateInfo()
    }
}
