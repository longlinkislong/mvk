package com.longlinkislong.mvk

enum class StencilOp(internal val value: Int) {
    KEEP(0),
    ZERO(1),
    REPLACE(2),
    INCREMENT_AND_CLAMP(3),
    DECREMENT_AND_CLAMP(4),
    INVERT(5),
    INCREMENT_AND_WRAP(6),
    DECREMENT_AND_WRAP(7)
}
