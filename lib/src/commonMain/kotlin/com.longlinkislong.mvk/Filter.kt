package com.longlinkislong.mvk

enum class Filter(internal val value: Int) {
    NEAREST(0),
    LINEAR(1)
}
