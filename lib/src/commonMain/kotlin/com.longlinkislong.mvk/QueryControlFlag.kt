package com.longlinkislong.mvk

enum class QueryControlFlag(val value: Int) {
    PRECISE(1)
}