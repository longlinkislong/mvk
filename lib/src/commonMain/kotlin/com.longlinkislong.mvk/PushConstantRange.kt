package com.longlinkislong.mvk

data class PushConstantRange(
        val stages: ShaderStages = emptySet(),
        val offset: Int = 0,
        val size: Int = 0)