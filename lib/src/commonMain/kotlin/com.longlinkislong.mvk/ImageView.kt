package com.longlinkislong.mvk

expect class ImageView(image: Image, info: ImageViewCreateInfo = image.fullViewCreateInfo) : Resource {
    val info: ImageViewCreateInfo
    val image: Image
    val device: Device

    /**
     * User-defined data
     */
    var userData: Any?
}