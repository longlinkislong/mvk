package com.longlinkislong.mvk

expect class Fence : ManagedResource {
    val device: Device
    val isSignaled: Boolean

    fun reset(): Fence
    fun waitFor(): Fence
}