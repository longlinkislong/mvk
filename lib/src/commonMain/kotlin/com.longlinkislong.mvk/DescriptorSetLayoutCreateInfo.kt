package com.longlinkislong.mvk

data class DescriptorSetLayoutCreateInfo(
        val flags: Int = 0,
        val bindings: List<DescriptorSetLayoutBinding> = emptyList())