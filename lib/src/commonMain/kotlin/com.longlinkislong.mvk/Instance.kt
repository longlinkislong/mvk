package com.longlinkislong.mvk

expect class Instance : Resource {
    val physicalDevices: List<PhysicalDevice>

    fun createWindowSurface(window: Long): Long

    companion object {
        var applicationVersion: Int
        var engineVersion: Int
        var apiVersion: Int
        var applicationName: String
        var engineName: String

        val enabledLayers: MutableSet<InstanceLayer>
        val enabledExtensions: MutableSet<InstanceExtension>
        val current: Instance

        fun setApplicationVersion(major: Int, minor: Int, patch: Int)
        fun setEngineVersion(major: Int, minor: Int, patch: Int)
        fun setAPIVersion(major: Int, minor: Int, patch: Int)
        fun restoreDefaultLayers()
        fun restoreExtensions()
    }
}