package com.longlinkislong.mvk

enum class CullMode(internal val value: Int) {
    FRONT(0x1),
    BACK(0x2),
    FRONT_AND_BACK(0x3),
    NONE(0)
}
