package com.longlinkislong.mvk

/**
 * A Vulkan Queue object. Queues accept CommandBuffer objects and execute them on the Vulkan Device.
 */
expect class Queue {
    val device: Device
    val queueFamily: QueueFamily
    val queueIndex: Int

    /**
     * Blocks the current thread until the Queue has completed all enqueued Command Submissions.
     */
    fun waitIdle()

    /**
     * Submits a Command Submission to the Queue with an optional Fence to signal on completion.
     */
    fun submit(submitInfo: QueueSubmitInfo, fence: Fence? = null)

    fun submit(submitInfos: List<QueueSubmitInfo>, fence: Fence? = null)

    fun present(info: QueuePresentInfo)
}