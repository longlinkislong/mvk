package com.longlinkislong.mvk

object StandardDependencyFlags {
    val NONE: DependencyFlags = emptySet()
    val BY_REGION: DependencyFlags = setOf(DependencyFlag.BY_REGION)
    val DEVICE_GROUP: DependencyFlags = setOf(DependencyFlag.DEVICE_GROUP)
    val VIEW_LOCAL: DependencyFlags = setOf(DependencyFlag.VIEW_LOCAL)
}