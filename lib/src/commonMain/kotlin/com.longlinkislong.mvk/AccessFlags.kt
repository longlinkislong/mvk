package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.AccessTransition

typealias AccessFlags = Set<AccessFlag>

val AccessFlags.bitfield: Int
    get() = AccessFlag.toBitfield(this)

infix fun AccessFlags.transitionTo(that: AccessFlags) = AccessTransition(this, that)

infix fun AccessFlags.transitionTo(that: AccessFlag) = AccessTransition(this, setOf(that))

fun AccessFlags.inPlace() = AccessTransition(this, this)

object StandardAccessFlags {
    val INDIRECT_COMMAND_READ: AccessFlags = setOf(AccessFlag.INDIRECT_COMMAND_READ)
    val INDEX_READ: AccessFlags = setOf(AccessFlag.INDEX_READ)
    val VERTEX_ATTRIBUTE_READ: AccessFlags = setOf(AccessFlag.VERTEX_ATTRIBUTE_READ)
    val UNIFORM_READ: AccessFlags = setOf(AccessFlag.UNIFORM_READ)
    val INPUT_ATTACHMENT_READ: AccessFlags = setOf(AccessFlag.INPUT_ATTACHMENT_READ)
    val SHADER_READ: AccessFlags = setOf(AccessFlag.SHADER_READ)
    val SHADER_WRITE: AccessFlags = setOf(AccessFlag.SHADER_WRITE)
    val SHADER_READ_WRITE: AccessFlags = setOf(AccessFlag.SHADER_READ, AccessFlag.SHADER_WRITE)
    val COLOR_ATTACHMENT_READ: AccessFlags = setOf(AccessFlag.COLOR_ATTACHMENT_READ)
    val COLOR_ATTACHMENT_WRITE: AccessFlags = setOf(AccessFlag.COLOR_ATTACHMENT_WRITE)
    val COLOR_ATTACHMENT_READ_WRITE: AccessFlags = setOf(AccessFlag.COLOR_ATTACHMENT_READ, AccessFlag.COLOR_ATTACHMENT_WRITE)
    val DEPTH_STENCIL_ATTACHMENT_READ: AccessFlags = setOf(AccessFlag.DEPTH_STENCIL_ATTACHMENT_READ)
    val DEPTH_STENCIL_ATTACHMENT_WRITE: AccessFlags = setOf(AccessFlag.DEPTH_STENCIL_ATTACHMENT_WRITE)
    val DEPTH_STENCIL_ATTACHMENT_READ_WRITE: AccessFlags = setOf(AccessFlag.DEPTH_STENCIL_ATTACHMENT_READ, AccessFlag.DEPTH_STENCIL_ATTACHMENT_WRITE)
    val TRANSFER_READ: AccessFlags = setOf(AccessFlag.TRANSFER_READ)
    val TRANSFER_WRITE: AccessFlags = setOf(AccessFlag.TRANSFER_WRITE)
    val TRANSFER_READ_WRITE: AccessFlags = setOf(AccessFlag.TRANSFER_READ, AccessFlag.TRANSFER_WRITE)
    val HOST_READ: AccessFlags = setOf(AccessFlag.HOST_READ)
    val HOST_WRITE: AccessFlags = setOf(AccessFlag.HOST_WRITE)
    val HOST_READ_WRITE: AccessFlags = setOf(AccessFlag.HOST_READ, AccessFlag.HOST_WRITE)
    val MEMORY_READ: AccessFlags = setOf(AccessFlag.MEMORY_READ)
    val MEMORY_WRITE: AccessFlags = setOf(AccessFlag.MEMORY_WRITE)
    val MEMORY_READ_WRITE: AccessFlags = setOf(AccessFlag.MEMORY_WRITE, AccessFlag.MEMORY_READ)
    val NONE: AccessFlags = emptySet()
}
