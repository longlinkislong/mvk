package com.longlinkislong.mvk

enum class ImageTiling(internal val value: Int) {
    OPTIMAL(0),
    LINEAR(1)
}
