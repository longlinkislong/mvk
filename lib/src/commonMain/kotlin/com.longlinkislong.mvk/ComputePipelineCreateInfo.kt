package com.longlinkislong.mvk

data class ComputePipelineCreateInfo(
        val flags: Int = 0,
        val stage: PipelineShaderStageCreateInfo,
        val layoutInfo: PipelineLayoutCreateInfo)