package com.longlinkislong.mvk

data class PipelineColorBlendStateCreateInfo(
        val flags: Int = 0,
        val logicOpEnable: Boolean = false,
        val logicOp: LogicOp = LogicOp.COPY,
        val attachments: List<PipelineColorBlendAttachmentState> = emptyList(),
        val blendConstants: Color = Color.OPAQUE_WHITE)