package com.longlinkislong.mvk

typealias ImageUsageFlags = Set<ImageUsageFlag>

val ImageUsageFlags.bitfield: Int
    get() = ImageUsageFlag.toBitfield(this)

