package com.longlinkislong.mvk

data class Color(val red: Float, val green: Float, val blue: Float, val alpha: Float) {

    companion object {
        val TRANSPARENT_BLACK = Color(0.0f, 0.0f, 0.0f, 0.0f)
        val OPAQUE_BLACK = Color(0.0f, 0.0f, 0.0f, 1.0f)
        val OPAQUE_WHITE = Color(1.0f, 1.0f, 1.0f, 1.0f)
    }
}
