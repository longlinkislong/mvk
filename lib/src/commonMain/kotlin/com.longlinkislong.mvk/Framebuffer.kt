package com.longlinkislong.mvk

expect class Framebuffer : Resource {
    val info: FramebufferCreateInfo
    val device: Device
    val renderPass: RenderPass
    val attachments: List<ImageView>

    /**
     * User-defined data
     */
    var userData: Any?

    fun getRenderPassBeginInfo(clearValues: List<ClearValue>): CommandBufferRenderPassBeginInfo
}