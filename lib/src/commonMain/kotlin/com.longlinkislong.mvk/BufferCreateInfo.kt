package com.longlinkislong.mvk

data class BufferCreateInfo(
        val flags: Int = 0,
        val size: Long = 0L,
        val usage: BufferUsageFlags = StandardBufferUsageFlags.NONE,
        val sharingMode: SharingMode = SharingMode.EXCLUSIVE,
        val queueFamilies: Set<QueueFamily> = emptySet(),
        val handleType: ExternalMemoryHandleType? = null)