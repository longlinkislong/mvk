package com.longlinkislong.mvk

data class VertexInputAttributeDescription(
        val location: Int = 0,
        val binding: Int = 0,
        val format: Format,
        val offset: Int = 0)