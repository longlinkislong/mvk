package com.longlinkislong.mvk

expect class DescriptorSet : ManagedResource {
    val device: Device

    fun writeBuffer(type: DescriptorType, binding: Int, buffer: Buffer): DescriptorSet

    fun writeBuffer(type: DescriptorType, binding: Int, buffer: Buffer, offset: Long, size: Long): DescriptorSet

    fun writeTexelBuffer(type: DescriptorType, binding: Int, view: BufferView): DescriptorSet

    fun writeImageArray(
            type: DescriptorType, binding: Int,
            samplers: List<Sampler>?,
            imageLayout: ImageLayout,
            views: List<ImageView>): DescriptorSet

    fun writeImage(
            type: DescriptorType, binding: Int,
            sampler: Sampler?, imageLayout: ImageLayout, imageView: ImageView): DescriptorSet
}