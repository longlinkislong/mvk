package com.longlinkislong.mvk

typealias CommandBufferUsageFlags = Set<CommandBufferUsageFlag>

val CommandBufferUsageFlags.bitfield: Int
    get() = CommandBufferUsageFlag.toBitfield(this)

