package com.longlinkislong.mvk

data class ImageMemoryBarrier(
        val srcAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val dstAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val oldLayout: ImageLayout = ImageLayout.UNDEFINED,
        val newLayout: ImageLayout = ImageLayout.UNDEFINED,
        val srcQueueFamily: QueueFamily? = null,
        val dstQueueFamily: QueueFamily? = null,
        val image: Image,
        val subresourceRange: ImageSubresourceRange)