package com.longlinkislong.mvk

typealias BufferUsageFlags = Set<BufferUsageFlag>

val BufferUsageFlags.bitfield: Int
    get() = BufferUsageFlag.toBitfield(this)

