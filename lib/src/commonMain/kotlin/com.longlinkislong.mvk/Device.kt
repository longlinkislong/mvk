package com.longlinkislong.mvk

/**
 * An instance of an implementation of a Vulkan Device. All objects are allocated through the Device.
 */
expect class Device : Resource {
    val info: DeviceCreateInfo
    val physicalDevice: PhysicalDevice

    /**
     * A list of all QueueFamilies that belong to the device. This list is read-only.
     */
    val queueFamilies: List<QueueFamily>

    /**
     * Acquires a Fence from the fence pool. If there are no available fences, a new Fence is allocated. This method is
     * thread-safe.
     * @return an unsignaled Fence
     */
    fun acquireFence(): Fence

    /**
     * Acquires a Semaphore from the semaphore pool. If there are no available Semaphores, a new Semaphore is allocated.
     * This method is thread-safe.
     * @return an unsignaled Semaphore.
     */
    fun acquireSemaphore(): Semaphore

    /**
     * Allocates a new Sampler from the sampler cache. This method is thread-safe.
     * @param createInfo the information required to describe construction of a Sampler.
     * @return the Sampler.
     */
    fun allocateSampler(createInfo: SamplerCreateInfo): Sampler

    /**
     * Waits until all Queues that belong to the Device are idle.
     */
    fun waitIdle()

    /**
     * Allocates a ComputePipeline. A previously created ComputePipeline may be returned if it was created with a
     * compatible CreateInfo. This method is thread-safe.
     * @param createInfo the information required to describe construction of a ComputePipeline.
     * @return the ComputePipeline
     */
    fun allocatePipeline(createInfo: ComputePipelineCreateInfo): ComputePipeline

    /**
     * Allocates a GraphicsPipeline. A previously created GraphicsPipeline may be returned if it was created with a
     * compatible CreateInfo. This method is thread-safe.
     * @param createInfo the information required to describe construction of a GraphicsPipeline
     * @param renderPass the RenderPass or a compatible RenderPass that this GraphicsPipeline will be used in.
     * @return the GraphicsPipeline
     */
    fun allocatePipeline(createInfo: GraphicsPipelineCreateInfo, renderPass: RenderPass): GraphicsPipeline

    /**
     * Creates a new Buffer with memory backing. This method is thread-safe.
     * @param createInfo the information required to describe construction of a Buffer.
     * @param properties A series of MemoryProperty objects that describe the type of memory required.
     * @return the Buffer.
     */
    fun createBuffer(createInfo: BufferCreateInfo, properties: MemoryProperties): Buffer

    /**
     * Creates a new Image with memory backing. This method is thread-safe.
     * @param createInfo the information required to describe construction of an Image.
     * @param properties A series of MemoryProperty objects that describe the type of memory required.
     * @return the Image.
     */
    fun createImage(createInfo: ImageCreateInfo, properties: MemoryProperties): Image

    /**
     * Creates a new RenderPass. This method is thread-safe.
     * @param createInfo the information required to describe construction of a RenderPass.
     * @return the RenderPass.
     */
    fun createRenderPass(createInfo: RenderPassCreateInfo): RenderPass

    /**
     * Creates a new SwapChain. This method is thread-safe.
     * @param createInfo the information required to describe construction of a SwapChain.
     * @return the SwapChain.
     */
    fun createSwapchain(createInfo: SwapchainCreateInfo): Swapchain
}