package com.longlinkislong.mvk

enum class ShaderStage(internal val value: Int) {
    VERTEX(0x1),
    TESSELLATION_CONTROL(0x2),
    TESSELLATION_EVALUATION(0x4),
    GEOMETRY(0x8),
    FRAGMENT(0x10),
    COMPUTE(0x20);


    companion object {
        internal fun toBitfield(flags: Set<ShaderStage>): Int = flags.sumBy { it.value }
    }
}
