package com.longlinkislong.mvk

data class ClearValue(
        val color: Color? = null,
        val depthStencil: DepthStencil? = null)