package com.longlinkislong.mvk

enum class BufferUsageFlag(internal val value: Int) {
    TRANSFER_SRC(0x1),
    TRANSFER_DST(0x2),
    UNIFORM_TEXEL_BUFFER(0x4),
    STORAGE_TEXEL_BUFFER(0x8),
    UNIFORM_BUFFER(0x10),
    STORAGE_BUFFER(0x20),
    INDEX_BUFFER(0x40),
    VERTEX_BUFFER(0x80),
    INDIRECT_BUFFER(0x100);

    fun toBufferUsageFlags(): BufferUsageFlags = setOf(this)

    companion object {
        internal fun toBitfield(flags: Set<BufferUsageFlag>): Int = flags.sumBy { it.value }
    }
}
