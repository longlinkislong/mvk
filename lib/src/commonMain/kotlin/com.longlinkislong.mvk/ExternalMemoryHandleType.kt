package com.longlinkislong.mvk

enum class ExternalMemoryHandleType(internal val value: Int) {
    OPAQUE_FD(1),
    OPAQUE_WIN32(2),
    OPAQUE_WIN32_KMT(4),
    D3D11_TEXTURE(8),
    D3D11_TEXTURE_KMT(4),
    D3D12_HEAP(32),
    D3D12_RESOURCE(64)
}
