package com.longlinkislong.mvk

data class AttachmentDescription(
        val flags: Int = 0,
        val format: Format,
        val samples: Int = 0x1,
        val loadOp: LoadOp = LoadOp.CLEAR,
        val storeOp: StoreOp = StoreOp.STORE,
        val stencilLoadOp: LoadOp = LoadOp.DONT_CARE,
        val stencilStoreOp: StoreOp = StoreOp.DONT_CARE,
        val initialLayout: ImageLayout = ImageLayout.UNDEFINED,
        val finalLayout: ImageLayout = ImageLayout.UNDEFINED)