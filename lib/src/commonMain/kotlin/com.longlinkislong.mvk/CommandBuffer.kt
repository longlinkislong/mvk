package com.longlinkislong.mvk

expect class CommandBuffer : ManagedResource {
    val commandPool: CommandPool

    fun reset(flags: Int)

    fun begin(flags: CommandBufferUsageFlags, inheritanceInfo: CommandBufferInheritanceInfo? = null): CommandBuffer

    fun end(): CommandBuffer

    fun executeCommands(commands: List<CommandBuffer>): CommandBuffer

    fun beginRenderPass(beginInfo: CommandBufferRenderPassBeginInfo): CommandBuffer

    fun nextSubpass(contents: SubpassContents): CommandBuffer

    fun endRenderPass(): CommandBuffer

    fun setViewport(viewport: Viewport): CommandBuffer

    fun setScissor(scissor: Rect2D): CommandBuffer

    fun bindVertexBuffers(firstBinding: Int, buffers: List<Buffer>, offsets: List<Long>): CommandBuffer

    fun bindIndexBuffer(buffer: Buffer, offset: Long, indexType: IndexType): CommandBuffer

    fun dispatch(groupsX: Int, groupsY: Int = 1, groupsZ: Int = 1): CommandBuffer

    fun dispatchIndirect(buffer: Buffer, offset: Long): CommandBuffer

    fun draw(vertexCount: Int, instanceCount: Int, firstVertex: Int, firstInstance: Int): CommandBuffer

    fun drawIndirect(buffer: Buffer, offset: Long, drawCount: Int, stride: Int): CommandBuffer

    fun drawIndexed(indexCount: Int, instanceCount: Int, firstIndex: Int, vertexOffset: Int, firstInstance: Int): CommandBuffer

    fun drawIndexedIndirect(buffer: Buffer, offset: Long, drawCount: Int, stride: Int): CommandBuffer

    fun pipelineBarrier(
            srcStageMask: PipelineStageFlags, dstStageMask: PipelineStageFlags,
            dependencyFlags: DependencyFlags,
            memoryBarriers: List<MemoryBarrier>,
            bufferMemoryBarriers: List<BufferMemoryBarrier>,
            imageMemoryBarriers: List<ImageMemoryBarrier>): CommandBuffer

    fun bindPipeline(pipeline: Pipeline): CommandBuffer

    fun bindDescriptorSets(pipeline: Pipeline, firstSet: Int, descriptorSets: List<DescriptorSet>, dynamicOffsets: List<Int>): CommandBuffer

    fun copyImage(
            src: Image, srcLayout: ImageLayout,
            dst: Image, dstLayout: ImageLayout,
            regions: List<ImageCopy>): CommandBuffer

    fun blitImage(
            src: Image, srcLayout: ImageLayout,
            dst: Image, dstLayout: ImageLayout,
            regions: List<ImageBlit>, filter: Filter = Filter.LINEAR): CommandBuffer

    fun copyBufferToImage(src: Buffer, dst: Image, layout: ImageLayout, regions: List<BufferImageCopy>): CommandBuffer

    fun copyBuffer(src: Buffer, dst: Buffer, regions: List<BufferCopy>): CommandBuffer

    fun fillBuffer(buffer: Buffer, offset: Long, size: Long, data: Int): CommandBuffer

    fun generateMipmaps(image: Image, filter: Filter): CommandBuffer

    fun pushConstants(layout: PipelineLayout, stage: ShaderStages, offset: Int, size: Int, pData: Long): CommandBuffer

    fun updateBuffer(buffer: Buffer, offset: Long, dataSize: Long, pData: Long): CommandBuffer
}