package com.longlinkislong.mvk

expect class PhysicalDevice {
    val vendor: Vendor
    val preferredDepthStencilFormat: Format
    val hasUnifiedMemory: Boolean
    val physicalDeviceType: PhysicalDeviceType
    val totalDeviceRam: Long

    fun createDevice(info: DeviceCreateInfo): Device
    fun alignUpToMinUniformBufferOffset(size: Long): Long
    fun alignDownToMinUniformBufferOffset(size: Long): Long
    fun alignUpToMinVertexBufferOffset(size: Long): Long
    fun alignDownToMinVertexBufferOffset(size: Long): Long
    fun alignUpToMinTexelBufferOffset(size: Long): Long
    fun alignDownToMinTexelBufferOffset(size: Long): Long
    fun alignUpToMinIndexBufferOffset(size: Long, indexType: IndexType): Long
    fun alignDownToMinIndexBufferOffset(size: Long, indexType: IndexType): Long
    fun alignUpToMinStorageBufferOffset(size: Long): Long
    fun alignDownToMinStorageBufferOffset(size: Long): Long

    companion object {
        val preferDiscreteGpu: (PhysicalDevice, PhysicalDevice) -> Int
        val preferDeviceRam: (PhysicalDevice, PhysicalDevice) -> Int
        val preferIntegratedGpu: (PhysicalDevice, PhysicalDevice) -> Int
    }
}