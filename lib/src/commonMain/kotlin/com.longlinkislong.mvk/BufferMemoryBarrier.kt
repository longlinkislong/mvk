package com.longlinkislong.mvk

data class BufferMemoryBarrier(
        val srcAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val dstAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val srcQueueFamily: QueueFamily? = null,
        val dstQueueFamily: QueueFamily? = null,
        val buffer: Buffer,
        val offset: Long = 0L,
        val size: Long = -1L)