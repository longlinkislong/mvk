package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.dsl.*

fun CommandBuffer.setScissor(x: Int, y: Int, width: Int, height: Int): CommandBuffer = this.setScissor(
        Rect2D(
                offset = Offset2D(x, y),
                extent = Extent2D(width, height)))

fun CommandBuffer.setScissor(offset: Offset2D, extent: Extent2D) = this.setScissor(
        Rect2D(
                offset = offset,
                extent = extent))

fun CommandBuffer.setViewport(
        x: Float, y: Float,
        width: Float, height: Float,
        minDepth: Float = 0.0F, maxDepth: Float = 1.0F) = this.setViewport(
        Viewport(
                x = x,
                y = y,
                width = width,
                height = height,
                minDepth = minDepth,
                maxDepth = maxDepth))

fun CommandBuffer.bindVertexBuffer(binding: Int, buffer: Buffer, offset: Long) = this.bindVertexBuffers(binding, listOf(buffer), listOf(offset))

fun CommandBuffer.bindDescriptorSet(pipeline: Pipeline, binding: Int, descriptorSet: DescriptorSet) =
        this.bindDescriptorSets(pipeline, binding, listOf(descriptorSet), emptyList())

fun CommandBuffer.bindDescriptorSet(pipeline: Pipeline, binding: Int, descriptorSet: DescriptorSet, dynamicOffset: Int) =
        this.bindDescriptorSets(pipeline, binding, listOf(descriptorSet), listOf(dynamicOffset))

fun CommandBuffer.pushConstants(pipeline: Pipeline, stages: ShaderStages, offset: Int, size: Int, pData: Long) =
        this.pushConstants(pipeline.pipelineLayout, stages, offset, size, pData)

fun CommandBuffer.copyImage(src: Image, srcLayout: ImageLayout, dst: Image, dstLayout: ImageLayout, region: ImageCopy) =
        this.copyImage(src, srcLayout, dst, dstLayout, listOf(region))

fun CommandBuffer.copyBufferToImage(src: Buffer, dst: Image, layout: ImageLayout, region: BufferImageCopy) =
        this.copyBufferToImage(src, dst, layout, listOf(region))

fun CommandBuffer.copyBuffer(src: Buffer, dst: Buffer, region: BufferCopy) = this.copyBuffer(src, dst, listOf(region))

fun CommandBuffer.blitImage(src: Image, srcLayout: ImageLayout, dst: Image, dstLayout: ImageLayout, region: ImageBlit, filter: Filter = Filter.LINEAR) =
        this.blitImage(src, srcLayout, dst, dstLayout, listOf(region), filter)

inline fun CommandBuffer.copyImage(block: CmdCopyImageDsl.() -> Unit): CommandBuffer {
    val dsl = CmdCopyImageDsl().apply(block)

    return this.copyImage(dsl.transfer.src, dsl.srcLayout, dsl.transfer.dst, dsl.dstLayout, dsl.regions)
}

inline fun CommandBuffer.blitImage(block: CmdBlitImageDsl.() -> Unit): CommandBuffer {
    val dsl = CmdBlitImageDsl().apply(block)

    return this.blitImage(dsl.transfer.src, dsl.srcLayout, dsl.transfer.dst, dsl.dstLayout, dsl.regions, dsl.filter)
}

inline fun CommandBuffer.begin(block: CmdBeginDsl.() -> Unit): CommandBuffer {
    val dsl = CmdBeginDsl().apply(block)

    return this.begin(dsl.flags, dsl.inheritanceInfo)
}

inline fun CommandBuffer.pipelineBarrier(block: CmdPipelineBarrierDsl.() -> Unit): CommandBuffer {
    val dsl = CmdPipelineBarrierDsl().apply(block)

    return this.pipelineBarrier(
            dsl.stage.initial, dsl.stage.final,
            dsl.dependencyFlags,
            dsl.memoryBarriers, dsl.bufferMemoryBarriers, dsl.imageMemoryBarriers)

}

inline fun CommandBuffer.copyBufferToImage(block: CmdCopyBufferToImageDsl.() -> Unit) =
        CmdCopyBufferToImageDsl().apply(block).copyBufferToImage(this)

inline fun CommandBuffer.copyBuffer(block: CmdCopyBufferDsl.() -> Unit) = apply {
    val dsl = CmdCopyBufferDsl().apply(block)

    copyBuffer(dsl.transfer.src, dsl.transfer.dst, dsl.regions)
}

inline fun CommandBuffer.beginRenderPass(block: CommandBufferRenderPassBeginInfoDsl.() -> Unit) : CommandBuffer {
    val dsl = CommandBufferRenderPassBeginInfoDsl().apply(block)

    return this.beginRenderPass(dsl.toCommandBufferRenderPassBeginInfo())
}