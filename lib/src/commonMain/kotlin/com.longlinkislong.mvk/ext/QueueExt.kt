package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.dsl.BatchQueueSubmitInfoDsl
import com.longlinkislong.mvk.dsl.QueuePresentInfoDsl
import com.longlinkislong.mvk.dsl.QueueSubmitInfoDsl

inline fun Queue.submit(block: QueueSubmitInfoDsl.() -> Unit) {
    val dsl = QueueSubmitInfoDsl().apply(block)

    submit(dsl.toQueueSubmitInfo(), dsl.fence)
}

inline fun Queue.submitBatch(block: BatchQueueSubmitInfoDsl.() -> Unit) {
    val dsl = BatchQueueSubmitInfoDsl().apply(block)

    submit(dsl.submitInfos, dsl.fence)
}

fun Queue.present(swapchain: Swapchain, imageIndex: Int, waitSemaphore: Semaphore? = null) {
    present(QueuePresentInfo(
            swapchains = listOf(swapchain),
            imageIndices = listOf(imageIndex),
            waitSemaphores = if (null == waitSemaphore) emptyList() else listOf(waitSemaphore)))
}

inline fun Queue.present(block: QueuePresentInfoDsl.() -> Unit) {
    val dsl = QueuePresentInfoDsl().apply(block)

    present(dsl.toQueuePresentInfo())
}