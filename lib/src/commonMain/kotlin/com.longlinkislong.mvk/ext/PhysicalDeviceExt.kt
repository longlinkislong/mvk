package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.Device
import com.longlinkislong.mvk.PhysicalDevice
import com.longlinkislong.mvk.dsl.DeviceCreateInfoDsl

inline fun PhysicalDevice.device(block: DeviceCreateInfoDsl.() -> Unit): Device {
    val dsl = DeviceCreateInfoDsl().apply(block)

    return createDevice(dsl.toCreateInfo())
}