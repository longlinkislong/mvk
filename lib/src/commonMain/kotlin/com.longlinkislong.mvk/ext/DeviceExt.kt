package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.*
import com.longlinkislong.mvk.dsl.*

inline fun Device.computePipeline(block: ComputePipelineCreateInfoDsl.() -> Unit): ComputePipeline {
    val dsl = ComputePipelineCreateInfoDsl().apply(block)

    return allocatePipeline(dsl.toCreateInfo())
}

inline fun Device.graphicsPipeline(renderPass: RenderPass, block: GraphicsPipelineCreateInfoDsl.() -> Unit): GraphicsPipeline {
    val dsl = GraphicsPipelineCreateInfoDsl().apply(block)

    return allocatePipeline(dsl.toCreateInfo(), renderPass)
}

inline fun Device.buffer(block: BufferCreateInfoDsl.() -> Unit): Buffer {
    val dsl = BufferCreateInfoDsl().apply(block)
    val buffer = createBuffer(dsl.toCreateInfo(), dsl.memoryProperties.toSet())

    dsl.init?.invoke(buffer)

    return buffer
}

inline fun Device.image(block: ImageCreateInfoDsl.() -> Unit): Image {
    val dsl = ImageCreateInfoDsl().apply(block)
    val image = createImage(dsl.toCreateInfo(), dsl.memoryProperties.toSet())

    dsl.init?.invoke(image)

    return image
}

inline fun Device.sampler(block: SamplerCreateInfoDsl.() -> Unit): Sampler {
    val dsl = SamplerCreateInfoDsl().apply(block)

    return allocateSampler(dsl.toSamplerCreateInfo())
}

inline fun Device.renderPass(block: RenderPassCreateInfoDsl.() -> Unit): RenderPass {
    val dsl = RenderPassCreateInfoDsl().apply(block)

    return createRenderPass(dsl.toRenderPassCreateInfo())
}

inline fun Device.swapchain(block: SwapchainCreateInfoDsl.() -> Unit): Swapchain {
    val dsl = SwapchainCreateInfoDsl().apply(block)

    return createSwapchain(dsl.toSwapchainCreateInfo())
}