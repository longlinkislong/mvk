package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.Buffer
import com.longlinkislong.mvk.Image
import com.longlinkislong.mvk.util.BufferToBufferCopy
import com.longlinkislong.mvk.util.BufferToImageCopy

infix fun Buffer.copyTo(that: Buffer) = BufferToBufferCopy(this, that)

infix fun Buffer.copyTo(that: Image) = BufferToImageCopy(this, that)