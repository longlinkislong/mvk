package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.Buffer
import com.longlinkislong.mvk.Image
import com.longlinkislong.mvk.util.ImageToBufferCopy
import com.longlinkislong.mvk.util.ImageToImageCopy

infix fun Image.copyTo(that: Buffer) = ImageToBufferCopy(this, that)

infix fun Image.copyTo(that: Image) = ImageToImageCopy(this, that)