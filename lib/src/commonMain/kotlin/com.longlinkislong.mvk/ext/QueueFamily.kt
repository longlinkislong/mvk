package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.QueueFamily
import com.longlinkislong.mvk.util.QueueFamilyTransfer

infix fun QueueFamily.transferTo(that: QueueFamily) = QueueFamilyTransfer(this, that)