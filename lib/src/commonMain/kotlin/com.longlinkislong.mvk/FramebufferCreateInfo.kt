package com.longlinkislong.mvk

data class FramebufferCreateInfo(
        val width: Int,
        val height: Int,
        val layers: Int = 1)