package com.longlinkislong.mvk

typealias ShaderStages = Set<ShaderStage>

val ShaderStages.bitfield: Int
    get() = ShaderStage.toBitfield(this)

