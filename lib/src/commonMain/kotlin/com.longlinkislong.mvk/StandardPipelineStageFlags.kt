package com.longlinkislong.mvk

object StandardPipelineStageFlags {
    val NONE: PipelineStageFlags = emptySet()
    val HOST: PipelineStageFlags = setOf(PipelineStageFlag.HOST)
    val TOP_OF_PIPE: PipelineStageFlags = setOf(PipelineStageFlag.TOP_OF_PIPE)
    val DRAW_INDIRECT: PipelineStageFlags = setOf(PipelineStageFlag.DRAW_INDIRECT)
    val VERTEX_INPUT: PipelineStageFlags = setOf(PipelineStageFlag.VERTEX_INPUT)
    val VERTEX_SHADER: PipelineStageFlags = setOf(PipelineStageFlag.VERTEX_SHADER)
    val TESSELLATION_CONTROL: PipelineStageFlags = setOf(PipelineStageFlag.TESSELLATION_CONTROL_SHADER)
    val TESSELLATION_EVALUATION: PipelineStageFlags = setOf(PipelineStageFlag.TESSELLATION_EVALUATION_SHADER)
    val GEOMETRY_SHADER: PipelineStageFlags = setOf(PipelineStageFlag.GEOMETRY_SHADER)
    val FRAGMENT_SHADER: PipelineStageFlags = setOf(PipelineStageFlag.FRAGMENT_SHADER)
    val EARLY_FRAGMENT_TESTS: PipelineStageFlags = setOf(PipelineStageFlag.EARLY_FRAGMENT_TESTS)
    val LATE_FRAGMENT_TESTS: PipelineStageFlags = setOf(PipelineStageFlag.LATE_FRAGMENT_TESTS)
    val COLOR_ATTACHMENT_OUTPUT: PipelineStageFlags = setOf(PipelineStageFlag.COLOR_ATTACHMENT_OUTPUT)
    val COMPUTE_SHADER: PipelineStageFlags = setOf(PipelineStageFlag.COMPUTE_SHADER)
    val TRANSFER: PipelineStageFlags = setOf(PipelineStageFlag.TRANSFER)
    val BOTTOM_OF_PIPE: PipelineStageFlags = setOf(PipelineStageFlag.BOTTOM_OF_PIPE)
}