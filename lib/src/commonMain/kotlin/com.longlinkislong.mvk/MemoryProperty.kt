package com.longlinkislong.mvk

enum class MemoryProperty(internal val value: Int) {
    DEVICE_LOCAL(0x1),
    HOST_VISIBLE(0x2),
    HOST_COHERENT(0x4),
    HOST_CACHED(0x8),
    LAZILY_ALLOCATED(0x10);


    companion object {
        internal fun toBitfield(flags: Set<MemoryProperty>): Int = flags.sumBy { it.value }
    }
}
