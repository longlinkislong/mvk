package com.longlinkislong.mvk

enum class SamplerAddressMode(internal val value: Int) {
    REPEAT(0),
    MIRRORED_REPEAT(1),
    CLAMP_TO_EDGE(2),
    CLAMP_TO_BORDER(3)
}
