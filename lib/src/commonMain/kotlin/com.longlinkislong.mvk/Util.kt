package com.longlinkislong.mvk

import kotlin.math.log2
import kotlin.math.max
import kotlin.math.min

object Util {
    fun alignUp(a: Long, b: Long) = (a + b - 1) / b * b

    fun alignDown(a: Long, b: Long) = a and -b

    fun clamp(value: Int, min: Int, max: Int) = max(min, min(max, value))

    fun vkAssert(result: Int, msg: String){
        if (0 != result) {
            throw RuntimeException(msg)
        }
    }

    fun vkAssert(result: Int){
        if (0 != result) {
            throw RuntimeException(translateVulkanResult(result))
        }
    }

    fun translateVulkanResult(result: Int) = when (result) {
        0 -> "Command successfully completed."
        1 -> "A fence or query has not yet completed."
        2 -> "A wait operation has not completed in the specified time."
        3 ->  "An event is signaled."
        4 -> "An event is unsignaled."
        5 -> "A return array was too small for the result."
        1000001003 -> "A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully."

        // Error codes
        -1 -> "A host memory allocation has failed."
        -2 -> "A device memory allocation has failed."
        -3 -> "Initialization of an object could not be completed for implementation-specific reasons."
        -4 -> "The logical or physical device has been lost."
        -5 -> "Mapping of a memory object has failed."
        -6 -> "A requested layer is not present or could not be loaded."
        -7 -> "A requested extension is not supported."
        -8 -> "A requested feature is not supported."
        -9 -> """
            The requested version of Vulkan is not supported by the driver or is otherwise incompatible for
            implementation-specific reasons.""".trimIndent()
        -10 -> "Too many objects of the type have already been created."
        -11 -> "A requested surfaceFormat is not supported on this device."
        -1000000000 -> "A surface is no longer available."
        -1000000001 -> "The requested window is already connected to a VkSurfaceKHR, or to some other non-Vulkan API."
        -1000001004 -> """
            A surface has changed in such a way that it is no
            longer compatible with the swapchain, and further presentation requests using the swapchain will
            fail. Applications must query the new surface properties and recreate their swapchain if they
            wish to continue" + "presenting to the surface.""".trimIndent()
        -1000003001 -> """
            The display used by a swapchain does not use the same presentable image layout, or is incompatible in a
            way that prevents sharing an image.""".trimIndent()
        -1000011001 -> "A validation layer found an error."
        else -> "Unknown result $result"
    }

    fun mipLevels(width: Int, height: Int): Int = log2(max(width, height).toDouble()).toInt()
}
