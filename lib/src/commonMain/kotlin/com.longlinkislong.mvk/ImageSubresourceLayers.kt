package com.longlinkislong.mvk

data class ImageSubresourceLayers(
        val aspectMask: AspectFlags = StandardAspectFlags.NONE,
        val mipLevel: Int = 0,
        val baseArrayLayer: Int = 0,
        val layerCount: Int = 1)
