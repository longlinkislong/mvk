package com.longlinkislong.mvk

interface InstanceLayer {
    val value: String

    companion object {
        fun of(name: String): InstanceLayer = object: InstanceLayer {
            override val value: String = name
        }
    }
}
