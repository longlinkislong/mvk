package com.longlinkislong.mvk

data class BufferCopy (
        val srcOffset: Long = 0L,
        val dstOffset: Long = 0L,
        val size: Long = -1L)
