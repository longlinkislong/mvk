package com.longlinkislong.mvk

data class CommandBufferInheritanceInfo(
        val renderPass: RenderPass? = null,
        val subpass: Int = 0,
        val framebuffer: Framebuffer? = null,
        val occlusionQueryEnable: Boolean = false,
        val queryFlags: Int = 0,
        val pipelineStatistics: Int = 0)