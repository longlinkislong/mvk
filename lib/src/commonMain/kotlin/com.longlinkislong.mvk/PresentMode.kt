package com.longlinkislong.mvk

enum class PresentMode(internal val value: Int) {
    IMMEDIATE(0),
    MAILBOX(1),
    FIFO(2),
    FIFO_RELAXED(3)
}
