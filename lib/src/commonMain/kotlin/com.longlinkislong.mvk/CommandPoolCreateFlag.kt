package com.longlinkislong.mvk

enum class CommandPoolCreateFlag(internal val value: Int) {
    CREATE_TRANSIENT(0x1),
    CREATE_RESET_COMMAND_BUFFER(0x2),
    CREATE_PROTECTED(0x4);

    fun toCommandPoolCreateFlags(): CommandPoolCreateFlags = setOf(this)

    companion object {

        internal fun toBitfield(flags: Set<CommandPoolCreateFlag>): Int = flags.sumBy { it.value }
    }
}
