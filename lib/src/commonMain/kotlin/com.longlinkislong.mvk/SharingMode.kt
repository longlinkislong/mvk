package com.longlinkislong.mvk

enum class SharingMode(internal val value: Int) {
    EXCLUSIVE(0),
    CONCURRENT(1)
}
