package com.longlinkislong.mvk

/**
 * Information used to describe how to wait for a new Command.
 *
 * @param semaphore is the Semaphore to trigger when the command is ready to execute
 * @param stageFlags is the Set of flags to determine where to wait for the Semaphore
 */
data class QueueSubmitWaitInfo(
        val semaphore: Semaphore,
        val stageFlags: PipelineStageFlags = StandardPipelineStageFlags.TOP_OF_PIPE)