package com.longlinkislong.mvk

enum class PolygonMode(internal val value: Int) {
    FILL(0),
    LINE(1),
    POINT(2)
}
