package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.PipelineStageTransition

enum class PipelineStageFlag(internal val value: Int) {
    HOST(0x4000),
    TOP_OF_PIPE(0x1),
    DRAW_INDIRECT(0x2),
    VERTEX_INPUT(0x4),
    VERTEX_SHADER(0x8),
    TESSELLATION_CONTROL_SHADER(0x10),
    TESSELLATION_EVALUATION_SHADER(0x20),
    GEOMETRY_SHADER(0x40),
    FRAGMENT_SHADER(0x80),
    EARLY_FRAGMENT_TESTS(0x100),
    LATE_FRAGMENT_TESTS(0x200),
    COLOR_ATTACHMENT_OUTPUT(0x400),
    COMPUTE_SHADER(0x800),
    TRANSFER(0x1000),
    BOTTOM_OF_PIPE(0x2000);

    fun toPipelineStageFlags(): PipelineStageFlags = setOf(this)

    infix fun until(that: PipelineStageFlag) = PipelineStageTransition(setOf(this), setOf(that))

    infix fun until(that: PipelineStageFlags) = PipelineStageTransition(setOf(this), that)

    fun inPlace() = PipelineStageTransition(setOf(this), setOf(this))

    companion object {

        internal fun toBitfield(stageMask: Set<PipelineStageFlag>): Int = stageMask.sumBy { it.value }
    }
}
