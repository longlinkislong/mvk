package com.longlinkislong.mvk

expect interface Pipeline : ManagedResource {
    val bindPoint: PipelineBindPoint
    val device: Device
    val descriptorSetLayouts: List<DescriptorSetLayout>
    val pipelineLayout: PipelineLayout
}