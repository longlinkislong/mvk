package com.longlinkislong.mvk

enum class DependencyFlag(internal val value: Int) {
    BY_REGION(0x1),
    VIEW_LOCAL(0x2),
    DEVICE_GROUP(0x4);

    fun toDependencyFlags(): DependencyFlags = setOf(this)

    companion object {

        internal fun toBitfield(flags: Set<DependencyFlag>): Int = flags.sumBy { it.value }
    }
}
