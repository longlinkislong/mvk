package com.longlinkislong.mvk

data class GraphicsPipelineCreateInfo(
        val flags: Int = 0,
        val stages: List<PipelineShaderStageCreateInfo> = emptyList(),
        val layoutInfo: PipelineLayoutCreateInfo,
        val colorBlendState: PipelineColorBlendStateCreateInfo? = null,
        val depthStencilState: PipelineDepthStencilStateCreateInfo = PipelineDepthStencilStateCreateInfo.DEFAULT,
        val inputAssemblyState: PipelineInputAssemblyStateCreateInfo,
        val multisampleState: PipelineMultisampleStateCreateInfo = PipelineMultisampleStateCreateInfo.DEFAULT,
        val rasterizationState: PipelineRasterizationStateCreateInfo = PipelineRasterizationStateCreateInfo.DEFAULT,
        val tessellationState: PipelineTessellationStateCreateInfo? = null,
        val vertexInputState: PipelineVertexInputStateCreateInfo = PipelineVertexInputStateCreateInfo.DEFAULT,
        val subpass: Int)