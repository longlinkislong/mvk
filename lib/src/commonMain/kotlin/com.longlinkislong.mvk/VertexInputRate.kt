package com.longlinkislong.mvk

enum class VertexInputRate(internal val value: Int) {
    PER_VERTEX(0),
    PER_INSTANCE(1)
}
