package com.longlinkislong.mvk

data class PipelineColorBlendAttachmentState(
        val blendEnable: Boolean = false,
        val srcColorBlendFactor: BlendFactor = BlendFactor.ONE,
        val dstColorBlendFactor: BlendFactor = BlendFactor.ZERO,
        val colorBlendOp: BlendOp = BlendOp.ADD,
        val srcAlphaBlendFactor: BlendFactor = BlendFactor.ONE,
        val dstAlphaBlendFactor: BlendFactor = BlendFactor.ZERO,
        val alphaBlendOp: BlendOp = BlendOp.ADD,
        val colorWriteMask: Int = 0xF)