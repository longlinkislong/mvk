package com.longlinkislong.mvk

expect class Swapchain : Resource {
    val info: SwapchainCreateInfo

    val device: Device

    val width: Int
    val height: Int
    val images: List<Image>

    fun acquireNextImage(): SwapchainBackbuffer
    fun resize(width: Int, height: Int)
}