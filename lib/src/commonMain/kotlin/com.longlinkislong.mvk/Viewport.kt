package com.longlinkislong.mvk

data class Viewport(
        val x: Float = 0.0f,
        val y: Float = 0.0f,
        val width: Float = 1.0f,
        val height: Float = 1.0f,
        val minDepth: Float = 0.0f,
        val maxDepth: Float = 1.0f)
