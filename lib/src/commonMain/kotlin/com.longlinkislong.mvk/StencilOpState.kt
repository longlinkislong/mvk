package com.longlinkislong.mvk

data class StencilOpState(
        val failOp: StencilOp = StencilOp.REPLACE,
        val passOp: StencilOp = StencilOp.REPLACE,
        val depthFailOp: StencilOp = StencilOp.REPLACE,
        val compareOp: CompareOp = CompareOp.ALWAYS,
        val compareMask: Int = 0.inv(),
        val writeMask: Int = 0.inv(),
        val reference: Int = 0.inv())