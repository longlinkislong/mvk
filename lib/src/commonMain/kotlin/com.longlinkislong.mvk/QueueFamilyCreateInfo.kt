package com.longlinkislong.mvk

data class QueueFamilyCreateInfo(
        val index: Int,
        val queueFlags: QueueFlags,
        val queueCount: Int)