package com.longlinkislong.mvk

enum class Vendor {
    AMD,
    NVIDIA,
    INTEL,
    IMGTEC,
    ARM,
    QUALCOMM,
    UNKNOWN
}
