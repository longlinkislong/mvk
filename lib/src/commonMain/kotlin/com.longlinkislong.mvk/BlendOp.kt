package com.longlinkislong.mvk

enum class BlendOp(internal val value: Int) {
    ADD(0),
    SUBTRACT(1),
    REVERSE_SUBTRACT(2),
    MIN(3),
    MAX(4)
}
