package com.longlinkislong.mvk

expect class ComputePipeline : Pipeline {
    val info: ComputePipelineCreateInfo
}