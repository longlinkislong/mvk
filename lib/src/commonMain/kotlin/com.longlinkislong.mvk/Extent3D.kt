package com.longlinkislong.mvk

data class Extent3D(
        val width: Int,
        val height: Int,
        val depth: Int) {

    fun toOffset3D() = Offset3D(
            x = width,
            y = height,
            z = depth)

    companion object {
        val ZERO = Extent3D(0, 0, 0)
    }
}
