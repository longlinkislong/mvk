package com.longlinkislong.mvk

data class VertexInputBindingDescription(
        val binding: Int = 0,
        val stride: Int = 0,
        val inputRate: VertexInputRate = VertexInputRate.PER_VERTEX)