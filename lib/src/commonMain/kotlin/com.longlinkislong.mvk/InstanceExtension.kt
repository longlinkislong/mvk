package com.longlinkislong.mvk

interface InstanceExtension {
    val value: String

    companion object {
        fun of(name: String) = object: InstanceExtension {
            override val value: String = name
        }
    }
}