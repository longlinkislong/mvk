package com.longlinkislong.mvk

data class MemoryBarrier (
        val srcAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val dstAccessMask: AccessFlags = StandardAccessFlags.NONE)