package com.longlinkislong.mvk

typealias QueueFlags = Set<QueueFlag>

val QueueFlags.bitfield: Int
    get() = QueueFlag.toBitfield(this)

