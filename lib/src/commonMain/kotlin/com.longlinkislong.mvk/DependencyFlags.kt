package com.longlinkislong.mvk

typealias DependencyFlags = Set<DependencyFlag>

val DependencyFlags.bitfield: Int
    get() = DependencyFlag.toBitfield(this)

