package com.longlinkislong.mvk

typealias MemoryProperties = Set<MemoryProperty>

val MemoryProperties.bitfield: Int
    get() = MemoryProperty.toBitfield(this)

