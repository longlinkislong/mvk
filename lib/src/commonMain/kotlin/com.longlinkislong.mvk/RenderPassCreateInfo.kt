package com.longlinkislong.mvk

data class RenderPassCreateInfo(
        val flags: Int = 0,
        val attachments: List<AttachmentDescription> = emptyList(),
        val subpasses: List<SubpassDescription> = emptyList(),
        val dependencies: List<SubpassDependency> = emptyList())