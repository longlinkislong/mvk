package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.ImageLayoutTransition

enum class ImageLayout(internal val value: Int) {
    UNDEFINED(0),
    GENERAL(1),
    COLOR_ATTACHMENT(2),
    DEPTH_STENCIL_ATTACHMENT(3),
    DEPTH_STENCIL_READ_ONLY(4),
    SHADER_READ_ONLY(5),
    PREINITIALIZED(8),
    TRANSFER_SRC(6),
    TRANSFER_DST(7),
    PRESENT_SRC_KHR(1000001002);

    infix fun transitionTo(that: ImageLayout) = ImageLayoutTransition(this, that)

    fun inPlace() = ImageLayoutTransition(this, this)
}
