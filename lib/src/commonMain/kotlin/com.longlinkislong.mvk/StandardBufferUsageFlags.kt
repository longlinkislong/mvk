package com.longlinkislong.mvk

object StandardBufferUsageFlags {
    val NONE: BufferUsageFlags = emptySet()
    val INDEX_BUFFER_FROM_TRANSFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_DST, BufferUsageFlag.INDEX_BUFFER)
    val VERTEX_BUFFER_FROM_TRANSFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_DST, BufferUsageFlag.VERTEX_BUFFER)
    val STORAGE_BUFFER_FROM_TRANSFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_DST, BufferUsageFlag.STORAGE_BUFFER)
    val UNIFORM_BUFFER_FROM_TRANSFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_DST, BufferUsageFlag.UNIFORM_BUFFER)

    val TRANSFER_BUFFER_FROM_STORAGE: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_SRC, BufferUsageFlag.STORAGE_BUFFER)

    val VERTEX_BUFFER_FROM_STORAGE: BufferUsageFlags = setOf(BufferUsageFlag.STORAGE_BUFFER, BufferUsageFlag.VERTEX_BUFFER)
    val INDEX_BUFFER_FROM_STORAGE: BufferUsageFlags = setOf(BufferUsageFlag.STORAGE_BUFFER, BufferUsageFlag.INDEX_BUFFER)

    val READ_ONLY_TRANSFER_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_SRC)
    val WRITE_ONLY_TRANSFER_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_DST)
    val TRANSFER_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.TRANSFER_SRC, BufferUsageFlag.TRANSFER_DST)
    val UNIFORM_TEXEL_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.UNIFORM_TEXEL_BUFFER)
    val STORAGE_TEXEL_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.STORAGE_TEXEL_BUFFER)
    val UNIFORM_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.UNIFORM_BUFFER)
    val STORAGE_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.STORAGE_BUFFER)
    val INDEX_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.INDEX_BUFFER)
    val VERTEX_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.VERTEX_BUFFER)
    val INDIRECT_BUFFER: BufferUsageFlags = setOf(BufferUsageFlag.INDIRECT_BUFFER)
}