package com.longlinkislong.mvk

data class BufferViewCreateInfo(
        val flags: Int = 0,
        val format: Format,
        val offset: Long = 0L,
        val range: Long = -1L)