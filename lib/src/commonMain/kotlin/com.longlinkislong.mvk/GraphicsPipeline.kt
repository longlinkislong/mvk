package com.longlinkislong.mvk

expect class GraphicsPipeline : Pipeline {
    val info: GraphicsPipelineCreateInfo
}