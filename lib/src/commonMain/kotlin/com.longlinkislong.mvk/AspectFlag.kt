package com.longlinkislong.mvk

enum class AspectFlag(internal val value: Int) {
    COLOR(0x1),
    DEPTH(0x2),
    STENCIL(0x4);

    fun toAspectFlags(): AspectFlags = setOf(this)

    companion object {
        internal fun toBitfield(aspectMask: Set<AspectFlag>): Int = aspectMask.sumBy { it.value }

        internal fun toEnumSet(bitfield: Int): Set<AspectFlag> = values()
                .asSequence()
                .filter { 0 != bitfield and it.value }
                .toSet()
    }
}
