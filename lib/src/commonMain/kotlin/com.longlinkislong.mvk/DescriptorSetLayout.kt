package com.longlinkislong.mvk

expect class DescriptorSetLayout : ManagedResource {
    val device: Device
    val info: DescriptorSetLayoutCreateInfo

    fun allocate(): DescriptorSet
    fun allocate(nSets: Int): List<DescriptorSet>
}