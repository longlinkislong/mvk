package com.longlinkislong.mvk

enum class BorderColor(internal val value: Int) {
    FLOAT_TRANSPARENT_BLACK(0),
    INT_TRANSPARENT_BLACK(1),
    FLOAT_OPAQUE_BLACK(2),
    INT_OPAQUE_BLACK(3),
    FLOAT_OPAQUE_WHITE(4),
    INT_OPAQUE_WHITE(5)
}
