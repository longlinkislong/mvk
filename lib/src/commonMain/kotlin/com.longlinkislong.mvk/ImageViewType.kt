package com.longlinkislong.mvk

enum class ImageViewType(internal val value: Int) {
    VIEW_1D(0),
    VIEW_2D(1),
    VIEW_3D(2),
    CUBE(3),
    VIEW_1D_ARRAY(4),
    VIEW_2D_ARRAY(5),
    CUBE_ARRAY(6)
}
