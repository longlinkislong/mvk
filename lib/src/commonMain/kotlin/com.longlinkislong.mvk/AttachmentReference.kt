package com.longlinkislong.mvk

data class AttachmentReference(
        val attachment: Int,
        val layout: ImageLayout)