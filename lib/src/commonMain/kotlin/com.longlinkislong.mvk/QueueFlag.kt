package com.longlinkislong.mvk

enum class QueueFlag(internal val value: Int) {
    GRAPHICS(0x1),
    COMPUTE(0x2),
    TRANSFER(0x4),
    SPARSE_BINDING(0x8),
    PROTECTED(0x10);

    fun toQueueFlags(): QueueFlags = setOf(this)

    companion object {
        internal fun toBitfield(bitset: Set<QueueFlag>): Int = bitset.sumBy { it.value }

        internal fun toEnumSet(bitfield: Int): Set<QueueFlag> {
            return values().asSequence()
                    .filter { 0 != bitfield and it.value }
                    .toSet()
        }
    }
}
