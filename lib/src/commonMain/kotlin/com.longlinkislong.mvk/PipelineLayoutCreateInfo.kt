package com.longlinkislong.mvk

data class PipelineLayoutCreateInfo(
        val flags: Int = 0,
        val pushConstantRanges: List<PushConstantRange> = emptyList(),
        val setLayoutInfos: List<DescriptorSetLayoutCreateInfo> = emptyList())