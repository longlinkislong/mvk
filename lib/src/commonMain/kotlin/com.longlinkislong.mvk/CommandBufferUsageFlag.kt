package com.longlinkislong.mvk

enum class CommandBufferUsageFlag(internal val value: Int) {
    ONE_TIME_SUBMIT(0x1),
    RENDER_PASS_CONTINUE(0x2),
    SIMULTANEOUS_USE(0x4);

    fun toCommandBufferUsageFlags(): CommandBufferUsageFlags = setOf(this)

    companion object {

        internal fun toBitfield(flags: Set<CommandBufferUsageFlag>): Int = flags.sumBy { it.value }
    }
}
