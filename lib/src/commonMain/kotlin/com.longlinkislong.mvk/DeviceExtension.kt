package com.longlinkislong.mvk

interface DeviceExtension {
    val value: String

    companion object {
        fun of(name: String) = object: DeviceExtension {
            override val value: String = name
        }
    }
}