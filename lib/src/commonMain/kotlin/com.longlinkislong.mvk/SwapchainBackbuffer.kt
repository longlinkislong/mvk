package com.longlinkislong.mvk

data class SwapchainBackbuffer(
        val image: Image,
        val acquireSemaphore: Semaphore,
        val index: Int)