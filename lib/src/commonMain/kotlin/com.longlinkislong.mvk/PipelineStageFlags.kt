package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.PipelineStageTransition

typealias PipelineStageFlags = Set<PipelineStageFlag>

val PipelineStageFlags.bitfield: Int
    get() = PipelineStageFlag.toBitfield(this)

fun PipelineStageFlags.inPlace() = PipelineStageTransition(this, this)

infix fun PipelineStageFlags.until(that: PipelineStageFlags) = PipelineStageTransition(this, that)

infix fun PipelineStageFlags.until(that: PipelineStageFlag) = PipelineStageTransition(this, setOf(that))

