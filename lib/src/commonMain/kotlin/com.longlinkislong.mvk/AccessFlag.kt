package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.AccessTransition

/**
 * Flags that specify memory access types
 * @see [Vulkan 1.1 Document](https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkAccessFlagBits.html)
 */
enum class AccessFlag(internal val value: Int) {
    INDIRECT_COMMAND_READ(0x1),
    INDEX_READ(0x2),
    VERTEX_ATTRIBUTE_READ(0x4),
    UNIFORM_READ(0x8),
    INPUT_ATTACHMENT_READ(0x10),
    SHADER_READ(0x20),
    SHADER_WRITE(0x40),
    COLOR_ATTACHMENT_READ(0x80),
    COLOR_ATTACHMENT_WRITE(0x100),
    DEPTH_STENCIL_ATTACHMENT_READ(0x200),
    DEPTH_STENCIL_ATTACHMENT_WRITE(0x400),
    TRANSFER_READ(0x800),
    TRANSFER_WRITE(0x1000),
    HOST_READ(0x2000),
    HOST_WRITE(0x4000),
    MEMORY_READ(0x8000),
    MEMORY_WRITE(0x10000);

    infix fun transitionTo(that: AccessFlag) = AccessTransition(setOf(this), setOf(that))

    infix fun transitionTo(that: AccessFlags) = AccessTransition(setOf(this), that)

    fun inPlace() = AccessTransition(setOf(this), setOf(this))

    companion object {
        internal fun toBitfield(accessMask: Set<AccessFlag>): Int = accessMask.sumBy { it.value }
    }
}
