package com.longlinkislong.mvk

data class BufferImageCopy(
        val bufferOffset: Long = 0L,
        val bufferRowLength: Int = 0,
        val bufferImageHeight: Int = 0,
        val imageSubresource: ImageSubresourceLayers,
        val imageOffset: Offset3D = Offset3D.ZERO,
        val imageExtent: Extent3D)