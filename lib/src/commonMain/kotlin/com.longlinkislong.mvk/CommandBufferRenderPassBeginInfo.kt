package com.longlinkislong.mvk

data class CommandBufferRenderPassBeginInfo(
        val framebuffer: Framebuffer,
        val renderPass: RenderPass,
        val subpassContents: SubpassContents = SubpassContents.INLINE,
        val renderArea: Rect2D,
        val clearValues: List<ClearValue> = emptyList())