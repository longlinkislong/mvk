package com.longlinkislong.mvk

data class ImageSubresourceRange(
        val aspectMask: AspectFlags = StandardAspectFlags.NONE,
        val baseMipLevel: Int = 0,
        val levelCount: Int = 0,
        val baseArrayLayer: Int = 0,
        val layerCount: Int = 0)
