package com.longlinkislong.mvk

enum class ColorSpace(internal val value: Int) {
    SRGB_NONLINEAR(0);

    companion object {
        fun toColorSpace(value: Int): ColorSpace? = values().firstOrNull { it.value == value }
    }
}
