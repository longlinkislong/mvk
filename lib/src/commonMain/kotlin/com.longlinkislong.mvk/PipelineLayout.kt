package com.longlinkislong.mvk

expect class PipelineLayout : ManagedResource {
    val info: PipelineLayoutCreateInfo
    val device: Device
    val descriptorSetLayouts: List<DescriptorSetLayout>
}