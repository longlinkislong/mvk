package com.longlinkislong.mvk

object StandardShaderStages {
    val NONE: ShaderStages = emptySet()
    val VERTEX: ShaderStages = setOf(ShaderStage.VERTEX)
    val TESSELLATION_CONTROL: ShaderStages = setOf(ShaderStage.TESSELLATION_CONTROL)
    val TESSELLATION_EVALUATION: ShaderStages = setOf(ShaderStage.TESSELLATION_EVALUATION)
    val GEOMETRY: ShaderStages = setOf(ShaderStage.GEOMETRY)
    val FRAGMENT: ShaderStages = setOf(ShaderStage.FRAGMENT)
    val COMPUTE: ShaderStages = setOf(ShaderStage.COMPUTE)
    val ALL_GRAPHICS: ShaderStages = setOf(ShaderStage.VERTEX, ShaderStage.TESSELLATION_CONTROL, ShaderStage.TESSELLATION_EVALUATION, ShaderStage.GEOMETRY, ShaderStage.FRAGMENT)
}