package com.longlinkislong.mvk

enum class StoreOp(internal val value: Int) {
    DONT_CARE(1),
    STORE(0)
}
