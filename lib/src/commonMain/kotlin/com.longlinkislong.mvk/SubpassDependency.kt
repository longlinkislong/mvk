package com.longlinkislong.mvk

class SubpassDependency(
        val srcSubpass: Int = SUBPASS_EXTERNAL,
        val dstSubpass: Int = SUBPASS_EXTERNAL,
        val srcStageMask: PipelineStageFlags = StandardPipelineStageFlags.BOTTOM_OF_PIPE,
        val dstStageMask: PipelineStageFlags = StandardPipelineStageFlags.BOTTOM_OF_PIPE,
        val srcAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val dstAccessMask: AccessFlags = StandardAccessFlags.NONE,
        val dependencyFlags: DependencyFlags = StandardDependencyFlags.BY_REGION) {

    companion object {
        const val SUBPASS_EXTERNAL = 0.inv()
    }
}
