package com.longlinkislong.mvk

data class DescriptorSetLayoutBinding(
        val binding: Int,
        val descriptorType: DescriptorType,
        val descriptorCount: Int = 1,
        val stages: ShaderStages = StandardShaderStages.NONE,
        val immutableSamplers: List<Sampler> = emptyList())