package com.longlinkislong.mvk

enum class LoadOp(internal val value: Int) {
    DONT_CARE(2),
    LOAD(0),
    CLEAR(1)
}
