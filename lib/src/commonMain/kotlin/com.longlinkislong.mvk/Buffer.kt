package com.longlinkislong.mvk

expect class Buffer : Resource {
    val device: Device
    val info: BufferCreateInfo

    /**
     * Retrieves the raw pointer of the mapped data. This will map the buffer if it currently is not mapped.
     * This method is thread-safe.
     * @return the raw pointer
     */
    fun mapAddress(): Long

    /**
     * Unmaps the Buffer.
     */
    fun unmap()

    /**
     * User-defined data
     */
    var userData: Any?
}