package com.longlinkislong.mvk

data class ImageBlit(
        val srcOffset0: Offset3D,
        val srcOffset1: Offset3D,
        val dstOffset0: Offset3D,
        val dstOffset1: Offset3D,
        val srcSubresource: ImageSubresourceLayers,
        val dstSubresource: ImageSubresourceLayers)