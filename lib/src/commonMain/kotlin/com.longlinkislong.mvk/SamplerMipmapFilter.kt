package com.longlinkislong.mvk

enum class SamplerMipmapFilter(internal val value: Int) {
    NEAREST(0),
    LINEAR(1)
}
