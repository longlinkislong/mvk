package com.longlinkislong.mvk

data class ImageCopy(
        val srcOffset: Offset3D = Offset3D.ZERO,
        val dstOffset: Offset3D = Offset3D.ZERO,
        val extent: Extent3D,
        val srcSubresource: ImageSubresourceLayers,
        val dstSubresource: ImageSubresourceLayers)