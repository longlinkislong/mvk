package com.longlinkislong.mvk

enum class SubpassContents(internal val value: Int) {
    INLINE(0),
    SECONDARY_COMMAND_BUFFERS(1)
}
