package com.longlinkislong.mvk

data class Rect2D(
        val offset: Offset2D = Offset2D.ZERO,
        val extent: Extent2D) {

    companion object {
        val ZERO = Rect2D(Offset2D.ZERO, Extent2D.ZERO)
    }
}
