package com.longlinkislong.mvk

enum class IndexType(internal val value: Int) {
    UINT16(0),
    UINT32(1)
}
