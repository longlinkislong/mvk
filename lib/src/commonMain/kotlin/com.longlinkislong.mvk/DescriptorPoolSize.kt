package com.longlinkislong.mvk

data class DescriptorPoolSize(
        val type: Int,
        val descriptorCount: Int)