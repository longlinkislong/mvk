package com.longlinkislong.mvk

data class Offset2D(
        val x: Int,
        val y: Int) {

    companion object {
        val ZERO = Offset2D(0, 0)
    }
}
