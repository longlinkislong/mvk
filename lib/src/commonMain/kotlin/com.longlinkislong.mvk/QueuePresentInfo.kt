package com.longlinkislong.mvk

data class QueuePresentInfo(
        val waitSemaphores: List<Semaphore>,
        val swapchains: List<Swapchain>,
        val imageIndices: List<Int>)