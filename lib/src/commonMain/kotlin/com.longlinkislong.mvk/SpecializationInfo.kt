package com.longlinkislong.mvk

data class SpecializationInfo(
        val mapEntries: List<SpecializationMapEntry> = emptyList(),
        val data: Long,
        val size: Int)