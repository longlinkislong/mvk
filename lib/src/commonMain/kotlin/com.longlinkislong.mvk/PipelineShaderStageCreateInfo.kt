package com.longlinkislong.mvk

class PipelineShaderStageCreateInfo(
        val flags: Int = 0,
        val stage: ShaderStage,
        val moduleInfo: ShaderModuleCreateInfo,
        val name: String = "main",
        val specializationInfo: SpecializationInfo? = null)
