package com.longlinkislong.mvk

expect class QueueFamily {
    val device: Device
    val info: QueueFamilyCreateInfo

    /**
     * Retrieves the next Queue index in round-robin form. This method is atomic in determining the next Queue index.
     */
    val nextQueueIndex: Int

    /**
     * A Thread-Local CommandPool that allocates CommandBuffers ready to be used with any Queue of this QueueFamily.
     */
    val currentCommandPool: CommandPool

    /**
     * Retrieves the next Queue in round-robin form. This method calls `getNextQueueIndex` to determine the
     * index of the next Queue. This method is atomic in determining the next Queue.
     */
    val nextQueue: Queue

    /**
     * All of the Queues that belong to this QueueFamily.
     */
    val queues: List<Queue>

    /**
     * Checks if the Queues of this QueueFamily can present the supplied Surface.
     * @param surface the Surface to check for presentation.
     * @return true if the QueueFamily supports present.
     */
    fun canPresent(surface: Long): Boolean

    /**
     * Disassociates the current Thread from this QueueFamily. This will free any Thread-Local data allocated by this
     * QueueFamily.
     */
    fun detach()

    /**
     * Creates a new CommandPool to be used with any Queue of this QueueFamily.
     * @param flags flags used to construct the CommandPool.
     * @return the CommandPool.
     */
    fun createCommandPool(flags: Set<CommandPoolCreateFlag>): CommandPool
}