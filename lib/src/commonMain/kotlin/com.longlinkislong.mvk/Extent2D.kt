package com.longlinkislong.mvk

data class Extent2D(
        val width: Int,
        val height: Int) {

    fun toExtent3D() = Extent3D(width, height, 1)

    companion object {
        val ZERO = Extent2D(0, 0)
    }
}
