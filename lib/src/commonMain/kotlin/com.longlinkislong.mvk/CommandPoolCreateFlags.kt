package com.longlinkislong.mvk

typealias CommandPoolCreateFlags = Set<CommandPoolCreateFlag>

val CommandPoolCreateFlags.bitfield: Int
    get() = CommandPoolCreateFlag.toBitfield(this)

