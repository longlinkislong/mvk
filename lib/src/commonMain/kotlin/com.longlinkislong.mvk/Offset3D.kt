package com.longlinkislong.mvk

data class Offset3D(
        val x: Int,
        val y: Int,
        val z: Int) {

    companion object {
        var ZERO = Offset3D(0, 0, 0)
    }
}
