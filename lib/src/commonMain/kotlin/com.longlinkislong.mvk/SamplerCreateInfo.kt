package com.longlinkislong.mvk

data class SamplerCreateInfo(
        val flags: Int = 0,
        val minFilter: Filter = Filter.NEAREST,
        val magFilter: Filter = Filter.NEAREST,
        val mipmapFilter: SamplerMipmapFilter = SamplerMipmapFilter.NEAREST,
        val addressModeU: SamplerAddressMode = SamplerAddressMode.REPEAT,
        val addressModeV: SamplerAddressMode = SamplerAddressMode.REPEAT,
        val addressModeW: SamplerAddressMode = SamplerAddressMode.REPEAT,
        val mipLodBias: Float = 0.0f,
        val anistropyEnable: Boolean = false,
        val maxAnisotropy: Float = 1.0f,
        val compareEnable: Boolean = false,
        val compareOp: CompareOp = CompareOp.ALWAYS,
        val maxLod: Float = 1000.0f,
        val minLod: Float = -1000.0f,
        val borderColor: BorderColor = BorderColor.FLOAT_TRANSPARENT_BLACK,
        val unnormalizedCoordinates: Boolean = false)