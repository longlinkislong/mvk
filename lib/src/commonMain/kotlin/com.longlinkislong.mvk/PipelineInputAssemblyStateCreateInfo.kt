package com.longlinkislong.mvk

data class PipelineInputAssemblyStateCreateInfo(
        val flags: Int = 0,
        val topology: PrimitiveTopology,
        val primitiveRestartEnable: Boolean = false)