package com.longlinkislong.mvk

enum class GoogleLayer private constructor(override val value: String) : InstanceLayer {
    THREADING("VK_LAYER_GOOGLE_threading"),
    UNIQUE_OBJECTS("VK_LAYER_GOOGLE_unique_objects")
}
