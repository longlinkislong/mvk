package com.longlinkislong.mvk

data class PipelineDepthStencilStateCreateInfo(
        val flags: Int = 0,
        val depthTestEnable: Boolean = false,
        val depthWriteEnable: Boolean = false,
        val depthCompareOp: CompareOp = CompareOp.LESS,
        val depthBoundsTestEnable: Boolean = false,
        val stencilTestEnable: Boolean = false,
        val front: StencilOpState = StencilOpState(),
        val back: StencilOpState = StencilOpState(),
        val minDepthBounds: Float = 0.0f,
        val maxDepthBounds: Float = 0.0f) {

    companion object {
        val DEFAULT = PipelineDepthStencilStateCreateInfo()
    }
}
