package com.longlinkislong.mvk

data class ShaderModuleCreateInfo(
        val flags: Int = 0,
        val pData: Long,
        val size: Int)