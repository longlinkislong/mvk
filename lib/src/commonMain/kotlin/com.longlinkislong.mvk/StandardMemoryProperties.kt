package com.longlinkislong.mvk

object StandardMemoryProperties {
    val NONE: MemoryProperties = emptySet()
    val DEVICE: MemoryProperties = setOf(MemoryProperty.DEVICE_LOCAL)
    val SYSTEM: MemoryProperties = setOf(MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT)
    val SYSTEM_CACHED: MemoryProperties = setOf(MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT, MemoryProperty.HOST_CACHED)
    val AMD_PINNED: MemoryProperties = setOf(MemoryProperty.DEVICE_LOCAL, MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT)
    val UNIFIED: MemoryProperties = setOf(MemoryProperty.DEVICE_LOCAL, MemoryProperty.HOST_VISIBLE, MemoryProperty.HOST_COHERENT)
}