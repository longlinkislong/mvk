package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.alignDown
import com.longlinkislong.mvk.Util.alignUp
import com.longlinkislong.mvk.dsl.DeviceCreateInfoDsl
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*

actual class PhysicalDevice internal constructor(val handle: VkPhysicalDevice) {
    val pvkProperties: VkPhysicalDeviceProperties
    val pvkFeatures: VkPhysicalDeviceFeatures
    val pvkMemoryProperties: VkPhysicalDeviceMemoryProperties

    actual val hasUnifiedMemory: Boolean
    actual val physicalDeviceType: PhysicalDeviceType
    actual val preferredDepthStencilFormat: Format
    actual val totalDeviceRam: Long
    actual val vendor: Vendor

    init {
        this.pvkProperties = VkPhysicalDeviceProperties.create().apply { VK10.vkGetPhysicalDeviceProperties(handle, this) }
        this.pvkFeatures = VkPhysicalDeviceFeatures.create().apply { VK10.vkGetPhysicalDeviceFeatures(handle, this) }
        this.pvkMemoryProperties = VkPhysicalDeviceMemoryProperties.create().apply { VK10.vkGetPhysicalDeviceMemoryProperties(handle, this) }

        this.preferredDepthStencilFormat = stackLet { mem ->
            val pvkFormatProperties = VkFormatProperties.callocStack(mem)

            sequenceOf(Format.D24_UNORM_S8_UINT, Format.D32_SFLOAT_S8_UINT, Format.D16_UNORM_S8_UINT)
                    .filter {
                        VK10.vkGetPhysicalDeviceFormatProperties(handle, it.value, pvkFormatProperties)

                        0 != pvkFormatProperties.optimalTilingFeatures() and VK10.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
                    }
                    .first()
        }

        this.vendor = when (this.pvkProperties.vendorID()) {
            0x1002 -> Vendor.AMD
            0x1010 -> Vendor.IMGTEC
            0x10DE -> Vendor.NVIDIA
            0x13B5 -> Vendor.ARM
            0x5143 -> Vendor.QUALCOMM
            0x8086 -> Vendor.INTEL
            else -> Vendor.UNKNOWN
        }

        this.hasUnifiedMemory = run {
            val memoryTypes = this.pvkMemoryProperties.memoryTypeCount()
            val umFlags = StandardMemoryProperties.UNIFIED.bitfield
            var hasUM = false

            for (i in 0 until memoryTypes) {
                if (0 != umFlags and this.pvkMemoryProperties.memoryTypes(i).propertyFlags()) {
                    hasUM = true
                    break
                }
            }

            hasUM
        }

        this.totalDeviceRam = this.pvkMemoryProperties.memoryHeaps().asSequence()
                .filter { heap -> 0 != heap.flags() and VK10.VK_MEMORY_HEAP_DEVICE_LOCAL_BIT }
                .map { it.size() }
                .sum()

        val deviceType = this.pvkProperties.deviceType()

        this.physicalDeviceType = PhysicalDeviceType.values().first { it.value == deviceType }
    }

    actual fun createDevice(info: DeviceCreateInfo): Device = Device(this, info)

    actual fun alignUpToMinUniformBufferOffset(size: Long): Long {
        val minUboAlignment = this.pvkProperties.limits().minUniformBufferOffsetAlignment()

        return alignUp(size, minUboAlignment)
    }

    actual fun alignDownToMinUniformBufferOffset(size: Long): Long {
        val minUboAlignment = this.pvkProperties.limits().minUniformBufferOffsetAlignment()

        return alignDown(size, minUboAlignment)
    }

    actual fun alignUpToMinVertexBufferOffset(size: Long): Long = alignUp(size, 4L * java.lang.Float.BYTES)

    actual fun alignDownToMinVertexBufferOffset(size: Long): Long = alignDown(size, 4L * java.lang.Float.BYTES)

    actual fun alignUpToMinTexelBufferOffset(size: Long): Long {
        val minTboAlignment = this.pvkProperties.limits().minTexelBufferOffsetAlignment()

        return alignUp(size, minTboAlignment)
    }

    actual fun alignDownToMinTexelBufferOffset(size: Long): Long {
        val minTboAlignment = this.pvkProperties.limits().minTexelBufferOffsetAlignment()

        return alignDown(size, minTboAlignment)
    }

    actual fun alignUpToMinIndexBufferOffset(size: Long, indexType: IndexType): Long = when (indexType) {
        IndexType.UINT16 -> alignUp(size, 2)
        IndexType.UINT32 -> alignUp(size, 4)
    }

    actual fun alignDownToMinIndexBufferOffset(size: Long, indexType: IndexType): Long = when (indexType) {
        IndexType.UINT16 -> alignDown(size, 2)
        IndexType.UINT32 -> alignDown(size, 4)
    }

    actual fun alignUpToMinStorageBufferOffset(size: Long): Long {
        val minSboAlignment = this.pvkProperties.limits().minStorageBufferOffsetAlignment()

        return alignUp(size, minSboAlignment)
    }

    actual fun alignDownToMinStorageBufferOffset(size: Long): Long {
        val minSboAlignment = this.pvkProperties.limits().minStorageBufferOffsetAlignment()

        return alignDown(size, minSboAlignment)
    }

    override fun toString(): String {
        val apiVersion = pvkProperties.apiVersion()
        val major = VK10.VK_VERSION_MAJOR(apiVersion)
        val minor = VK10.VK_VERSION_MINOR(apiVersion)
        val patch = VK10.VK_VERSION_PATCH(apiVersion)

        return String.format("Physical Device: {DeviceID=0x%x, VendorID=0x%x, DeviceName=%s, API Version=%d.%d.%d, DeviceType=%s, TotalRam: %.2fGB}",
                pvkProperties.deviceID(),
                pvkProperties.vendorID(),
                pvkProperties.deviceNameString(),
                major, minor, patch,
                physicalDeviceType,
                totalDeviceRam * 1E-9)
    }

    actual companion object {

        actual val preferDiscreteGpu: (PhysicalDevice, PhysicalDevice) -> Int = { pDev0: PhysicalDevice, pDev1: PhysicalDevice ->
                val type0 = pDev0.physicalDeviceType
                val type1 = pDev1.physicalDeviceType

            when (type0) {
                PhysicalDeviceType.DISCRETE_GPU -> if (type1 === type0) 0 else 1
                else -> if (PhysicalDeviceType.DISCRETE_GPU === type1) -1 else 0
            }
        }

        actual val preferIntegratedGpu: (PhysicalDevice, PhysicalDevice) -> Int = { pDev0: PhysicalDevice, pDev1: PhysicalDevice ->
                val type0 = pDev0.physicalDeviceType
                val type1 = pDev1.physicalDeviceType

            when (type0) {
                PhysicalDeviceType.INTEGRATED_GPU -> if (type1 === type0) 0 else 1
                else -> if (PhysicalDeviceType.INTEGRATED_GPU === type1) -1 else 0
            }
        }

        actual val preferDeviceRam: (PhysicalDevice, PhysicalDevice) -> Int = { pDev0: PhysicalDevice, pDev1: PhysicalDevice ->
            java.lang.Long.compareUnsigned(pDev0.totalDeviceRam, pDev1.totalDeviceRam)
        }
    }
}
