package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import org.lwjgl.vulkan.VK10
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

actual class Fence internal constructor(
        pool: FencePool,
        override val handle: Long) : ManagedResource, Handle {

    private val fencePool: FencePool by WeakReference(pool)

    actual val device: Device
        get() = this.fencePool.device

    actual val isSignaled: Boolean
        get() = when (VK10.vkGetFenceStatus(this.device.handle, this.handle)) {
            VK10.VK_SUCCESS -> true
            VK10.VK_NOT_READY -> false
            VK10.VK_ERROR_DEVICE_LOST -> throw RuntimeException("Device was lost!")
            else -> throw RuntimeException("Unknown error!")
        }

    actual fun reset(): Fence = apply {
        vkAssert(VK10.vkResetFences(this.device.handle, this.handle))
    }

    actual fun waitFor(): Fence = apply {
        VK10.vkWaitForFences(this.device.handle, this.handle, true, java.lang.Long.MAX_VALUE)
    }

    @Throws(TimeoutException::class)
    fun waitFor(timeout: Long, unit: TimeUnit) = apply {
        val result = VK10.vkWaitForFences(this.device.handle, this.handle, true, unit.toNanos(timeout))

        when (result) {
            VK10.VK_TIMEOUT -> throw TimeoutException()
            else -> vkAssert(result)
        }
    }

    override fun release() = this.fencePool.releaseFence(this)
}
