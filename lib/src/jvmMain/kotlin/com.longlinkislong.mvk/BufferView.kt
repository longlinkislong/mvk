package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkBufferViewCreateInfo
import java.lang.ref.WeakReference

actual class BufferView actual constructor(
        buffer: Buffer,
        actual val info: BufferViewCreateInfo) : Resource, Handle{

    actual val buffer: Buffer by WeakReference(buffer)

    actual val device: Device
        get() = this.buffer.device

    actual var userData: Any? = null

    override val handle: Long

    init {
        this.handle = stackLet { mem ->
            val device = buffer.device

            val pvkBufferViewCI = VkBufferViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO)
                    .flags(info.flags)
                    .buffer(buffer.handle)
                    .format(info.format.value)
                    .offset(info.offset)
                    .range(info.range)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateBufferView(device.handle, pvkBufferViewCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun free() = VK10.vkDestroyBufferView(this.device.handle, this.handle, null)
}
