package com.longlinkislong.mvk

interface ExternalHandle : Handle {
    /**
     * Retrieves a WIN32 handle for the backing memory.
     * This function is only available on supporting Windows NT systems.
     *
     * NOTE: Vulkan specs state that the handle must be closed. Currently there is no way to close these handles.
     *
     * @return the exported memory.
     */
    val win32Handle: Long

    /**
     * Retrieves a POSIX FileDescriptor for the backing memory.
     * This function is only available on supporting POSIX systems.
     *
     * NOTE: Vulkan specs state that the File Descriptor must be closed. Currently there is no way to close the File Descriptor.
     *
     * @return the exported memory.
     */
    val fd: Int
}