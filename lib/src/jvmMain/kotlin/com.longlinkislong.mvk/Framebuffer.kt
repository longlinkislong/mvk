package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkFramebufferCreateInfo
import java.lang.ref.WeakReference

actual class Framebuffer(
        renderPass: RenderPass,
        actual val info: FramebufferCreateInfo,
        attachments: List<ImageView>) : Resource, Handle {

    private val _attachments: List<WeakReference<ImageView>> = attachments.map { WeakReference(it) }

    override val handle: Long

    actual val renderPass: RenderPass by WeakReference(renderPass)

    actual val device: Device
        get() = this.renderPass.device

    actual val attachments: List<ImageView>
        get() = _attachments.map { it.get() ?: throw IllegalStateException("ImageView was lost!") }

    actual var userData: Any? = null

    init {
        this.handle = stackLet { mem ->
            val device = renderPass.device

            val pAttachments = mem.callocLong(attachments.size).apply {
                attachments.forEach { put(it.handle) }
                flip()
            }

            val pvkFramebufferCI = VkFramebufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
                    .renderPass(renderPass.handle)
                    .pAttachments(pAttachments)
                    .width(info.width)
                    .height(info.height)
                    .layers(info.layers)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateFramebuffer(device.handle, pvkFramebufferCI, null, pHandle))

            pHandle[0]
        }
    }

    actual fun getRenderPassBeginInfo(clearValues: List<ClearValue>): CommandBufferRenderPassBeginInfo =
            CommandBufferRenderPassBeginInfo(
                    this, this.renderPass, SubpassContents.INLINE,
                    Rect2D(Offset2D.ZERO, Extent2D(this.info.width, this.info.height)),
                    clearValues)

    override fun free() = VK10.vkDestroyFramebuffer(this.device.handle, this.handle, null)
}
