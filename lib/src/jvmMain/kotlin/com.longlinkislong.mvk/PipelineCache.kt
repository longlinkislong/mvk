package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.heapLet
import com.longlinkislong.mvk.util.map
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkPipelineCacheCreateInfo
import org.slf4j.LoggerFactory
import java.io.IOException
import java.lang.ref.WeakReference
import java.nio.channels.FileChannel
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

internal class PipelineCache internal constructor(device: Device) : Handle, Resource {
    val device: Device by WeakReference(device)

    override val handle: Long

    init {
        var handle = VK10.VK_NULL_HANDLE
        var result = VK10.VK_SUCCESS

        if (Files.exists(OFFLINE_STORAGE)) {
            OFFLINE_STORAGE.map(FileChannel.MapMode.READ_ONLY) { data ->
                handle = stackLet { mem ->
                    val pvkPipelineCacheCI = VkPipelineCacheCreateInfo.callocStack(mem)
                            .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO)
                            .pInitialData(data)

                    val pHandle = mem.callocLong(1)

                    result = VK10.vkCreatePipelineCache(device.handle, pvkPipelineCacheCI, null, pHandle)
                    pHandle[0]
                }
            }
        }

        // either the cache doesn't exist or the cache cannot be loaded
        if (VK10.VK_NULL_HANDLE == handle) {
            handle = stackLet { mem ->
                val pvkPipelinecacheCI = VkPipelineCacheCreateInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO)

                val pHandle = mem.callocLong(1)

                result = VK10.vkCreatePipelineCache(device.handle, pvkPipelinecacheCI, null, pHandle)
                pHandle[0]
            }
        }

        this.handle = when (result) {
            VK10.VK_SUCCESS -> handle
            else -> {
                if (VK10.VK_NULL_HANDLE != handle) {
                    VK10.vkDestroyPipelineCache(device.handle, handle, null)
                }

                VK10.VK_NULL_HANDLE
            }
        }
    }

    fun allocate(createInfo: ComputePipelineCreateInfo) = ComputePipeline(this, createInfo)

    fun allocate(createInfo: GraphicsPipelineCreateInfo, renderPass: RenderPass) =
            GraphicsPipeline(this, createInfo, renderPass)

    fun release(pipeline: ComputePipeline) = pipeline.free()

    fun release(pipeline: GraphicsPipeline) = pipeline.free()

    override fun free() {
        if (VK10.VK_NULL_HANDLE == this.handle) {
            return
        }

        try {
            Files.deleteIfExists(OFFLINE_STORAGE)
        } catch (ex: IOException) {
            LOGGER.warn("Unable to delete PipelineCache offline storage: {}", OFFLINE_STORAGE)
        }

        stackLet { mem ->
            val pDataSize = mem.callocPointer(1)

            VK10.vkGetPipelineCacheData(device.handle, this.handle, pDataSize, null)

            heapLet(pDataSize[0].toInt()) { pData ->
                VK10.vkGetPipelineCacheData(device.handle, this.handle, pDataSize, pData)

                FileChannel.open(OFFLINE_STORAGE, StandardOpenOption.CREATE, StandardOpenOption.READ, StandardOpenOption.WRITE).use {
                    fc -> fc.write(pData)
                }
            }

            Unit
        }

        VK10.vkDestroyPipelineCache(device.handle, this.handle, null)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(PipelineCache::class.java)
        private val OFFLINE_STORAGE = Paths.get(System.getProperty("jvk.Pipelinecache.OFFLINE_STORAGE", "pipeline.bin"))
    }
}
