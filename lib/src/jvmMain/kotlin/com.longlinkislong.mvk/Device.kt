package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.memory.MemoryManager
import com.longlinkislong.mvk.util.stackLet
import com.longlinkislong.mvk.util.address
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.toByteBuffer
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference

actual class Device internal constructor(
        physicalDevice: PhysicalDevice,
        actual val info: DeviceCreateInfo) : Resource {

    private val _fencePool: FencePool
    private val _lock = Any()
    private val _pipelineCache: PipelineCache
    private val _samplerCache: SamplerCache
    private val _semaphorePool: SemaphorePool
    private val _shaderCache = HashMap<ShaderModuleCreateInfo, ShaderModule>()

    internal val descriptorSetLayoutCache: DescriptorSetLayoutCache
    internal val pipelineLayoutCache: PipelineLayoutCache

    val handle: VkDevice
    val memoryManager: MemoryManager

    actual val physicalDevice: PhysicalDevice by WeakReference(physicalDevice)
    actual val queueFamilies: List<QueueFamily>

    init {
        var queueFamilyCount: Int
        val queueFamilyCreateInfos = ArrayList<QueueFamilyCreateInfo>()

        this.handle = stackLet { mem ->
            val pQueueFamilyCount = mem.callocInt(1)

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.handle, pQueueFamilyCount, null)

            queueFamilyCount = pQueueFamilyCount[0]

            val pvkQueueFamilyProperties = VkQueueFamilyProperties.callocStack(queueFamilyCount, mem)

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.handle, pQueueFamilyCount, pvkQueueFamilyProperties)

            val pvkDeviceQueueCI = VkDeviceQueueCreateInfo.callocStack(queueFamilyCount, mem)

            for (i in 0 until queueFamilyCount) {
                val queueFamilyQueueCount = pvkQueueFamilyProperties[i].queueCount()
                val queueFlags = QueueFlag.toEnumSet(pvkQueueFamilyProperties[i].queueFlags())

                val queuePriorities = info.queueCreationRules?.invoke(i, queueFlags) ?: when {
                    queueFlags.containsAll(StandardQueueFlags.GENERAL) -> floatArrayOf(1.0F)
                    else -> FloatArray(queueFamilyQueueCount) { 1.0F }
                }

                val pProperties = mem.callocFloat(queuePriorities.size).apply {
                    queuePriorities.forEach { put(it) }
                    flip()
                }

                pvkDeviceQueueCI.get(i)
                        .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO)
                        .queueFamilyIndex(i)
                        .pQueuePriorities(pProperties)

                queueFamilyCreateInfos += QueueFamilyCreateInfo(
                        index = i,
                        queueFlags = queueFlags,
                        queueCount = queuePriorities.size)
            }

            val ppEnabledExtensions = mem.callocPointer(info.enabledExtensions.size).apply {
                info.enabledExtensions.forEach { put(mem.UTF8(it.value)) }
                flip()
            }

            val pvkDeviceCI = VkDeviceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO)
                    .pQueueCreateInfos(pvkDeviceQueueCI)
                    .pEnabledFeatures(physicalDevice.pvkFeatures)
                    .ppEnabledExtensionNames(ppEnabledExtensions)

            val pHandle = mem.callocPointer(1)

            vkAssert(VK10.vkCreateDevice(physicalDevice.handle, pvkDeviceCI, null, pHandle))

            VkDevice(pHandle[0], physicalDevice.handle, pvkDeviceCI)
        }

        this.queueFamilies = queueFamilyCreateInfos.map { QueueFamily(this, it) }
        this._semaphorePool = SemaphorePool(this)
        this.memoryManager = MemoryManager(this.handle)
        this.descriptorSetLayoutCache = DescriptorSetLayoutCache(this)
        this.pipelineLayoutCache = PipelineLayoutCache(this)
        this._pipelineCache = PipelineCache(this)
        this._fencePool = FencePool(this)
        this._samplerCache = SamplerCache(this)
    }

    internal fun getShaderModule(createInfo: ShaderModuleCreateInfo): ShaderModule = synchronized(_lock) {
        this._shaderCache.computeIfAbsent(createInfo) { ShaderModule(this, it) }
    }

    actual fun acquireFence(): Fence = this._fencePool.acquireFence()

    actual fun acquireSemaphore(): Semaphore = this._semaphorePool.acquireSemaphore()

    actual fun allocatePipeline(createInfo: ComputePipelineCreateInfo) =
            this._pipelineCache.allocate(createInfo)

    actual fun allocatePipeline(createInfo: GraphicsPipelineCreateInfo, renderPass: RenderPass) =
            this._pipelineCache.allocate(createInfo, renderPass)

    actual fun createBuffer(createInfo: BufferCreateInfo, properties: MemoryProperties) =
            Buffer(this, createInfo, MemoryProperty.toBitfield(properties))

    actual fun createImage(createInfo: ImageCreateInfo, properties: MemoryProperties) =
            Image(this, createInfo, MemoryProperty.toBitfield(properties))

    actual fun allocateSampler(createInfo: SamplerCreateInfo): Sampler = this._samplerCache.allocate(createInfo)

    actual fun createRenderPass(createInfo: RenderPassCreateInfo): RenderPass = RenderPass(this, createInfo)

    actual fun createSwapchain(createInfo: SwapchainCreateInfo): Swapchain = Swapchain(this, createInfo)

    actual fun waitIdle() = vkAssert(VK10.vkDeviceWaitIdle(this.handle))

    override fun free() {
        this.waitIdle()

        synchronized(this) {
            this._samplerCache.free()
            this.pipelineLayoutCache.free()
            this._pipelineCache.free()
            this.memoryManager.free()
            this.descriptorSetLayoutCache.free()
            this._semaphorePool.free()
            this._fencePool.free()
            this._shaderCache.values.forEach { it.free() }
            VK10.vkDestroyDevice(this.handle, null)
        }
    }
}
