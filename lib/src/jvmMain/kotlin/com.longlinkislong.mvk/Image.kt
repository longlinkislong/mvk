package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.memory.MemoryBlock
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference
import java.nio.ByteBuffer
import java.nio.IntBuffer

actual class Image : Resource, ExternalHandle {
    private val _device: WeakReference<Device>

    actual val device: Device
        get() = this._device.get() ?: throw IllegalStateException("Device was lost!")

    actual val fullRange: ImageSubresourceRange
        get() = ImageSubresourceRange(this.info.format.aspect, 0, this.info.mipLevels, 0, this.info.arrayLayers)

    actual val isArrayed: Boolean
        get() = this.info.arrayLayers > 1

    actual val hasMipmap: Boolean
        get() = this.info.mipLevels > 1

    actual val fullViewCreateInfo: ImageViewCreateInfo
        get() {
            val viewType: ImageViewType = if (this.isArrayed) {
                when (this.info.imageType) {
                    ImageType.IMAGE_1D -> ImageViewType.VIEW_1D_ARRAY
                    ImageType.IMAGE_2D -> ImageViewType.VIEW_2D_ARRAY
                    else -> throw UnsupportedOperationException("Only 1D and 2D images can be arrayed!")
                }
            } else {
                when (this.info.imageType) {
                    ImageType.IMAGE_1D -> ImageViewType.VIEW_1D
                    ImageType.IMAGE_2D -> ImageViewType.VIEW_2D
                    ImageType.IMAGE_3D -> ImageViewType.VIEW_3D
                }
            }

            return ImageViewCreateInfo(viewType, this.info.format, this.fullRange)
        }

    override val fd: Int
        get() {
            assert(ExternalMemoryHandleType.OPAQUE_FD === this.info.handleType) { "HandleType must be OPAQUE_FD for this feature!" }

            return stackLet { mem ->
                val pvkMemoryGetFdInfo = VkMemoryGetFdInfoKHR.callocStack(mem)
                        .sType(KHRExternalMemoryFd.VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR)
                        .memory(this.memory!!.handle)
                        .handleType(this.info.handleType!!.value)

                val pFd = mem.callocInt(1)

                vkAssert(KHRExternalMemoryFd.vkGetMemoryFdKHR(device.handle, pvkMemoryGetFdInfo, pFd))

                pFd[0]
            }
        }

    override val win32Handle: Long
        get() {
            assert(ExternalMemoryHandleType.OPAQUE_WIN32 === this.info.handleType) { "HandleType must be OPAQUE_WIN32 for this feature!" }

            return stackLet { mem ->
                val pvkMemoryGetWin32Info = VkMemoryGetWin32HandleInfoKHR.callocStack(mem)
                        .sType(KHRExternalMemoryWin32.VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR)
                        .memory(this.memory!!.handle)
                        .handleType(this.info.handleType!!.value)

                val pHandle = mem.callocPointer(1)

                KHRExternalMemoryWin32.vkGetMemoryWin32HandleKHR(device.handle, pvkMemoryGetWin32Info, pHandle)

                pHandle[0]
            }
        }

    actual var userData: Any? = null

    val memory: MemoryBlock?

    override val handle: Long

    actual val info: ImageCreateInfo

    internal constructor(device: Device, info: ImageCreateInfo, handle: Long) {
        this._device = WeakReference(device)
        this.info = info
        this.handle = handle
        this.memory = null
    }

    internal constructor(device: Device, info: ImageCreateInfo, memoryProperties: Int) {
        this._device = WeakReference(device)
        this.info = info
        this.handle = stackLet { mem ->
            val pQueueFamilyIndicies: IntBuffer? = if (info.queueFamilies.isEmpty()) null else {
                mem.callocInt(info.queueFamilies.size).apply {
                    info.queueFamilies.forEach { put(it.info.index) }
                    flip()
                }
            }

            val pvkImageCI = VkImageCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO)
                    .flags(info.flags)
                    .imageType(info.imageType.value)
                    .format(info.format.value)
                    .mipLevels(info.mipLevels)
                    .arrayLayers(info.arrayLayers)
                    .samples(info.samples)
                    .tiling(info.tiling.value)
                    .usage(info.usage.bitfield)
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndicies)
                    .initialLayout(info.initialLayout.value)

            Serialization.deserializeExtent3D(info.extent, pvkImageCI.extent())

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateImage(device.handle, pvkImageCI, null, pHandle))

            pHandle[0]
        }

        this.memory = stackLet { mem ->
            val pvkMemoryRequirements = VkMemoryRequirements.callocStack(mem)

            VK10.vkGetImageMemoryRequirements(device.handle, this.handle, pvkMemoryRequirements)

            if (null == info.handleType) {
                device.memoryManager.allocateImageMemory(pvkMemoryRequirements, memoryProperties).apply {
                    bindToImage(this@Image.handle)
                }
            } else {
                device.memoryManager.allocateExportableImageMemory(pvkMemoryRequirements, memoryProperties, info.handleType.value).apply {
                    bindToImage(this@Image.handle)
                }
            }
        }
    }

    actual fun getMipAllLayers(mipLevel: Int): ImageSubresourceLayers =
            ImageSubresourceLayers(this.info.format.aspect, mipLevel, 0, this.info.arrayLayers)

    actual fun getSubresourceLayers(mipLevel: Int, baseArrayLayer: Int, layerCount: Int): ImageSubresourceLayers =
            ImageSubresourceLayers(this.info.format.aspect, mipLevel, baseArrayLayer, layerCount)

    actual fun getSubresourceLayer(mipLevel: Int, baseArrayLayer: Int): ImageSubresourceLayers =
            this.getSubresourceLayers(mipLevel, baseArrayLayer, 1)

    actual fun getSubresourceRange(baseMipLevel: Int, levelCount: Int, baseArrayLayer: Int, layerCount: Int): ImageSubresourceRange =
            ImageSubresourceRange(this.info.format.aspect, baseMipLevel, levelCount, baseArrayLayer, layerCount)

    override fun free() {
        VK10.vkDestroyImage(this.device.handle, this.handle, null)
        this.memory?.free()
    }
}
