package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.KHRSurface
import org.lwjgl.vulkan.VK10
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicInteger

/**
 * A collection of Vulkan Queues that all have similar properties.
 */
actual class QueueFamily internal constructor(
        device: Device,
        actual val info: QueueFamilyCreateInfo) {

    private val _commandPools = ThreadLocal.withInitial { CommandPool(this, setOf(CommandPoolCreateFlag.CREATE_RESET_COMMAND_BUFFER)) }
    private val _selectedQueueIndex = AtomicInteger(0)

    actual val device: Device by WeakReference(device)

    actual val queues = (0 until info.queueCount)
            .map { queueIndex -> Queue(device, info.index, queueIndex) }
            .toList()

    actual val currentCommandPool: CommandPool
        get() = this._commandPools.get()

    actual val nextQueueIndex: Int
        get() = if (this.queues.size == 1) {
            0
        } else {
            this._selectedQueueIndex.getAndUpdate { idx -> idx % this.queues.size }
        }

    actual val nextQueue: Queue
        get() = this.queues[this.nextQueueIndex]

    actual fun createCommandPool(flags: Set<CommandPoolCreateFlag>): CommandPool = CommandPool(this, flags)

    actual fun detach() {
        this._commandPools.get().free()
        this._commandPools.remove()
    }

    actual fun canPresent(surface: Long): Boolean = stackLet { mem ->
        val pSupported = mem.callocInt(1)

        KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR(this.device.handle.physicalDevice, this.info.index, surface, pSupported)

        VK10.VK_TRUE == pSupported[0]
    }

    companion object {
        const val MAX_QUEUES_PROPERTY_NAME = "jvk.QueueFamily.MAX_QUEUES"
        const val ALL_QUEUES = -1

        /**
         * The maximum number of Queues to allocate from the QueueFamily.
         */
        val MAX_QUEUES = Integer.getInteger(MAX_QUEUES_PROPERTY_NAME, ALL_QUEUES)!!

        internal fun indexOf(qf: QueueFamily?): Int = qf?.info?.index ?: VK10.VK_QUEUE_FAMILY_IGNORED
    }
}