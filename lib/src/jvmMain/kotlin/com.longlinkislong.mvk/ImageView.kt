package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkImageViewCreateInfo
import java.lang.ref.WeakReference

actual class ImageView actual constructor(
        image: Image,
        actual val info: ImageViewCreateInfo) : Resource, Handle {

    actual val image: Image by WeakReference(image)

    override val handle: Long

    actual val device: Device
        get() = this.image.device

    actual var userData: Any? = null

    init {
        this.handle = stackLet { mem ->
            val pvkImageViewCI = VkImageViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO)
                    .image(image.handle)
                    .viewType(info.viewType.value)
                    .format(info.format.value)

            Serialization.deserializeImageSubresourceRange(info.subresourceRange, pvkImageViewCI.subresourceRange())

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateImageView(device.handle, pvkImageViewCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun free() = VK10.vkDestroyImageView(this.device.handle, this.handle, null)
}
