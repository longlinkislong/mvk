package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference
import java.util.*

internal class SemaphorePool(device: Device) : Resource {
    private val _lock = Any()
    private val _allSemaphores = HashSet<Semaphore>()
    private val _availableSemaphores = ArrayDeque<Semaphore>()
    private val _handleTypes: Int = device.info.enabledExtensions.sumBy {
        when (it) {
            StandardDeviceExtensions.KHR_EXTERNAL_SEMAPHORE_FD -> VK11.VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT
            StandardDeviceExtensions.KHR_EXTERNAL_SEMAPHORE_WIN32 -> VK11.VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT
            else -> 0
        }
    }

    val device: Device by WeakReference(device)

    private fun allocateSemaphore(): Semaphore = stackLet { mem ->
        val pvkSemaphoreCI = VkSemaphoreCreateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO)

        if (0 != this._handleTypes) {
            val pvkExportSemaphore = VkExportSemaphoreCreateInfo.callocStack(mem)
                    .sType(VK11.VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO)
                    .handleTypes(this._handleTypes)

            pvkSemaphoreCI.pNext(pvkExportSemaphore.address())
        }

        val pHandle = mem.callocLong(1)

        vkAssert(VK10.vkCreateSemaphore(this.device.handle, pvkSemaphoreCI, null, pHandle))

        Semaphore(this, pHandle[0]).also { this._allSemaphores.add(it) }
    }

    fun acquireSemaphore(): Semaphore = synchronized(_lock) {
        if (this._availableSemaphores.isEmpty()) {
            allocateSemaphore()
        } else {
            this._availableSemaphores.poll()
        }
    }

    fun releaseSemaphore(semaphore: Semaphore) = synchronized(_lock) {
        assert(this._allSemaphores.contains(semaphore))

        this._availableSemaphores.offer(semaphore)
        Unit
    }

    override fun free() = synchronized(_lock) {
        this._allSemaphores.forEach { semaphore -> VK10.vkDestroySemaphore(device.handle, semaphore.handle, null) }
        this._allSemaphores.clear()
        this._availableSemaphores.clear()
    }
}
