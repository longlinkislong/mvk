package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.asSequence
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkCommandBufferAllocateInfo
import org.lwjgl.vulkan.VkCommandPoolCreateInfo
import java.lang.ref.WeakReference

actual class CommandPool internal constructor(
        queueFamily: QueueFamily,
        actual val flags: CommandPoolCreateFlags) : Resource, Handle {

    actual val queueFamily: QueueFamily by WeakReference(queueFamily)

    override val handle: Long

    actual val device: Device
        get() = this.queueFamily.device

    init {
        this.handle = stackLet { mem ->
            val device = queueFamily.device

            val pvkCommandPoolCI = VkCommandPoolCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
                    .flags(flags.bitfield)
                    .queueFamilyIndex(queueFamily.info.index)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateCommandPool(device.handle, pvkCommandPoolCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun free() = VK10.vkDestroyCommandPool(this.device.handle, this.handle, null)

    actual fun reset(flags: Int) = vkAssert(VK10.vkResetCommandPool(this.device.handle, this.handle, flags))

    actual fun allocate(level: CommandBufferLevel): CommandBuffer = stackLet { mem ->
        val pvkCommandBufferAI = VkCommandBufferAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                .commandPool(this.handle)
                .level(level.value)
                .commandBufferCount(1)

        val pHandle = mem.callocPointer(1)

        vkAssert(VK10.vkAllocateCommandBuffers(this.device.handle, pvkCommandBufferAI, pHandle))

        CommandBuffer(this, level, pHandle[0])
    }

    actual fun allocate(level: CommandBufferLevel, count: Int): List<CommandBuffer> = stackLet { mem ->
        val pvkCommandBufferAI = VkCommandBufferAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                .commandPool(this.handle)
                .level(level.value)
                .commandBufferCount(count)

        val pHandle = mem.callocPointer(count)

        vkAssert(VK10.vkAllocateCommandBuffers(this.device.handle, pvkCommandBufferAI, pHandle))

        pHandle.asSequence()
                .map { handle -> CommandBuffer(this, level, handle) }
                .toList()
    }
}
