package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.ShaderStage
import com.longlinkislong.mvk.dsl.GraphicsPipelineCreateInfoDsl
import com.longlinkislong.mvk.dsl.PipelineShaderStageCreateInfoDsl
import org.lwjgl.system.MemoryUtil
import java.nio.ByteBuffer

inline fun GraphicsPipelineCreateInfoDsl.stage(stage: ShaderStage, binary: ByteBuffer, block: PipelineShaderStageCreateInfoDsl.() -> Unit) {
    this.stages += PipelineShaderStageCreateInfoDsl(stage, binary.remaining(), MemoryUtil.memAddress(binary)).apply(block).toCreateInfo()
}

fun GraphicsPipelineCreateInfoDsl.stage(stage: ShaderStage, binary: ByteBuffer) {
    this.stage(stage, binary.remaining(), MemoryUtil.memAddress(binary))
}