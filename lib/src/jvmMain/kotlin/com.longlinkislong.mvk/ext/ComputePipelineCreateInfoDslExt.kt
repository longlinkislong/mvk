package com.longlinkislong.mvk.ext

import com.longlinkislong.mvk.ShaderStage
import com.longlinkislong.mvk.dsl.ComputePipelineCreateInfoDsl
import com.longlinkislong.mvk.dsl.PipelineShaderStageCreateInfoDsl
import org.lwjgl.system.MemoryUtil
import java.nio.ByteBuffer

inline fun ComputePipelineCreateInfoDsl.stage(binary: ByteBuffer, block: PipelineShaderStageCreateInfoDsl.() -> Unit) {
    this.stage = PipelineShaderStageCreateInfoDsl(ShaderStage.COMPUTE, binary.remaining(), MemoryUtil.memAddress(binary)).apply(block).toCreateInfo()
}

fun ComputePipelineCreateInfoDsl.stage(binary: ByteBuffer) =
        this.stage(binary.remaining(), MemoryUtil.memAddress(binary))