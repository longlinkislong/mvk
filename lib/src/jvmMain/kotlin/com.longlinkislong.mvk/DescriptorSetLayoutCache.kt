package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.getValue
import java.lang.ref.WeakReference

internal class DescriptorSetLayoutCache internal constructor(device: Device) : Resource {
    private val _lock = Any()
    private val _layouts = HashMap<DescriptorSetLayoutCreateInfo, Layout>()

    val device: Device by WeakReference(device)

    private data class Layout(
            val instance: DescriptorSetLayout,
            var references: Int = 0)

    override fun free() = synchronized(_lock) {
        this._layouts.values.forEach { layout -> layout.instance.free() }
        this._layouts.clear()
    }

    fun allocateDescriptorSetLayout(createInfo: DescriptorSetLayoutCreateInfo): DescriptorSetLayout =
            synchronized(_lock) {
                val layout = this._layouts.computeIfAbsent(createInfo) { Layout(DescriptorSetLayout(this, it)) }

                layout.references += 1

                layout.instance
            }

    fun releaseDescriptorSetLayout(layout: DescriptorSetLayout) = synchronized(_lock) {
        assert(layout.descriporSetLayoutCache == this)

        val dsl = this._layouts[layout.info]!!

        dsl.references -= 1

        if (0 == dsl.references) {
            layout.free()
            this._layouts.remove(layout.info)
        }
    }
}
