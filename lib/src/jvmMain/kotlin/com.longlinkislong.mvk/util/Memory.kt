package com.longlinkislong.mvk.util

import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import java.nio.ByteBuffer
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

internal inline fun <R> stackRun(block: MemoryStack.() -> R): R {
    val frame = MemoryStack.stackPush()
    val result = block(frame)

    frame.pop()

    return result
}

internal inline fun <R> stackLet(block: (MemoryStack) -> R): R {
    val frame = MemoryStack.stackPush()
    val result = block(frame)

    frame.pop()

    return result
}

internal inline fun <R> heapRun(size: Int, block: ByteBuffer.() -> R): R {
    val data = MemoryUtil.memAlloc(size)
    val result = block(data)

    MemoryUtil.memFree(data)

    return result
}

internal inline fun <R> heapLet(size: Int, block: (ByteBuffer) -> R): R {
    val data = MemoryUtil.memAlloc(size)
    val result = block(data)

    MemoryUtil.memFree(data)

    return result
}

internal inline fun <R> nheapRun(size: Long, block: Long.() -> R): R {
    val data = MemoryUtil.nmemAlloc(size)
    val result = block(data)

    MemoryUtil.nmemFree(data)

    return result
}

internal inline fun <R> nheapLet(size: Long, block: (Long) -> R): R {
    val data = MemoryUtil.nmemAlloc(size)
    val result = block(data)

    MemoryUtil.nmemFree(data)

    return result
}

internal inline fun <T> T.applyStack(block: T.(MemoryStack) -> Unit): T {
    val frame = MemoryStack.stackPush()
    this.block(frame)
    frame.pop()

    return this
}

internal inline fun <T> T.alsoStack(block: (T, MemoryStack) -> Unit): T {
    val frame = MemoryStack.stackPush()
    block(this, frame)
    frame.pop()

    return this
}

internal inline fun MemoryStack.applyPush(block: MemoryStack.(MemoryStack) -> Unit): MemoryStack {
    val frame = push()
    block(frame)
    frame.pop()

    return this
}

internal inline fun MemoryStack.alsoPush(block: (MemoryStack, MemoryStack) -> Unit): MemoryStack {
    val frame = push()
    block(this, frame)
    frame.pop()

    return this
}