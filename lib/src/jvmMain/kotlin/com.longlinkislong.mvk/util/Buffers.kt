package com.longlinkislong.mvk.util

import org.lwjgl.PointerBuffer
import org.lwjgl.system.MemoryUtil
import java.nio.ByteBuffer
import java.nio.LongBuffer

internal fun PointerBuffer.asSequence(): Sequence<Long> = generateSequence {
    when {
        this.hasRemaining() -> this.get()
        else -> null
    }
}

internal fun LongBuffer.asSequence(): Sequence<Long> = generateSequence {
    when {
        this.hasRemaining() -> this.get()
        else -> null
    }
}

internal val ByteBuffer.address: Long
    get() = MemoryUtil.memAddress(this)

internal fun CharSequence.toByteBuffer() = MemoryUtil.memUTF8(this)