package com.longlinkislong.mvk.util

import java.lang.ref.WeakReference
import kotlin.reflect.KProperty

internal operator fun <T> WeakReference<T>.getValue(thisRef: Any?, property: KProperty<*>) =
        this.get() ?: throw IllegalStateException("Reference was lost!")