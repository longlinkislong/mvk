package com.longlinkislong.mvk.util

import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.StandardOpenOption

internal inline fun <R> Path.open(options: Set<OpenOption>, block: (FileChannel) -> R): R = FileChannel.open(this, options).use(block)

@Throws(UnsupportedOperationException::class)
internal inline fun <R> Path.map(mapMode: FileChannel.MapMode, block: (ByteBuffer) -> R): R {
    return when (mapMode) {
        FileChannel.MapMode.READ_ONLY -> this.open(setOf(StandardOpenOption.READ)) {
            block(it.map(mapMode, 0, it.size()))
        }
        FileChannel.MapMode.READ_WRITE -> this.open(setOf(StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
            block(it.map(mapMode, 0, it.size()))
        }
        FileChannel.MapMode.PRIVATE -> this.open(setOf(StandardOpenOption.READ, StandardOpenOption.WRITE)) {
            block(it.map(mapMode, 0, it.size()))
        }
        else -> throw UnsupportedOperationException("Unsupported mapMode: $mapMode")
    }
}