package com.longlinkislong.mvk.util

import com.longlinkislong.mvk.*
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.*
import java.nio.ByteBuffer
import java.nio.FloatBuffer

internal object Serialization {
    fun deserializeAttachmentDescriptions(list: List<AttachmentDescription>, mem: MemoryStack): VkAttachmentDescription.Buffer =
            VkAttachmentDescription.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, attachment -> deserializeAttachmentDescription(attachment, get(index)) }
            }
    fun deserializeAttachmentDescription(attachmentDescription: AttachmentDescription, vkobj: VkAttachmentDescription): VkAttachmentDescription =
            vkobj.set(attachmentDescription.flags, attachmentDescription.format.value, attachmentDescription.samples, attachmentDescription.loadOp.value, attachmentDescription.storeOp.value, attachmentDescription.stencilLoadOp.value, attachmentDescription.stencilStoreOp.value, attachmentDescription.initialLayout.value, attachmentDescription.finalLayout.value)

    fun deserializeAttachmentReferences(buffer: List<AttachmentReference>, mem: MemoryStack): VkAttachmentReference.Buffer? {
        return if (buffer.isEmpty()) null else VkAttachmentReference.callocStack(buffer.size, mem).apply {
            buffer.forEachIndexed { index, ref -> deserializeAttachmentReference(ref, get(index)) }
        }
    }

    fun deserializeAttachmentReference(attachmentReference: AttachmentReference, vkobj: VkAttachmentReference): VkAttachmentReference {
        return vkobj.set(attachmentReference.attachment, attachmentReference.layout.value)
    }

    fun deserializeBufferImageCopy(bufferImageCopy: BufferImageCopy, vkobj: VkBufferImageCopy): VkBufferImageCopy {
        vkobj.bufferOffset(bufferImageCopy.bufferOffset)
                .bufferRowLength(bufferImageCopy.bufferRowLength)
                .bufferImageHeight(bufferImageCopy.bufferImageHeight)

        deserializeImageSubresourceLayers(bufferImageCopy.imageSubresource, vkobj.imageSubresource())
        deserializeOffset3D(bufferImageCopy.imageOffset, vkobj.imageOffset())
        deserializeExtent3D(bufferImageCopy.imageExtent, vkobj.imageExtent())

        return vkobj
    }

    fun deserializeImageSubresourceLayers(imageSubresourceLayers: ImageSubresourceLayers, vkobj: VkImageSubresourceLayers) =
            vkobj.set(imageSubresourceLayers.aspectMask.bitfield, imageSubresourceLayers.mipLevel, imageSubresourceLayers.baseArrayLayer, imageSubresourceLayers.layerCount)

    fun deserializeImageCopy(imageCopy: ImageCopy, vkobj: VkImageCopy): VkImageCopy {
        deserializeOffset3D(imageCopy.srcOffset, vkobj.srcOffset())
        deserializeOffset3D(imageCopy.dstOffset, vkobj.dstOffset())
        deserializeExtent3D(imageCopy.extent, vkobj.extent())
        deserializeImageSubresourceLayers(imageCopy.srcSubresource, vkobj.srcSubresource())
        deserializeImageSubresourceLayers(imageCopy.dstSubresource, vkobj.dstSubresource())

        return vkobj
    }

    fun deserializeOffset3D(offset3D: Offset3D, vkobj: VkOffset3D): VkOffset3D = vkobj.set(offset3D.x, offset3D.y, offset3D.z)

    fun deserializeExtent2D(extent2D: Extent2D, vkobj: VkExtent2D) = vkobj.set(extent2D.width, extent2D.height)

    fun serializeExtent3D(vkobj: VkExtent3D) = Extent3D(vkobj.width(), vkobj.height(), vkobj.depth())

    fun deserializeExtent3D(extent3D: Extent3D, vkobj: VkExtent3D) = vkobj.set(extent3D.width, extent3D.height, extent3D.depth)

    fun deserializeColor(color: Color, buffer: ByteBuffer): ByteBuffer = buffer
            .putFloat(0, color.red)
            .putFloat(4, color.green)
            .putFloat(8, color.blue)
            .putFloat(12, color.alpha)

    fun deserializeColor(color: Color, buffer: FloatBuffer): FloatBuffer = buffer.put(0, color.red)
            .put(1, color.green)
            .put(2, color.blue)
            .put(3, color.alpha)

    fun deserializeClearValue(clearValue: ClearValue, vkobj: VkClearValue) = vkobj.also {
        if (null != clearValue.color) {
            it.color()
                    .float32(0, clearValue.color.red)
                    .float32(1, clearValue.color.green)
                    .float32(2, clearValue.color.blue)
                    .float32(3, clearValue.color.alpha)
        } else if (null != clearValue.depthStencil) {
            it.depthStencil()
                    .depth(clearValue.depthStencil.depth)
                    .stencil(clearValue.depthStencil.stencil)
        }
    }

    fun deserializeDescriptorPoolSizes(list: List<DescriptorPoolSize>, mem: MemoryStack): VkDescriptorPoolSize.Buffer =
            VkDescriptorPoolSize.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, size -> deserializeDescriptorPoolSize(size, get(index)) }
            }

    fun deserializeDescriptorPoolSize(descriptorPoolSize: DescriptorPoolSize, vkobj: VkDescriptorPoolSize): VkDescriptorPoolSize = vkobj.set(descriptorPoolSize.type, descriptorPoolSize.descriptorCount)

    fun deserializeImageSubresourceRange(imageSubresourceRange: ImageSubresourceRange, vkobj: VkImageSubresourceRange) =
            vkobj.set(imageSubresourceRange.aspectMask.bitfield, imageSubresourceRange.baseMipLevel, imageSubresourceRange.levelCount, imageSubresourceRange.baseArrayLayer, imageSubresourceRange.layerCount)

    fun deserializeMemoryBarrier(memoryBarrier: MemoryBarrier, vkobj: VkMemoryBarrier) =
            vkobj.set(VK10.VK_STRUCTURE_TYPE_MEMORY_BARRIER, 0L, memoryBarrier.srcAccessMask.bitfield, memoryBarrier.dstAccessMask.bitfield)

    fun deserializeOffset2D(offset2D: Offset2D, vkobj: VkOffset2D): VkOffset2D = vkobj.set(offset2D.x, offset2D.y)

    fun deserializePipelineColorBlendAttachmentStates(list: List<PipelineColorBlendAttachmentState>, mem: MemoryStack) =
            VkPipelineColorBlendAttachmentState.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, state -> deserializePipelineColorBlendAttachmentState(state, get(index)) }
            }

    fun deserializePipelineColorBlendAttachmentState(pipelineColorBlendAttachmentState: PipelineColorBlendAttachmentState, vkobj: VkPipelineColorBlendAttachmentState): VkPipelineColorBlendAttachmentState =
            vkobj.set(pipelineColorBlendAttachmentState.blendEnable, pipelineColorBlendAttachmentState.srcColorBlendFactor.value, pipelineColorBlendAttachmentState.dstColorBlendFactor.value, pipelineColorBlendAttachmentState.colorBlendOp.value, pipelineColorBlendAttachmentState.srcAlphaBlendFactor.value, pipelineColorBlendAttachmentState.dstAlphaBlendFactor.value, pipelineColorBlendAttachmentState.alphaBlendOp.value, pipelineColorBlendAttachmentState.colorWriteMask)

    fun deserializePipelineColorBlendStateCreateInfo(pipelineColorBlendStateCreateInfo: PipelineColorBlendStateCreateInfo, vkobj: VkPipelineColorBlendStateCreateInfo, mem: MemoryStack) =
            vkobj
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
                    .pNext(VK10.VK_NULL_HANDLE)
                    .flags(pipelineColorBlendStateCreateInfo.flags)
                    .logicOpEnable(pipelineColorBlendStateCreateInfo.logicOpEnable)
                    .logicOp(pipelineColorBlendStateCreateInfo.logicOp.value)
                    .pAttachments(Serialization.deserializePipelineColorBlendAttachmentStates(pipelineColorBlendStateCreateInfo.attachments, mem))
                    .blendConstants(Serialization.deserializeColor(pipelineColorBlendStateCreateInfo.blendConstants, mem.callocFloat(4)))

    fun deserializeStencilOpState(stencilOpState: StencilOpState, vkobj: VkStencilOpState): VkStencilOpState =
            vkobj.set(stencilOpState.failOp.value, stencilOpState.passOp.value, stencilOpState.depthFailOp.value, stencilOpState.compareOp.value, stencilOpState.compareMask, stencilOpState.writeMask, stencilOpState.reference)

    fun deserializePipelineInputAssemblyStateCreateInfo(pipelineInputAssemblyStateCreateInfo: PipelineInputAssemblyStateCreateInfo, vkobj: VkPipelineInputAssemblyStateCreateInfo) =
            vkobj.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
                    .pNext(VK10.VK_NULL_HANDLE)
                    .flags(pipelineInputAssemblyStateCreateInfo.flags)
                    .topology(pipelineInputAssemblyStateCreateInfo.topology.value)
                    .primitiveRestartEnable(pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable)

    fun deserializePipelineMultisampleStateCreateInfo(pipelineMultisampleStateCreateInfo: PipelineMultisampleStateCreateInfo, vkobj: VkPipelineMultisampleStateCreateInfo): VkPipelineMultisampleStateCreateInfo = vkobj
            .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
            .pNext(VK10.VK_NULL_HANDLE)
            .flags(pipelineMultisampleStateCreateInfo.flags)
            .rasterizationSamples(pipelineMultisampleStateCreateInfo.rasterizationSamples)
            .sampleShadingEnable(pipelineMultisampleStateCreateInfo.sampleShadingEnable)
            .minSampleShading(pipelineMultisampleStateCreateInfo.minSampleShading)
            .pSampleMask(MemoryUtil.memIntBufferSafe(pipelineMultisampleStateCreateInfo.pSampleMask, 1))
            .alphaToCoverageEnable(pipelineMultisampleStateCreateInfo.alphaToCoverageEnable)
            .alphaToOneEnable(pipelineMultisampleStateCreateInfo.alphaToOneEnable)

    fun deserializePipelineRasterizationStateCreateInfo(pipelineRasterizationStateCreateInfo: PipelineRasterizationStateCreateInfo, vkobj: VkPipelineRasterizationStateCreateInfo) =
            vkobj
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
                    .pNext(VK10.VK_NULL_HANDLE)
                    .flags(pipelineRasterizationStateCreateInfo.flags)
                    .depthClampEnable(pipelineRasterizationStateCreateInfo.depthClampEnable)
                    .rasterizerDiscardEnable(pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable)
                    .polygonMode(pipelineRasterizationStateCreateInfo.polygonMode.value)
                    .cullMode(pipelineRasterizationStateCreateInfo.cullMode.value)
                    .frontFace(pipelineRasterizationStateCreateInfo.frontFace.value)
                    .depthBiasEnable(pipelineRasterizationStateCreateInfo.depthBiasEnable)
                    .depthBiasConstantFactor(pipelineRasterizationStateCreateInfo.depthBiasConstantFactor)
                    .depthBiasClamp(pipelineRasterizationStateCreateInfo.depthBiasClamp)
                    .depthBiasSlopeFactor(pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor)
                    .lineWidth(pipelineRasterizationStateCreateInfo.lineWidth)

    fun deserializePipelineTessellationStateCreateInfo(pipelineTessellationStateCreateInfo: PipelineTessellationStateCreateInfo, vkobj: VkPipelineTessellationStateCreateInfo): VkPipelineTessellationStateCreateInfo =
            vkobj.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO)
                    .pNext(VK10.VK_NULL_HANDLE)
                    .flags(pipelineTessellationStateCreateInfo.flags)
                    .patchControlPoints(pipelineTessellationStateCreateInfo.patchControlPoints)

    fun DeserializePipelineVertexInputStateCreateInfo(pipelineVertexInputStateCreateInfo: PipelineVertexInputStateCreateInfo, vkobj: VkPipelineVertexInputStateCreateInfo, mem: MemoryStack): VkPipelineVertexInputStateCreateInfo =
            vkobj
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO)
                    .pNext(VK10.VK_NULL_HANDLE)
                    .flags(pipelineVertexInputStateCreateInfo.flags)
                    .pVertexBindingDescriptions(deserializeVertexInputBindingDescriptions(pipelineVertexInputStateCreateInfo.vertexBindingDescriptions, mem))
                    .pVertexAttributeDescriptions(deserializeVertexInputAttachmentDescriptions(pipelineVertexInputStateCreateInfo.vertexAttributeDescriptions, mem))

    fun deserializePushConstantRange(pushConstantRange: PushConstantRange, vkobj: VkPushConstantRange) =
            vkobj.set(pushConstantRange.stages.bitfield, pushConstantRange.offset, pushConstantRange.size)

    fun deserializeRect2D(rect2D: Rect2D, vkobj: VkRect2D): VkRect2D {
        Serialization.deserializeOffset2D(rect2D.offset, vkobj.offset())
        Serialization.deserializeExtent2D(rect2D.extent, vkobj.extent())

        return vkobj
    }

    fun deserializeViewport(viewport: Viewport, vkObj: VkViewport): VkViewport = vkObj.set(
            viewport.x, viewport.y, viewport.width, viewport.height, viewport.minDepth, viewport.maxDepth)

    fun deserializeSpecializationMapEntries(list: List<SpecializationMapEntry>, mem: MemoryStack) =
            VkSpecializationMapEntry.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, spec -> deserializeSpecializationMapEntry(spec, get(index)) }
            }

    fun deserializeSpecializationMapEntry(specializationMapEntry: SpecializationMapEntry, vkobj: VkSpecializationMapEntry): VkSpecializationMapEntry =
            vkobj.set(specializationMapEntry.constantID, specializationMapEntry.offset, specializationMapEntry.size)

    fun deserializeSpecializationInfo(specializationInfo: SpecializationInfo, vkobj: VkSpecializationInfo, mem: MemoryStack) =
            vkobj.set(Serialization.deserializeSpecializationMapEntries(specializationInfo.mapEntries, mem), MemoryUtil.memByteBuffer(specializationInfo.data, specializationInfo.size))

    fun deserializeSubpassDependencies(list: List<SubpassDependency>, mem: MemoryStack) =
            VkSubpassDependency.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, dependency -> deserializeSubpassDependency(dependency, get(index)) }
            }

    fun deserializeSubpassDependency(subpassDependency: SubpassDependency, vkobj: VkSubpassDependency) =
            vkobj.set(
                    subpassDependency.srcSubpass, subpassDependency.dstSubpass,
                    subpassDependency.srcStageMask.bitfield, subpassDependency.dstStageMask.bitfield,
                    subpassDependency.srcAccessMask.bitfield, subpassDependency.dstAccessMask.bitfield,
                    DependencyFlag.toBitfield(subpassDependency.dependencyFlags))

    fun deserializeSubpassDescriptions(list: List<SubpassDescription>, mem: MemoryStack): VkSubpassDescription.Buffer =
            VkSubpassDescription.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, description -> deserializeSubpassDescription(description, get(index), mem) }
            }

    fun deserializeSubpassDescription(subpassDescription: SubpassDescription, vkobj: VkSubpassDescription, mem: MemoryStack): VkSubpassDescription {
        vkobj.flags(subpassDescription.flags)
                .pipelineBindPoint(subpassDescription.pipelineBindpoint.value)
                .pInputAttachments(deserializeAttachmentReferences(subpassDescription.inputAttachments, mem))
                .pColorAttachments(deserializeAttachmentReferences(subpassDescription.colorAttachments, mem))
                .colorAttachmentCount(subpassDescription.colorAttachments.size)
                .pResolveAttachments(deserializeAttachmentReferences(subpassDescription.resolveAttachments, mem))

        if (null == subpassDescription.depthStencilAttachment) {
            vkobj.pDepthStencilAttachment(null)
        } else {
            vkobj.pDepthStencilAttachment(deserializeAttachmentReference(subpassDescription.depthStencilAttachment, VkAttachmentReference.callocStack(mem)))
        }

        if (subpassDescription.preserveAttachments.isEmpty()) {
            vkobj.pPreserveAttachments(null)
        } else {
            val pPreserveAttachments = mem.callocInt(subpassDescription.preserveAttachments.size)

            subpassDescription.preserveAttachments.forEach { pPreserveAttachments.put(it) }
            pPreserveAttachments.flip()

            vkobj.pPreserveAttachments(pPreserveAttachments)
        }

        return vkobj
    }

    fun deserializeVertexInputAttachmentDescriptions(list: List<VertexInputAttributeDescription>, mem: MemoryStack): VkVertexInputAttributeDescription.Buffer =
            VkVertexInputAttributeDescription.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, description -> deserializeVertexInputAttachmentDescription(description, get(index)) }
            }

    fun deserializeVertexInputAttachmentDescription(vertexInputAttributeDescription: VertexInputAttributeDescription, vkobj: VkVertexInputAttributeDescription): VkVertexInputAttributeDescription =
            vkobj.set(vertexInputAttributeDescription.location, vertexInputAttributeDescription.binding, vertexInputAttributeDescription.format.value, vertexInputAttributeDescription.offset)

    fun deserializeVertexInputBindingDescriptions(list: List<VertexInputBindingDescription>, mem: MemoryStack) =
            VkVertexInputBindingDescription.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, description -> deserializeVertexInputBindingDescription(description, get(index)) }
            }

    fun deserializeVertexInputBindingDescription(vertexInputBindingDescription: VertexInputBindingDescription, vkobj: VkVertexInputBindingDescription) =
            vkobj.set(vertexInputBindingDescription.binding, vertexInputBindingDescription.stride, vertexInputBindingDescription.inputRate.value)

    fun deserializePipelineDepthStencilStateCreateInfo(pipelineDepthStencilStateCreateInfo: PipelineDepthStencilStateCreateInfo, vkobj: VkPipelineDepthStencilStateCreateInfo): VkPipelineDepthStencilStateCreateInfo {
        deserializeStencilOpState(pipelineDepthStencilStateCreateInfo.front, vkobj.front())
        deserializeStencilOpState(pipelineDepthStencilStateCreateInfo.back, vkobj.back())

        return vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO)
                .pNext(VK10.VK_NULL_HANDLE)
                .flags(pipelineDepthStencilStateCreateInfo.flags)
                .depthTestEnable(pipelineDepthStencilStateCreateInfo.depthTestEnable)
                .depthWriteEnable(pipelineDepthStencilStateCreateInfo.depthWriteEnable)
                .depthCompareOp(pipelineDepthStencilStateCreateInfo.depthCompareOp.value)
                .depthBoundsTestEnable(pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable)
                .stencilTestEnable(pipelineDepthStencilStateCreateInfo.stencilTestEnable)
                .minDepthBounds(pipelineDepthStencilStateCreateInfo.minDepthBounds)
                .maxDepthBounds(pipelineDepthStencilStateCreateInfo.maxDepthBounds)
    }

    fun deserializeBufferMemoryBarrier(bufferMemoryBarrier: BufferMemoryBarrier, vkobj: VkBufferMemoryBarrier): VkBufferMemoryBarrier {
        return vkobj.set(
                VK10.VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER, 0L,
                bufferMemoryBarrier.srcAccessMask.bitfield, bufferMemoryBarrier.dstAccessMask.bitfield,
                QueueFamily.indexOf(bufferMemoryBarrier.srcQueueFamily), QueueFamily.indexOf(bufferMemoryBarrier.dstQueueFamily),
                bufferMemoryBarrier.buffer.handle, bufferMemoryBarrier.offset, bufferMemoryBarrier.size)
    }

    fun deserializeDescriptorSetLayoutBindings(list: List<DescriptorSetLayoutBinding>, mem: MemoryStack) =
            VkDescriptorSetLayoutBinding.callocStack(list.size, mem).apply {
                list.forEachIndexed { index, binding -> deserializeDescriptorSetLayoutBindings(binding, get(index), mem) }
            }

    fun deserializeDescriptorSetLayoutBindings(descriptorSetLayoutBinding: DescriptorSetLayoutBinding, vkobj: VkDescriptorSetLayoutBinding, mem: MemoryStack) =
            if (descriptorSetLayoutBinding.immutableSamplers.isEmpty()) {
                vkobj.set(descriptorSetLayoutBinding.binding, descriptorSetLayoutBinding.descriptorType.value, descriptorSetLayoutBinding.descriptorCount, descriptorSetLayoutBinding.stages.bitfield, null)
            } else {
                val pImmutableSamplers = mem.callocLong(descriptorSetLayoutBinding.immutableSamplers.size).apply {
                    descriptorSetLayoutBinding.immutableSamplers.forEach { put(it.handle) }
                    flip()
                }

                vkobj.set(descriptorSetLayoutBinding.binding, descriptorSetLayoutBinding.descriptorType.value, descriptorSetLayoutBinding.descriptorCount, descriptorSetLayoutBinding.stages.bitfield, pImmutableSamplers)
            }

    fun deserializePipelineShaderStageCreateInfo(pipelineShaderStageCreateInfo: PipelineShaderStageCreateInfo, vkobj: VkPipelineShaderStageCreateInfo, device: Device, mem: MemoryStack): VkPipelineShaderStageCreateInfo {
        vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
                .flags(pipelineShaderStageCreateInfo.flags)
                .stage(pipelineShaderStageCreateInfo.stage.value)
                .module(device.getShaderModule(pipelineShaderStageCreateInfo.moduleInfo).handle)

        vkobj.pName(mem.UTF8(pipelineShaderStageCreateInfo.name))

        if (null != pipelineShaderStageCreateInfo.specializationInfo) {
            val pSpecializationInfo = VkSpecializationInfo.callocStack(mem)

            vkobj.pSpecializationInfo(Serialization.deserializeSpecializationInfo(pipelineShaderStageCreateInfo.specializationInfo, pSpecializationInfo, mem))
        }

        return vkobj
    }

    fun deserializeImageMemoryBarrier(imageMemoryBarrier: ImageMemoryBarrier, vkobj: VkImageMemoryBarrier): VkImageMemoryBarrier {
        vkobj
                .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                .srcAccessMask(AccessFlag.toBitfield(imageMemoryBarrier.srcAccessMask))
                .dstAccessMask(AccessFlag.toBitfield(imageMemoryBarrier.dstAccessMask))
                .oldLayout(imageMemoryBarrier.oldLayout.value)
                .newLayout(imageMemoryBarrier.newLayout.value)
                .srcQueueFamilyIndex(QueueFamily.indexOf(imageMemoryBarrier.srcQueueFamily))
                .dstQueueFamilyIndex(QueueFamily.indexOf(imageMemoryBarrier.dstQueueFamily))
                .image(imageMemoryBarrier.image.handle)

        Serialization.deserializeImageSubresourceRange(imageMemoryBarrier.subresourceRange, vkobj.subresourceRange())

        return vkobj
    }

    fun deserializeImageBlit(imageBlit: ImageBlit, vkobj: VkImageBlit): VkImageBlit {
        deserializeOffset3D(imageBlit.srcOffset0, vkobj.srcOffsets(0))
        deserializeOffset3D(imageBlit.srcOffset1, vkobj.srcOffsets(1))
        deserializeOffset3D(imageBlit.dstOffset0, vkobj.dstOffsets(0))
        deserializeOffset3D(imageBlit.dstOffset1, vkobj.dstOffsets(1))
        deserializeImageSubresourceLayers(imageBlit.srcSubresource, vkobj.srcSubresource())
        deserializeImageSubresourceLayers(imageBlit.dstSubresource, vkobj.dstSubresource())

        return vkobj
    }
}
