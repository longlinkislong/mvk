package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.clamp
import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.asSequence
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.*
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import java.nio.IntBuffer

actual class Swapchain internal constructor(device: Device, actual val info: SwapchainCreateInfo) : Resource {
    private var _handle = VK10.VK_NULL_HANDLE
    private val _presentMode = PresentMode.FIFO
    private lateinit var _support: Support
    private var _images = emptyList<Image>()

    private var _width: Int = 0
    actual val width: Int
        get() = this._width

    private var _height: Int = 0
    actual val height: Int
        get() = this._height

    actual val images: List<Image>
        get() = this._images.toList()

    val handle: Long?
        get() = if (VK10.VK_NULL_HANDLE == _handle) null else _handle

    actual val device: Device by WeakReference(device)

    private class Support(physicalDevice: VkPhysicalDevice, surface: Long) {
        val capabilities: VkSurfaceCapabilitiesKHR = VkSurfaceCapabilitiesKHR.calloc()
        val surfaceFormats: VkSurfaceFormatKHR.Buffer = stackLet { mem ->
            KHRSurface.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, this.capabilities)

            val pFormatCount = mem.callocInt(1)

            vkAssert(KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pFormatCount, null))

            VkSurfaceFormatKHR.calloc(pFormatCount[0]).apply {
                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pFormatCount, this))
            }
        }

        val presentModes: IntBuffer = stackLet { mem ->
            val pPresentModeCount = mem.callocInt(1)

            vkAssert(KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, null))

            MemoryUtil.memCallocInt(pPresentModeCount[0]).apply {
                vkAssert(KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, this))
            }
        }

        fun free() {
            this.capabilities.free()
            this.surfaceFormats.free()
            MemoryUtil.memFree(this.presentModes)
        }
    }

    init {
        this.resize(info.width, info.height)
    }

    actual fun acquireNextImage(): SwapchainBackbuffer {

        val imageAcquireSemaphore = device.acquireSemaphore()

        MemoryStack.stackPush().use { mem ->
            val pImageIndex = mem.callocInt(1)
            val result = KHRSwapchain.vkAcquireNextImageKHR(device.handle, this._handle, -1L, imageAcquireSemaphore.handle, 0L, pImageIndex)

            if (KHRSwapchain.VK_ERROR_OUT_OF_DATE_KHR == result) {
                device.waitIdle()
                this.resize(this._width, this._height)

                return this.acquireNextImage()
            } else {
                vkAssert(result)
            }

            val index = pImageIndex.get()
            val image = this._images[index]

            return SwapchainBackbuffer(image, imageAcquireSemaphore, index)
        }
    }

    override fun free() {
        if (VK10.VK_NULL_HANDLE != this._handle) {
            KHRSwapchain.vkDestroySwapchainKHR(this.device.handle, this._handle, null)
            this._handle = VK10.VK_NULL_HANDLE
            this._support.free()
        }
    }

    actual fun resize(width: Int, height: Int) {
        val physicalDevice = device.handle.physicalDevice
        var format: Format = Format.B8G8R8A8_UNORM

        stackLet { mem ->
            this._support = Support(physicalDevice, this.info.surface)

            if (0 == this._support.surfaceFormats.remaining() || 0 == this._support.presentModes.remaining()) {
                throw UnsupportedOperationException("Surface does not _support any SurfaceFormats or PresentModes!")
            }

            val surfaceFormat = chooseFormat(this._support.surfaceFormats, this.info.surfaceFormat.format, this.info.surfaceFormat.colorSpace)

            format = Format.values().find { it == surfaceFormat.format } ?: Format.B8G8R8A8_UNORM

            val presentMode: Int = when (this._presentMode) {
                PresentMode.IMMEDIATE -> choosePresentMode(this._support.presentModes, KHRSurface.VK_PRESENT_MODE_IMMEDIATE_KHR)
                PresentMode.FIFO_RELAXED -> choosePresentMode(this._support.presentModes, KHRSurface.VK_PRESENT_MODE_FIFO_RELAXED_KHR)
                else -> choosePresentMode(this._support.presentModes, KHRSurface.VK_PRESENT_MODE_FIFO_KHR)
            }

            val imageCount: Int = if (this._support.capabilities.maxImageCount() > 0 && this._support.capabilities.minImageCount() + 1 > this._support.capabilities.maxImageCount()) {
                this._support.capabilities.maxImageCount()
            } else {
                this._support.capabilities.minImageCount() + 1
            }

            val preTransform: Int = if (0 != this._support.capabilities.currentTransform() and KHRSurface.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
                KHRSurface.VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR
            } else {
                this._support.capabilities.currentTransform()
            }

            val extent = this._support.capabilities.currentExtent()
            val minExtent = this._support.capabilities.minImageExtent()
            val maxExtent = this._support.capabilities.maxImageExtent()

            if (-1 == extent.width() || -1 == extent.height()) {
                extent.set(width, height)
            }

            this._width = clamp(extent.width(), minExtent.width(), maxExtent.width())
            this._height = clamp(extent.height(), minExtent.height(), maxExtent.height())

            extent.set(this._width, this._height)

            LOGGER.info("Resized Swapchain to {}x{}", this.width, this.height)

            val pvkSwapchainCI = VkSwapchainCreateInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR)
                    .pNext(MemoryUtil.NULL)
                    .surface(this.info.surface)
                    .minImageCount(imageCount)
                    .imageFormat(surfaceFormat.format.value)
                    .imageColorSpace(surfaceFormat.colorSpace.value)
                    .preTransform(preTransform)
                    .imageArrayLayers(1)
                    .imageSharingMode(VK10.VK_SHARING_MODE_EXCLUSIVE)
                    .pQueueFamilyIndices(null)
                    .presentMode(presentMode)
                    .oldSwapchain(this._handle)
                    .clipped(true)
                    .compositeAlpha(KHRSurface.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
                    .imageExtent(extent)
                    .imageUsage(ImageUsageFlag.toBitfield(setOf(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.TRANSFER_DST)))

            val pHandle = mem.callocLong(1)

            vkAssert(KHRSwapchain.vkCreateSwapchainKHR(device.handle, pvkSwapchainCI, null, pHandle))

            if (VK10.VK_NULL_HANDLE != this._handle) {
                KHRSwapchain.vkDestroySwapchainKHR(device.handle, this._handle, null)
            }

            this._handle = pHandle.get()
        }

        stackLet { mem ->
            val pImageCount = mem.callocInt(1)

            vkAssert(KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, this._handle, pImageCount, null))

            val pvkSwapchainImages = mem.callocLong(pImageCount.get(0))

            vkAssert(KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, this._handle, pImageCount, pvkSwapchainImages))

            val imageCI = ImageCreateInfo(
                    imageType = ImageType.IMAGE_2D,
                    format = format,
                    extent = Extent3D(this._width, this._height, 1),
                    usage = setOf(ImageUsageFlag.COLOR_ATTACHMENT, ImageUsageFlag.TRANSFER_DST),
                    initialLayout = ImageLayout.PRESENT_SRC_KHR)

            this._images = pvkSwapchainImages.asSequence()
                    .map { handle -> Image(device, imageCI, handle) }
                    .toList()
        }
    }

    companion object {
        private val SLEEP_INTERVAL = java.lang.Long.getLong("com.longlinkislong.jvk.Swapchain.SLEEP_INTERVAL", 1L)!!
        private val LOGGER = LoggerFactory.getLogger(Swapchain::class.java)

        private fun chooseFormat(availableFormats: VkSurfaceFormatKHR.Buffer, format: Format, colorSpace: ColorSpace): SurfaceFormat {
            if (1 == availableFormats.remaining() && VK10.VK_FORMAT_UNDEFINED == availableFormats.get(0).format()) {
                return SurfaceFormat.DEFAULT
            }

            try {
                availableFormats.mark()

                while (availableFormats.hasRemaining()) {
                    val current = availableFormats.get()

                    if (format.value == current.format() && colorSpace.value == current.colorSpace()) {
                        return SurfaceFormat(format, colorSpace)
                    }
                }
            } finally {
                availableFormats.reset()
            }

            val selectedFormat = Format.toFormat(availableFormats[0].format()) ?: throw UnsupportedOperationException("Unsupported Format: ${availableFormats[0]}")
            val selectedColorSpace = ColorSpace.toColorSpace(availableFormats[0].colorSpace()) ?: throw UnsupportedOperationException("Unsupported ColorSpace: ${availableFormats[0].colorSpace()}")

            return SurfaceFormat(selectedFormat, selectedColorSpace)
        }

        private fun choosePresentMode(availablePresentModes: IntBuffer, presentMode: Int): Int {
            try {
                availablePresentModes.mark()

                while (availablePresentModes.hasRemaining()) {
                    if (presentMode == availablePresentModes.get()) {
                        return presentMode
                    }
                }
            } finally {
                availablePresentModes.reset()
            }

            return if (availablePresentModes.hasRemaining()) availablePresentModes.get(0) else KHRSurface.VK_PRESENT_MODE_FIFO_KHR
        }
    }
}
