package com.longlinkislong.mvk.memory

import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDevice

import java.nio.ByteBuffer

import com.longlinkislong.mvk.Util.vkAssert

interface MemoryBlock {
    val handle: Long

    val offset: Long

    val size: Long

    val device: VkDevice

    fun map(): ByteBuffer

    fun unmap()

    fun free()

    fun bindToImage(img: Long) = vkAssert(VK10.vkBindImageMemory(device, img, handle, offset))

    fun bindToBuffer(buf: Long) = vkAssert(VK10.vkBindBufferMemory(device, buf, handle, offset))
}
