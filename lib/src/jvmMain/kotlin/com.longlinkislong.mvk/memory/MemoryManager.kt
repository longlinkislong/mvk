package com.longlinkislong.mvk.memory

import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDevice
import org.lwjgl.vulkan.VkMemoryRequirements
import org.lwjgl.vulkan.VkPhysicalDeviceMemoryProperties
import java.lang.ref.WeakReference

class MemoryManager(device: VkDevice) {

    private val _device: WeakReference<VkDevice> = WeakReference(device)
    private val _largeBufferHeaps: Array<UniqueMemoryAllocator?>
    private val _largeImageHeaps: Array<UniqueMemoryAllocator?>
    private val _smallBufferHeaps = ArrayList<SlabMemoryAllocator>()
    private val _smallImageHeaps = ArrayList<SlabMemoryAllocator>()
    private val _standardBufferHeaps = ArrayList<BuddyBlockMemoryAllocator>()
    private val _standardImageHeaps = ArrayList<BuddyBlockMemoryAllocator>()
    private val _pPhysicalDeviceMemoryProperties: VkPhysicalDeviceMemoryProperties = VkPhysicalDeviceMemoryProperties.calloc().apply { VK10.vkGetPhysicalDeviceMemoryProperties(device.physicalDevice, this) }
    private val _lock = Any()

    init {
        val typeCount = _pPhysicalDeviceMemoryProperties.memoryTypeCount()

        this._largeBufferHeaps = arrayOfNulls(typeCount)
        this._largeImageHeaps = arrayOfNulls(typeCount)
    }

    val device: VkDevice
        get() = _device.get() ?: throw IllegalStateException("Device was lost!")

    var lazyUnmap: Boolean = false

    fun garbageCollect() = synchronized(_lock) {
        sequenceOf(this._smallBufferHeaps, this._smallImageHeaps, this._standardBufferHeaps, this._standardImageHeaps)
                .forEach {
                    it.asSequence()
                            .filter(MemoryAllocator::isEmpty)
                            .forEach(MemoryAllocator::free)

                    it.removeIf(MemoryAllocator::isEmpty)
                }
    }

    fun free() = synchronized(_lock) {
        sequenceOf(this._largeBufferHeaps, this._largeImageHeaps)
                .flatMap { it.asSequence() }
                .filterNotNull()
                .forEach(UniqueMemoryAllocator::free)

        sequenceOf(this._smallBufferHeaps, this._smallImageHeaps, this._standardBufferHeaps, this._standardImageHeaps)
                .flatMap { it.asSequence() }
                .filterNotNull()
                .forEach(MemoryAllocator::free)
    }

    fun allocateImageMemory(pMemReqs: VkMemoryRequirements, properties: Int): MemoryBlock {
        val index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties)
        val size = pMemReqs.size()

        synchronized(_lock) {
            return when {
                size > LARGE_ALLOC_THRESHOLD -> {
                    if (this._largeImageHeaps[index] == null) {
                        this._largeImageHeaps[index] = UniqueMemoryAllocator(device, index).apply {
                            this.lazyUnmap = this@MemoryManager.lazyUnmap
                        }
                    }

                    this._largeImageHeaps[index]!!.malloc(MemoryType.IMAGE, pMemReqs)
                }
                size <= SMALL_ALLOC_THRESHOLD -> allocate(MemoryType.IMAGE, pMemReqs, index, this._smallImageHeaps) {
                    SlabMemoryAllocator(device, index, SMALL_HEAP_SIZES).apply {
                        this.lazyUnmap = this@MemoryManager.lazyUnmap
                    }
                }
                else -> allocate(MemoryType.IMAGE, pMemReqs, index, this._standardImageHeaps) {
                    BuddyBlockMemoryAllocator(device, index, MINIMUM_IMAGE_SUBDIV_SIZE, STANDARD_IMAGE_HEAP_SIZE).apply {
                        this.lazyUnmap = this@MemoryManager.lazyUnmap
                    }
                }
            }
        }
    }

    fun allocateBufferMemory(pMemReqs: VkMemoryRequirements, properties: Int): MemoryBlock {
        val index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties)
        val size = pMemReqs.size()

        synchronized(_lock) {
            return when {
                size >= LARGE_ALLOC_THRESHOLD -> {
                    if (this._largeBufferHeaps[index] == null) {
                        this._largeBufferHeaps[index] = UniqueMemoryAllocator(device, index).apply {
                            this.lazyUnmap = this@MemoryManager.lazyUnmap
                        }
                    }

                    this._largeBufferHeaps[index]!!.malloc(MemoryType.BUFFER, pMemReqs)
                }
                size < SMALL_ALLOC_THRESHOLD -> allocate(MemoryType.BUFFER, pMemReqs, index, this._smallBufferHeaps) {
                    SlabMemoryAllocator(device, index, SMALL_HEAP_SIZES).apply {
                        this.lazyUnmap = this@MemoryManager.lazyUnmap
                    }
                }
                else -> allocate(MemoryType.BUFFER, pMemReqs, index, this._standardBufferHeaps) {
                    BuddyBlockMemoryAllocator(device, index, MINIMUM_BUFFER_SUBDIV_SIZE, STANDARD_BUFFER_HEAP_SIZE).apply {
                        this.lazyUnmap = this@MemoryManager.lazyUnmap
                    }
                }
            }
        }
    }

    fun allocateExportableBufferMemory(pMemReqs: VkMemoryRequirements, properties: Int, exportedType: Int): MemoryBlock {
        val index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties)

        synchronized(_lock) {
            if (null == this._largeBufferHeaps[index]) {
                this._largeBufferHeaps[index] = UniqueMemoryAllocator(this.device, index)
            }

            return this._largeBufferHeaps[index]!!.mallocShared(MemoryType.BUFFER, pMemReqs, exportedType)
        }
    }

    fun allocateExportableImageMemory(pMemReqs: VkMemoryRequirements, properties: Int, exportedType: Int): MemoryBlock {
        val index = this.getMemoryTypeIndex(pMemReqs.memoryTypeBits(), properties)

        synchronized(_lock) {
            if (null == this._largeImageHeaps[index]) {
                this._largeImageHeaps[index] = UniqueMemoryAllocator(this.device, index)
            }

            return this._largeImageHeaps[index]!!.mallocShared(MemoryType.IMAGE, pMemReqs, exportedType)
        }
    }

    private fun getMemoryTypeIndex(
            typeBits: Int, requirementsMask: Int): Int {

        val props = this._pPhysicalDeviceMemoryProperties

        for (i in 0 until props.memoryTypeCount()) {
            if (typeBits and (1 shl i) != 0) {
                if (props.memoryTypes(i).propertyFlags() and requirementsMask == requirementsMask) {
                    return i
                }
            }
        }

        throw UnsupportedOperationException("No MemoryType exists with the required features!")
    }

    companion object {
        private const val LARGE_ALLOC_THRESHOLD = 128 * 1024 * 1024L
        private const val SMALL_ALLOC_THRESHOLD = 32 * 1024L
        private const val STANDARD_BUFFER_HEAP_SIZE = 1024 * 1024 * 128L
        private const val STANDARD_IMAGE_HEAP_SIZE = 1024 * 1024 * 256L
        private const val MINIMUM_BUFFER_SUBDIV_SIZE = 1024 * 4L
        private const val MINIMUM_IMAGE_SUBDIV_SIZE = 1024 * 4L
        private val SMALL_HEAP_SIZES: List<SlabMemoryAllocator.SlabSizeInfo> = listOf(
                SlabMemoryAllocator.SlabSizeInfo(4 * 1024L, 256),
                SlabMemoryAllocator.SlabSizeInfo(8 * 1024L, 128),
                SlabMemoryAllocator.SlabSizeInfo(16 * 1024L, 64),
                SlabMemoryAllocator.SlabSizeInfo(32 * 1024L, 32))

        private fun <MemAllocT : MemoryAllocator> allocate(
                memType: MemoryType, pMemReqs: VkMemoryRequirements, index: Int,
                allocator: MutableList<MemAllocT>, constructor: () -> MemAllocT): MemoryBlock {

            val allocation = allocator.asSequence()
                    .filter { it.typeIndex == index }
                    .map {
                        try {
                            it.malloc(memType, pMemReqs)
                        } catch (err: OutOfMemoryError) {
                            // selected heap is too full; try next
                            null
                        }
                    }
                    .filterNotNull()
                    .firstOrNull()

            return allocation ?: constructor()
                    .also { allocator.add(it) }
                    .malloc(memType, pMemReqs)
        }
    }

}
