package com.longlinkislong.mvk.memory

import com.longlinkislong.mvk.Util
import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDevice
import org.lwjgl.vulkan.VkMemoryAllocateInfo
import org.lwjgl.vulkan.VkMemoryRequirements
import java.lang.ref.WeakReference
import java.nio.ByteBuffer

internal class BuddyBlockMemoryAllocator(device: VkDevice, override val typeIndex: Int, private val minSize: Long, private val size: Long) : MemoryAllocator {
    private val _lock = Any()
    private val _device: WeakReference<VkDevice> = WeakReference(device)
    private val _handle: Long = stackLet { mem ->
        val info = VkMemoryAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                .memoryTypeIndex(typeIndex)
                .allocationSize(this.size)

        val pHandle = mem.callocLong(1)

        vkAssert(VK10.vkAllocateMemory(device, info, null, pHandle))

        pHandle[0]
    }

    private val _root: BuddyBlock = BuddyBlock(0L, this.size, null)

    private var _address: ByteBuffer? = null
    private var _mapCount: Int = 0

    override var lazyUnmap: Boolean = false

    override val isEmpty: Boolean
        get() = synchronized(_lock) {
            this._root.reclaim()

            this._root.type === MemoryType.FREE
        }

    override fun malloc(type: MemoryType, pMemReqs: VkMemoryRequirements): MemoryBlock {
        val size = pMemReqs.size()
        val alignment = pMemReqs.alignment()
        val alloc: BuddyBlock?

        synchronized(_lock) {
            //this._root.reclaim()

            alloc = this._root.sub(size, alignment)
        }

        if (alloc == null) {
            throw OutOfMemoryError()
        }

        alloc.type = type
        alloc.align(alignment)

        return alloc
    }

    override fun free() = synchronized(_lock) {
        if (this._mapCount > 0) {
            VK10.vkUnmapMemory(this.device, this._handle)
        }

        VK10.vkFreeMemory(this.device, this._handle, null)
    }

    override val device: VkDevice
        get() = this._device.get() ?: throw IllegalStateException("Device was lost!")

    private fun map(): ByteBuffer = synchronized(_lock) {
        if (this._address == null) {
            stackLet { mem ->
                val ppData = mem.callocPointer(1)

                vkAssert(VK10.vkMapMemory(this.device, this._handle, 0L, this.size, 0, ppData))

                this._address = ppData.getByteBuffer(0, this.size.toInt())
            }
        }

        this._mapCount++

        return this._address!!
    }

    private fun unmap() = synchronized(_lock) {
        if (0 == --this._mapCount && !this.lazyUnmap) {
            this._address = null
            VK10.vkUnmapMemory(this.device, this._handle)
        }
    }

    private inner class BuddyBlock(private val _offset: Long, val _size: Long, private var parent: BuddyBlock?) : MemoryBlock {
        var type: MemoryType = MemoryType.FREE
        var alignedOffset: Long = _offset
        var left: BuddyBlock? = null
        var right: BuddyBlock? = null

        override val handle: Long
            get() = this@BuddyBlockMemoryAllocator._handle

        override val device: VkDevice
            get() = this@BuddyBlockMemoryAllocator.device

        fun reclaim() {
            val l = this.left
            val r = this.right

            if (l != null && r != null) {
                l.reclaim()
                r.reclaim()

                if (l.type == MemoryType.FREE && r.type == MemoryType.FREE) {
                    this.type = MemoryType.FREE
                    this.left = null
                    this.right = null
                }
            }
        }

        fun sub(size: Long, alignment: Long): BuddyBlock? {
            if (this.left != null) {
                val out = this.left!!.sub(size, alignment)

                if (out != null) {
                    return out
                }
            }

            if (this.right != null) {
                val out = this.right!!.sub(size, alignment)

                if (out != null) {
                    return out
                }
            }

            if (this.type !== MemoryType.FREE) {
                return null
            }

            this.align(alignment)

            if (this.size < size) {
                return null
            }

            val halfSize = this.size / 2

            if (halfSize < size || halfSize < minSize) {
                // cant split; leave
                return this
            }

            // try subdividing
            val left = BuddyBlock(this._offset, halfSize, this)
            val right = BuddyBlock(this._offset + halfSize, halfSize, this)

            left.align(alignment)
            right.align(alignment)

            if (left.size >= size) {
                this.type = MemoryType.UNKNOWN
                this.left = left
                this.right = right

                val out = left.sub(size, alignment)

                if (out != null) {
                    return out
                }
            }

            if (right.size >= size) {
                this.type = MemoryType.UNKNOWN
                this.left = left
                this.right = right

                val out = right.sub(size, alignment)

                if (out != null) {
                    return out
                }
            }

            return this
        }

        override fun toString(): String =
                String.format("BuddyBlock: %s PTR: +0x%x Size: 0x%x", type, _offset, _size)

        fun align(alignment: Long) {
            this.alignedOffset = Util.alignUp(this._offset, alignment)
        }

        override val offset: Long
            get() = this.alignedOffset

        override val size: Long
            get() = this._offset + this._size - this.alignedOffset

        override fun map(): ByteBuffer {
            val superBlock = this@BuddyBlockMemoryAllocator.map()

            return MemoryUtil.memSlice(superBlock, this.offset.toInt(), this.size.toInt())
        }

        override fun unmap() = this@BuddyBlockMemoryAllocator.unmap()

        override fun free() {
            this.type = MemoryType.FREE
            this.left = null
            this.right = null

            val p = this.parent

            if (null != p) {
                if (p.left == this && p.right?.type == MemoryType.FREE) {
                    p.free()
                } else if (p.right == this && p.left?.type == MemoryType.FREE) {
                    p.free()
                }
            }
        }
    }
}
