package com.longlinkislong.mvk.memory

import com.longlinkislong.mvk.Util
import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference
import java.nio.ByteBuffer
import java.util.*

internal class UniqueMemoryAllocator(device: VkDevice, override val typeIndex: Int) : MemoryAllocator {
    private val _device: WeakReference<VkDevice> = WeakReference(device)
    private val allAllocations = HashSet<UniqueMemoryBlock>()
    private val lock = Any()

    override val device: VkDevice
        get() = _device.get() ?: throw IllegalStateException("Device was lost!")

    override val isEmpty: Boolean = false
    override var lazyUnmap: Boolean = false

    override fun malloc(type: MemoryType, pMemReqs: VkMemoryRequirements): MemoryBlock {
        val alignedSize = Util.alignUp(pMemReqs.size(), pMemReqs.alignment())

        return UniqueMemoryBlock(type, alignedSize)
    }

    fun mallocShared(type: MemoryType, pMemReqs: VkMemoryRequirements, exportedType: Int): MemoryBlock {
        val alignedSize = Util.alignUp(pMemReqs.size(), pMemReqs.alignment())

        return UniqueMemoryBlock(type, alignedSize, exportedType)
    }

    override fun free() = synchronized(lock) {
        this.allAllocations.forEach { alloc -> VK10.vkFreeMemory(this.device, alloc.handle, null) }
        this.allAllocations.clear()
    }

    private inner class UniqueMemoryBlock : MemoryBlock {
        override val handle: Long
        override val size: Long
        private val _type: MemoryType

        override val device: VkDevice
            get() = this@UniqueMemoryAllocator.device

        override val offset: Long = 0L

        constructor(type: MemoryType, size: Long, exportedType: Int) {
            this.size = size
            this._type = type

            this.handle = stackLet { mem ->
                val pHandle = mem.callocLong(1)
                val pExportInfo = VkExportMemoryAllocateInfo.callocStack(mem)
                        .sType(VK11.VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO)
                        .handleTypes(exportedType)

                val pAllocateInfo = VkMemoryAllocateInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                        .pNext(pExportInfo.address())
                        .memoryTypeIndex(typeIndex)
                        .allocationSize(size)

                vkAssert(VK10.vkAllocateMemory(device, pAllocateInfo, null, pHandle))

                pHandle[0]
            }
        }

        constructor(type: MemoryType, size: Long) {
            this.size = size
            this._type = type

            this.handle = stackLet { mem ->
                val pHandle = mem.callocLong(1)
                val pAllocateInfo = VkMemoryAllocateInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                        .memoryTypeIndex(typeIndex)
                        .allocationSize(size)

                vkAssert(VK10.vkAllocateMemory(device, pAllocateInfo, null, pHandle))

                pHandle[0]
            }

            synchronized(lock) {
                allAllocations.add(this)
            }
        }

        override fun toString(): String =
                String.format("ContextImageType: %s Offset: +0x%x Size: 0x%x", this._type, this.offset, this.size)

        override fun map(): ByteBuffer = stackLet { mem ->
            val ppData = mem.callocPointer(1)

            vkAssert(VK10.vkMapMemory(device, this.handle, 0, this.size, 0, ppData))

            return ppData.getByteBuffer(0, this.size.toInt())
        }

        override fun unmap() {
            if (!this@UniqueMemoryAllocator.lazyUnmap) {
                VK10.vkUnmapMemory(device, this.handle)
            }
        }

        override fun free() = synchronized(lock) {
            if (allAllocations.remove(this)) {
                VK10.vkFreeMemory(device, this.handle, null)
            }
        }
    }
}
