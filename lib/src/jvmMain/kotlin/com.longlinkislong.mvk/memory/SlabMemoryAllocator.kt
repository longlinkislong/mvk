package com.longlinkislong.mvk.memory

import com.longlinkislong.mvk.Util
import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDevice
import org.lwjgl.vulkan.VkMemoryAllocateInfo
import org.lwjgl.vulkan.VkMemoryRequirements
import java.lang.ref.WeakReference
import java.nio.ByteBuffer

internal class SlabMemoryAllocator(device: VkDevice, override val typeIndex: Int, sizeInfos: List<SlabSizeInfo>) : MemoryAllocator {

    private val _lock = Any()
    private val _device: WeakReference<VkDevice> = WeakReference(device)
    private val _size: Long = sizeInfos.asSequence()
            .map { info -> info.size * info.count }
            .sum()

    private val handle: Long = stackLet { mem ->
        val info = VkMemoryAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                .memoryTypeIndex(typeIndex)
                .allocationSize(this._size)

        val pHandle = mem.callocLong(1)

        vkAssert(VK10.vkAllocateMemory(device, info, null, pHandle))

        pHandle[0]
    }

    private val lists: MutableList<SlabList>
    private var address: ByteBuffer? = null
    private var mapCount: Int = 0

    override var lazyUnmap: Boolean = false

    override val device: VkDevice
        get() = _device.get() ?: throw IllegalStateException("Device was lost!")

    override val isEmpty: Boolean
        get() {
            synchronized(_lock) {
                for (slabList in this.lists) {
                    for (slab in slabList.slabs) {
                        if (slab.type !== MemoryType.FREE) {
                            return false
                        }
                    }
                }
            }

            return true
        }

    internal data class SlabSizeInfo(val size: Long, val count: Int)

    private data class SlabList(val size: Long, val slabs: List<MemorySlab>)

    init {
        this.lists = ArrayList()

        var offset = 0L
        for (sizeInfo in sizeInfos) {
            val baseOffset = offset
            val slabs = (0 until sizeInfo.count)
                    .map { idx -> baseOffset + sizeInfo.size * idx }
                    .map { off -> MemorySlab(off, sizeInfo.size) }
                    .toList()

            this.lists.add(SlabList(sizeInfo.size, slabs))
            offset += sizeInfo.count * sizeInfo.size
        }
    }

    override fun malloc(type: MemoryType, pMemReqs: VkMemoryRequirements): MemoryBlock {
        synchronized(_lock) {
            val slabListIt = this.lists.listIterator()
            val requiredSize = pMemReqs.size()
            val alignment = pMemReqs.alignment()

            while (slabListIt.hasNext()) {
                val slabList = slabListIt.next()

                if (slabList.size < requiredSize) {
                    continue
                }

                for (slab in slabList.slabs) {
                    if (slab.type !== MemoryType.FREE) {
                        continue
                    }

                    slab.align(alignment)

                    if (slab.size >= requiredSize) {
                        slab.type = type

                        return slab
                    } else {
                        break
                    }
                }
            }
        }

        throw OutOfMemoryError()
    }

    private fun map(): ByteBuffer = synchronized(_lock) {
        if (null == this.address) {
            this.address = stackLet { mem ->
                val ppData = mem.callocPointer(1)

                vkAssert(VK10.vkMapMemory(this.device, this.handle, 0L, this._size, 0, ppData))

                ppData.getByteBuffer(0, this._size.toInt())
            }
        }

        this.mapCount++

        return this.address!!
    }

    private fun unmap() = synchronized(_lock) {
        if (0 == --this.mapCount && !this.lazyUnmap) {
            this.address = null
            VK10.vkUnmapMemory(this.device, this.handle)
        }
    }

    override fun free() = synchronized(_lock) {
        this.lists.clear()

        if (this.mapCount > 0) {
            VK10.vkUnmapMemory(this.device, this.handle)
        }

        VK10.vkFreeMemory(this.device, this.handle, null)
    }

    private inner class MemorySlab(private val _offset: Long, private val _size: Long) : MemoryBlock {
        private var _alignedOffset: Long = _offset
        var type: MemoryType = MemoryType.FREE

        override val handle: Long
            get() = this@SlabMemoryAllocator.handle

        override val device: VkDevice
            get() = this@SlabMemoryAllocator.device

        override val offset: Long
            get() = this._alignedOffset

        override val size: Long
            get() = _offset + _size - _alignedOffset

        fun align(alignment: Long) {
            this._alignedOffset = Util.alignUp(this.offset, alignment)
        }

        override fun map(): ByteBuffer {
            val superBlock = this@SlabMemoryAllocator.map()

            return MemoryUtil.memSlice(superBlock, offset.toInt(), size.toInt())
        }

        override fun unmap() = this@SlabMemoryAllocator.unmap()

        override fun free() = synchronized(_lock) {
            this._alignedOffset = this.offset
            this.type = MemoryType.FREE
        }

        override fun toString(): String = "MemorySlabType: $type PTR: +$offset Size: $_size"
    }
}
