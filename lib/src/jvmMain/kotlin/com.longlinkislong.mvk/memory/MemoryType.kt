package com.longlinkislong.mvk.memory

enum class MemoryType {
    FREE,
    IMAGE,
    BUFFER,
    UNKNOWN;

    fun conflicts(neighbor: MemoryType): Boolean = when {
        this == UNKNOWN || neighbor == UNKNOWN -> true
        this == neighbor -> false
        else -> this != FREE && neighbor != FREE
    }
}
