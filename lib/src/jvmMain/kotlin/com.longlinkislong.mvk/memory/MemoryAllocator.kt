package com.longlinkislong.mvk.memory

import org.lwjgl.vulkan.VkDevice
import org.lwjgl.vulkan.VkMemoryRequirements

interface MemoryAllocator {

    val device: VkDevice

    val typeIndex: Int

    val isEmpty: Boolean

    fun malloc(type: MemoryType, pMemReqs: VkMemoryRequirements): MemoryBlock

    fun free()

    var lazyUnmap: Boolean
}
