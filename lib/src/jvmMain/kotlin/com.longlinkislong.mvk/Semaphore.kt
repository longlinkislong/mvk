package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference

actual class Semaphore internal constructor(
        pool: SemaphorePool,
        override val handle: Long) : ManagedResource, ExternalHandle {

    private val semaphorePool: SemaphorePool by WeakReference(pool)

    actual val device: Device
        get() = this.semaphorePool.device

    override val fd: Int by lazy {
        stackLet {
            mem ->

            val pvkSemaphoreGetFdInfo = VkSemaphoreGetFdInfoKHR.callocStack(mem)
                    .sType(KHRExternalSemaphoreFd.VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR)
                    .handleType(VK11.VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT)
                    .semaphore(this.handle)

            val pFd = mem.callocInt(1)

            vkAssert(KHRExternalSemaphoreFd.vkGetSemaphoreFdKHR(this.device.handle, pvkSemaphoreGetFdInfo, pFd))

            pFd[0]
        }
    }

    override val win32Handle: Long by lazy {
        stackLet { mem ->
            val pvkSemaphoreGetWin32HandleInfo = VkSemaphoreGetWin32HandleInfoKHR.callocStack(mem)
                    .sType(KHRExternalMemoryWin32.VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR)
                    .handleType(VK11.VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT)
                    .semaphore(this.handle)

            val pHandle = mem.callocPointer(1)

            vkAssert(KHRExternalSemaphoreWin32.vkGetSemaphoreWin32HandleKHR(this.device.handle, pvkSemaphoreGetWin32HandleInfo, pHandle))

            pHandle[0]
        }
    }

    override fun release() = this.semaphorePool.releaseSemaphore(this)
}
