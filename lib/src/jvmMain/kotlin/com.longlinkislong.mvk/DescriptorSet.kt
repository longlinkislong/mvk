package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.applyStack
import com.longlinkislong.mvk.util.getValue
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDescriptorBufferInfo
import org.lwjgl.vulkan.VkDescriptorImageInfo
import org.lwjgl.vulkan.VkWriteDescriptorSet
import java.lang.ref.WeakReference

actual class DescriptorSet internal constructor(
        pool: DescriptorPool,
        val poolIndex: Int,
        override val handle: Long) : ManagedResource, Handle {

    private val _descriptorPool: DescriptorPool by WeakReference(pool)

    actual val device: Device
        get() = this._descriptorPool.device

    override fun release() = this._descriptorPool.releaseDescriptorSet(this)

    actual fun writeBuffer(type: DescriptorType, binding: Int, buffer: Buffer): DescriptorSet =
            this.writeBuffer(type, binding, buffer, 0L, VK10.VK_WHOLE_SIZE)

    actual fun writeBuffer(type: DescriptorType, binding: Int, buffer: Buffer, offset: Long, size: Long): DescriptorSet = applyStack { mem ->
        val pBufferInfo = VkDescriptorBufferInfo.callocStack(1, mem)

        pBufferInfo[0].set(buffer.handle, offset, size)

        val pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem)

        pvkWriteDescriptorSet[0]
                .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                .dstSet(this.handle)
                .dstBinding(binding)
                .descriptorType(type.value)
                .pBufferInfo(pBufferInfo)

        VK10.vkUpdateDescriptorSets(this.device.handle, pvkWriteDescriptorSet, null)
    }

    actual fun writeTexelBuffer(type: DescriptorType, binding: Int, view: BufferView): DescriptorSet = applyStack { mem ->
        val pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem)

        pvkWriteDescriptorSet[0]
                .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                .dstSet(this.handle)
                .dstBinding(binding)
                .descriptorType(type.value)
                .pTexelBufferView(mem.longs(view.handle))

        VK10.vkUpdateDescriptorSets(this.device.handle, pvkWriteDescriptorSet, null)
    }

    actual fun writeImageArray(
            type: DescriptorType, binding: Int,
            samplers: List<Sampler>?,
            imageLayout: ImageLayout,
            views: List<ImageView>): DescriptorSet = applyStack { mem ->

        val pImageInfo = VkDescriptorImageInfo.callocStack(views.size, mem)

        views.forEachIndexed { index, imageView ->
            if (null != samplers) {
                pImageInfo[index].sampler(samplers[index].handle)
            }

            pImageInfo[index]
                    .imageView(imageView.handle)
                    .imageLayout(imageLayout.value)
        }

        val pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem)
                .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                .dstSet(this.handle)
                .dstBinding(binding)
                .descriptorType(type.value)
                .pImageInfo(pImageInfo)

        VK10.vkUpdateDescriptorSets(this.device.handle, pvkWriteDescriptorSet, null)
    }

    actual fun writeImage(
            type: DescriptorType, binding: Int,
            sampler: Sampler?, imageLayout: ImageLayout, imageView: ImageView): DescriptorSet = applyStack { mem ->

        val pImageInfo = VkDescriptorImageInfo.callocStack(1, mem)

        if (null != sampler) {
            pImageInfo.sampler(sampler.handle)
        }

        pImageInfo
                .imageLayout(imageLayout.value)
                .imageView(imageView.handle)

        val pvkWriteDescriptorSet = VkWriteDescriptorSet.callocStack(1, mem)

        pvkWriteDescriptorSet[0]
                .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                .dstSet(this.handle)
                .dstBinding(binding)
                .descriptorType(type.value)
                .pImageInfo(pImageInfo)

        VK10.vkUpdateDescriptorSets(this.device.handle, pvkWriteDescriptorSet, null)
    }
}
