package com.longlinkislong.mvk

internal data class DescriptorPoolCreateInfo(
        val flags: Int = 0,
        val maxSets: Int,
        val poolSizes: List<DescriptorPoolSize>)