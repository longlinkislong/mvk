package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.asSequence
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.glfw.GLFWVulkan
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.*
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.collections.HashSet

actual class Instance private constructor() : Resource {

    val handle: VkInstance = stackLet { mem ->
        val pvkApplicationInfo = VkApplicationInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_APPLICATION_INFO)
                .apiVersion(API_VERSION)
                .applicationVersion(APPLICATION_VERSION)
                .pApplicationName(mem.UTF8(APPLICATION_NAME))
                .engineVersion(ENGINE_VERSION)
                .pEngineName(mem.UTF8(ENGINE_NAME))

        val ppEnabledLayerNames = mem.callocPointer(enabledLayers.size).apply {
            enabledLayers.forEach { put(mem.UTF8(it.value)) }
            flip()
        }

        val ppEnabledExtensionNames = mem.callocPointer(enabledExtensions.size).apply {
            enabledExtensions.forEach { put(mem.UTF8(it.value)) }
            flip()
        }

        val pvkInstanceCI = VkInstanceCreateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
                .pApplicationInfo(pvkApplicationInfo)
                .ppEnabledExtensionNames(ppEnabledExtensionNames)
                .ppEnabledLayerNames(ppEnabledLayerNames)

        val pHandle = mem.callocPointer(1)

        vkAssert(VK10.vkCreateInstance(pvkInstanceCI, null, pHandle))

        VkInstance(pHandle[0], pvkInstanceCI)
    }

    private val pCallbackHandle: Long = stackLet { mem ->
        if (enabledExtensions.contains(InstanceExtension.of(EXTDebugReport.VK_EXT_DEBUG_REPORT_EXTENSION_NAME))) {
            val pvkDebugReportCallbackCI = VkDebugReportCallbackCreateInfoEXT.callocStack(mem)
                    .sType(EXTDebugReport.VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT)
                    .flags(EXTDebugReport.VK_DEBUG_REPORT_ERROR_BIT_EXT or EXTDebugReport.VK_DEBUG_REPORT_WARNING_BIT_EXT or EXTDebugReport.VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT or EXTDebugReport.VK_DEBUG_REPORT_DEBUG_BIT_EXT or EXTDebugReport.VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
                    .pfnCallback { flags, objectType, `object`, location, messageCode, pLayerPrefix, pMessage, pUserData -> debugCallback(flags, objectType, `object`, location, messageCode, pLayerPrefix, pMessage, pUserData) }

            val pCallbackHandle = mem.callocLong(1)

            vkAssert(EXTDebugReport.vkCreateDebugReportCallbackEXT(this.handle, pvkDebugReportCallbackCI, null, pCallbackHandle))

            pCallbackHandle[0]
        } else {
            VK10.VK_NULL_HANDLE
        }
    }

    actual val physicalDevices: List<PhysicalDevice> = stackLet { mem ->
        val pPhysicalDeviceCount = mem.callocInt(1)

        VK10.vkEnumeratePhysicalDevices(this.handle, pPhysicalDeviceCount, null)

        val physicalDeviceCount = pPhysicalDeviceCount[0]
        val pPhysicalDevices = mem.callocPointer(physicalDeviceCount)

        VK10.vkEnumeratePhysicalDevices(this.handle, pPhysicalDeviceCount, pPhysicalDevices)

        pPhysicalDevices.asSequence()
                .map { PhysicalDevice(VkPhysicalDevice(it, this.handle)) }
                .toList()
    }

    override fun free() {
        if (VK10.VK_NULL_HANDLE != pCallbackHandle) {
            EXTDebugReport.vkDestroyDebugReportCallbackEXT(this.handle, pCallbackHandle, null)
        }

        VK10.vkDestroyInstance(this.handle, null)
    }

    actual fun createWindowSurface(window: Long): Long = stackLet { mem ->
        val pHandle = mem.callocLong(1)

        vkAssert(GLFWVulkan.glfwCreateWindowSurface(this.handle, window, null, pHandle))

        pHandle[0]
    }

    actual companion object {
        private val DEFAULT_LAYERS = System.getProperty("Instance.DEFAULT_LAYERS", "")
                .split(",")
                .asSequence()
                .map(String::trim)
                .filter(String::isNotEmpty)
                .map(InstanceLayer.Companion::of)
                .toSet()

        private val DEFAULT_EXTENSIONS = System.getProperty("Instance.DEFAULT_EXTENSIONS", "")
                .split(",")
                .asSequence()
                .map(String::trim)
                .filter(String::isNotEmpty)
                .map(InstanceExtension.Companion::of)
                .toSet()


        private var APPLICATION_NAME = "JVK Application"
        private var ENGINE_NAME = "JVK"
        private var ENGINE_VERSION = VK10.VK_MAKE_VERSION(1, 0, 0)
        private var API_VERSION = VK10.VK_API_VERSION_1_0
        private var APPLICATION_VERSION = VK10.VK_MAKE_VERSION(1, 0, 0)
        private val LOGGER = LoggerFactory.getLogger(Instance::class.java)

        actual var applicationName: String = "MVK Application"
        actual var engineName: String = "MVK"
        actual var applicationVersion: Int = VK10.VK_MAKE_VERSION(1, 0, 0)
        actual var engineVersion: Int = VK10.VK_MAKE_VERSION(1, 0, 0)
        actual var apiVersion: Int = VK10.VK_API_VERSION_1_0

        actual val enabledLayers: MutableSet<InstanceLayer> = Collections.synchronizedSet(HashSet())
        actual val enabledExtensions: MutableSet<InstanceExtension> = Collections.synchronizedSet(HashSet())


        actual fun setApplicationVersion(major: Int, minor: Int, patch: Int) {
            APPLICATION_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch)
        }

        actual fun setEngineVersion(major: Int, minor: Int, patch: Int) {
            ENGINE_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch)
        }

        actual fun setAPIVersion(major: Int, minor: Int, patch: Int) {
            API_VERSION = VK10.VK_MAKE_VERSION(major, minor, patch)
        }

        actual fun restoreDefaultLayers() {
            this.enabledLayers.clear()
            this.enabledLayers += DEFAULT_LAYERS
        }

        actual fun restoreExtensions() {
            this.enabledExtensions.clear()
            this.enabledExtensions += DEFAULT_EXTENSIONS
        }

        fun enableRequiredGLFWExtensions() {
            GLFWVulkan.glfwGetRequiredInstanceExtensions()
                    ?.asSequence()
                    ?.map(MemoryUtil::memUTF8Safe)
                    ?.filterNotNull()
                    ?.forEach { this.enabledExtensions += InstanceExtension.of(it) }
        }

        actual val current: Instance by lazy(::Instance)

        private fun debugCallback(flags: Int, objectType: Int, `object`: Long, location: Long, messageCode: Int, pLayerPrefix: Long, pMessage: Long, pUserData: Long): Int {
            when {
                flags and EXTDebugReport.VK_DEBUG_REPORT_ERROR_BIT_EXT != 0 -> LOGGER.error("Vulkan Error: {}", MemoryUtil.memUTF8Safe(pMessage))
                flags and EXTDebugReport.VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT != 0 -> LOGGER.warn("Vulkan Performance Warning: {}", MemoryUtil.memUTF8Safe(pMessage))
                flags and EXTDebugReport.VK_DEBUG_REPORT_WARNING_BIT_EXT != 0 -> LOGGER.warn("Vulkan Warning: {}", MemoryUtil.memUTF8Safe(pMessage))
                flags and EXTDebugReport.VK_DEBUG_REPORT_DEBUG_BIT_EXT != 0 -> LOGGER.debug("Vulkan Debug: {}", MemoryUtil.memUTF8Safe(pMessage))
                flags and EXTDebugReport.VK_DEBUG_REPORT_INFORMATION_BIT_EXT != 0 -> LOGGER.trace("Vulkan CreateInfo: {}", MemoryUtil.memUTF8Safe(pMessage))
            }

            return 0
        }
    }
}
