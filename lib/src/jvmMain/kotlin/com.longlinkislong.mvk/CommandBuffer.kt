package com.longlinkislong.mvk

import org.lwjgl.vulkan.*

import java.lang.ref.WeakReference
import java.nio.ByteBuffer

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.dsl.*
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.address
import com.longlinkislong.mvk.util.stackLet
import com.longlinkislong.mvk.util.applyStack

actual class CommandBuffer internal constructor(
        pool: CommandPool,
        val level: CommandBufferLevel,
        handle: Long) : ManagedResource {

    private val _pool: WeakReference<CommandPool> = WeakReference(pool)

    val handle: VkCommandBuffer = VkCommandBuffer(handle, pool.device.handle)

    actual val commandPool: CommandPool
        get() = _pool.get() ?: throw IllegalStateException("CommandPool was lost!")

    actual fun reset(flags: Int) {
        VK10.vkResetCommandBuffer(this.handle, flags)
    }

    override fun release() = VK10.vkFreeCommandBuffers(this.handle.device, this.commandPool.handle, this.handle)

    actual fun begin(flags: CommandBufferUsageFlags, inheritanceInfo: CommandBufferInheritanceInfo?): CommandBuffer = applyStack { mem ->
        val pvkCommandBufferBI = VkCommandBufferBeginInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
                .flags(CommandBufferUsageFlag.toBitfield(flags))

        if (null != inheritanceInfo) {
            val pvkCommandBufferII = VkCommandBufferInheritanceInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO)
                    .renderPass(inheritanceInfo.renderPass?.handle ?: VK10.VK_NULL_HANDLE)
                    .subpass(inheritanceInfo.subpass)
                    .framebuffer(inheritanceInfo.framebuffer?.handle ?: VK10.VK_NULL_HANDLE)
                    .occlusionQueryEnable(inheritanceInfo.occlusionQueryEnable)
                    .queryFlags(inheritanceInfo.queryFlags)
                    .pipelineStatistics(inheritanceInfo.pipelineStatistics)

            pvkCommandBufferBI.pInheritanceInfo(pvkCommandBufferII)
        }

        vkAssert(VK10.vkBeginCommandBuffer(this.handle, pvkCommandBufferBI))
    }

    actual fun end(): CommandBuffer = apply {
        vkAssert(VK10.vkEndCommandBuffer(this.handle))
    }

    actual fun executeCommands(commands: List<CommandBuffer>): CommandBuffer = applyStack { mem ->
        val pCommandBuffers = mem.callocPointer(commands.size).apply {
            commands.forEach { put(it.handle.address()) }
            flip()
        }

        VK10.vkCmdExecuteCommands(this.handle, pCommandBuffers)
    }

    actual fun beginRenderPass(beginInfo: CommandBufferRenderPassBeginInfo): CommandBuffer = applyStack { mem ->
        val pvkBeginRenderPass = VkRenderPassBeginInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
                .renderPass(beginInfo.renderPass.handle)
                .framebuffer(beginInfo.framebuffer.handle)

        Serialization.deserializeRect2D(beginInfo.renderArea, pvkBeginRenderPass.renderArea())

        val ppClearValues = VkClearValue.callocStack(beginInfo.clearValues.size, mem).apply {
            beginInfo.clearValues.forEachIndexed { index: Int, clearValue: ClearValue ->
                Serialization.deserializeClearValue(clearValue, get(index))
            }
        }

        pvkBeginRenderPass.pClearValues(ppClearValues)

        VK10.vkCmdBeginRenderPass(this.handle, pvkBeginRenderPass, beginInfo.subpassContents.value)
    }

    actual fun nextSubpass(contents: SubpassContents): CommandBuffer = apply {
        VK10.vkCmdNextSubpass(this.handle, contents.value)
    }

    actual fun endRenderPass(): CommandBuffer = apply {
        VK10.vkCmdEndRenderPass(this.handle)
    }

    actual fun setViewport(viewport: Viewport): CommandBuffer = applyStack { mem ->
        val pViewports = VkViewport.callocStack(1, mem).apply { Serialization.deserializeViewport(viewport, this[0]) }

        VK10.vkCmdSetViewport(this.handle, 0, pViewports)
    }

    actual fun setScissor(scissor: Rect2D): CommandBuffer = applyStack { mem ->
        val pScissors = VkRect2D.callocStack(1, mem).apply { Serialization.deserializeRect2D(scissor, this[0]) }

        VK10.vkCmdSetScissor(this.handle, 0, pScissors)
    }

    actual fun bindVertexBuffers(firstBinding: Int, buffers: List<Buffer>, offsets: List<Long>): CommandBuffer = applyStack { mem ->
        val pBuffers = mem.callocLong(buffers.size).apply {
            buffers.forEach { put(it.handle) }
            flip()
        }

        val pOffsets = mem.callocLong(offsets.size).apply {
            offsets.forEach { put(it) }
            flip()
        }

        VK10.vkCmdBindVertexBuffers(this.handle, firstBinding, pBuffers, pOffsets)
    }

    actual fun bindIndexBuffer(buffer: Buffer, offset: Long, indexType: IndexType): CommandBuffer = apply {
        VK10.vkCmdBindIndexBuffer(this.handle, buffer.handle, offset, indexType.value)
    }

    actual fun dispatch(groupsX: Int, groupsY: Int, groupsZ: Int): CommandBuffer = apply {
        VK10.vkCmdDispatch(this.handle, groupsX, groupsY, groupsZ)
    }

    actual fun dispatchIndirect(buffer: Buffer, offset: Long): CommandBuffer = apply {
        VK10.vkCmdDispatchIndirect(this.handle, buffer.handle, offset)
    }

    actual fun draw(vertexCount: Int, instanceCount: Int, firstVertex: Int, firstInstance: Int): CommandBuffer = apply {
        VK10.vkCmdDraw(this.handle, vertexCount, instanceCount, firstVertex, firstInstance)
    }

    actual fun drawIndirect(buffer: Buffer, offset: Long, drawCount: Int, stride: Int): CommandBuffer = apply {
        VK10.vkCmdDrawIndirect(this.handle, buffer.handle, offset, drawCount, stride)
    }

    actual fun drawIndexed(indexCount: Int, instanceCount: Int, firstIndex: Int, vertexOffset: Int, firstInstance: Int): CommandBuffer = apply {
        VK10.vkCmdDrawIndexed(this.handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance)
    }

    actual fun drawIndexedIndirect(buffer: Buffer, offset: Long, drawCount: Int, stride: Int): CommandBuffer = apply {
        VK10.vkCmdDrawIndexedIndirect(this.handle, buffer.handle, offset, drawCount, stride)
    }

    fun updateBuffer(buffer: Buffer, offset: Long, data: ByteBuffer): CommandBuffer = apply {
        VK10.vkCmdUpdateBuffer(this.handle, buffer.handle, offset, data)
    }

    actual fun updateBuffer(buffer: Buffer, offset: Long, dataSize: Long, pData: Long): CommandBuffer = apply {
        VK10.nvkCmdUpdateBuffer(this.handle, buffer.handle, offset, dataSize, pData)
    }

    actual fun pipelineBarrier(
            srcStageMask: PipelineStageFlags, dstStageMask: PipelineStageFlags,
            dependencyFlags: DependencyFlags,
            memoryBarriers: List<MemoryBarrier>,
            bufferMemoryBarriers: List<BufferMemoryBarrier>,
            imageMemoryBarriers: List<ImageMemoryBarrier>): CommandBuffer = applyStack { mem ->

        val pMemoryBarriers: VkMemoryBarrier.Buffer?

        if (memoryBarriers.isEmpty()) {
            pMemoryBarriers = null
        } else {
            pMemoryBarriers = VkMemoryBarrier.callocStack(memoryBarriers.size, mem).apply {
                memoryBarriers.forEachIndexed { index, memoryBarrier -> Serialization.deserializeMemoryBarrier(memoryBarrier, get(index)) }
            }
        }

        val pBufferMemoryBarriers: VkBufferMemoryBarrier.Buffer?

        if (bufferMemoryBarriers.isEmpty()) {
            pBufferMemoryBarriers = null
        } else {
            pBufferMemoryBarriers = VkBufferMemoryBarrier.callocStack(bufferMemoryBarriers.size, mem).apply {
                bufferMemoryBarriers.forEachIndexed { index, bufferMemoryBarrier -> Serialization.deserializeBufferMemoryBarrier(bufferMemoryBarrier, get(index)) }
            }
        }

        val pImageMemoryBarriers: VkImageMemoryBarrier.Buffer?

        if (imageMemoryBarriers.isEmpty()) {
            pImageMemoryBarriers = null
        } else {
            pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(imageMemoryBarriers.size, mem).apply {
                imageMemoryBarriers.forEachIndexed { index, imageMemoryBarrier -> Serialization.deserializeImageMemoryBarrier(imageMemoryBarrier, get(index)) }
            }
        }

        VK10.vkCmdPipelineBarrier(this.handle, srcStageMask.bitfield, dstStageMask.bitfield, dependencyFlags.bitfield, pMemoryBarriers, pBufferMemoryBarriers, pImageMemoryBarriers)
    }

    actual fun bindPipeline(pipeline: Pipeline): CommandBuffer = apply {
        VK10.vkCmdBindPipeline(this.handle, pipeline.bindPoint.value, pipeline.handle)
    }

    actual fun bindDescriptorSets(pipeline: Pipeline, firstSet: Int, descriptorSets: List<DescriptorSet>, dynamicOffsets: List<Int>): CommandBuffer = applyStack { mem ->
        val pDescriptorSets = mem.callocLong(descriptorSets.size).apply {
            descriptorSets.forEach { put(it.handle) }
            flip()
        }

        val pDynamicOffsets = if (dynamicOffsets.isEmpty()) null else mem.callocInt(dynamicOffsets.size).apply {
            dynamicOffsets.forEach { put(it) }
            flip()
        }

        VK10.vkCmdBindDescriptorSets(this.handle, pipeline.bindPoint.value, pipeline.pipelineLayout.handle, firstSet, pDescriptorSets, pDynamicOffsets)
    }

    fun pushConstants(pipeline: Pipeline, stages: ShaderStages, offset: Int, data: ByteBuffer) =
            this.pushConstants(pipeline.pipelineLayout, stages, offset, data.remaining(), data.address)

    fun pushConstants(layout: PipelineLayout, stage: ShaderStages, offset: Int, data: ByteBuffer) =
            this.pushConstants(layout, stage, offset, data.remaining(), data.address)

    actual fun pushConstants(layout: PipelineLayout, stage: ShaderStages, offset: Int, size: Int, pData: Long): CommandBuffer = apply {
        VK10.nvkCmdPushConstants(this.handle, layout.handle, stage.bitfield, offset, size, pData)
    }

    actual fun copyImage(
            src: Image, srcLayout: ImageLayout,
            dst: Image, dstLayout: ImageLayout,
            regions: List<ImageCopy>): CommandBuffer = applyStack { mem ->

        val pRegions = VkImageCopy.callocStack(regions.size, mem).apply {
            regions.forEachIndexed { index, imageCopy -> Serialization.deserializeImageCopy(imageCopy, this[index]) }
        }

        VK10.vkCmdCopyImage(this.handle, src.handle, srcLayout.value, dst.handle, dstLayout.value, pRegions)
    }

    actual fun blitImage(
            src: Image, srcLayout: ImageLayout,
            dst: Image, dstLayout: ImageLayout,
            regions: List<ImageBlit>, filter: Filter): CommandBuffer = applyStack { mem ->

        val pRegions = VkImageBlit.callocStack(regions.size, mem).apply {
            regions.forEachIndexed { index, imageBlit -> Serialization.deserializeImageBlit(imageBlit, this[index]) }
        }

        VK10.vkCmdBlitImage(this.handle, src.handle, srcLayout.value, dst.handle, dstLayout.value, pRegions, filter.value)
    }

    fun stageImage(
            image: Image, oldLayout: ImageLayout, newLayout: ImageLayout,
            srcStageMask: Set<PipelineStageFlag>, dstStageMask: Set<PipelineStageFlag>,
            srcAccess: Set<AccessFlag>, dstAccess: Set<AccessFlag>,
            srcQueueFamily: QueueFamily? = null, dstQueueFamily: QueueFamily? = null) = applyStack { mem ->

        val pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(1, mem)
                .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                .srcAccessMask(srcAccess.bitfield)
                .dstAccessMask(dstAccess.bitfield)
                .oldLayout(oldLayout.value)
                .newLayout(newLayout.value)
                .srcQueueFamilyIndex(QueueFamily.indexOf(srcQueueFamily))
                .dstQueueFamilyIndex(QueueFamily.indexOf(dstQueueFamily))
                .image(image.handle)

        Serialization.deserializeImageSubresourceRange(image.fullRange, pImageMemoryBarriers.subresourceRange())

        VK10.vkCmdPipelineBarrier(this.handle, srcStageMask.bitfield, dstStageMask.bitfield, 0, null, null, pImageMemoryBarriers)
    }

    actual fun generateMipmaps(image: Image, filter: Filter): CommandBuffer = apply {
        val aspectMask = AspectFlag.toBitfield(image.info.format.aspect)
        val width = image.info.extent.width
        val height = image.info.extent.height
        val layerCount = image.info.arrayLayers

        for (i in 1 until image.info.mipLevels) {
            stackLet { mem ->
                val pImageBlit = VkImageBlit.callocStack(1, mem)

                pImageBlit.srcSubresource().set(aspectMask, i - 1, 0, layerCount)
                pImageBlit.srcOffsets(1).set(width shr i - 1, height shr i - 1, 1)
                pImageBlit.dstSubresource().set(aspectMask, i, 0, layerCount)
                pImageBlit.dstOffsets(1).set(width shr i, height shr i, 1)

                val pMipSubRange = VkImageSubresourceRange.callocStack(mem)

                pMipSubRange.aspectMask(aspectMask)
                pMipSubRange.baseMipLevel(i)
                pMipSubRange.levelCount(1)
                pMipSubRange.layerCount(layerCount)

                val pInitialMemoryBarrier = VkImageMemoryBarrier.callocStack(1, mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                        .srcAccessMask(0)
                        .dstAccessMask(VK10.VK_ACCESS_TRANSFER_WRITE_BIT)
                        .oldLayout(VK10.VK_IMAGE_LAYOUT_UNDEFINED)
                        .newLayout(VK10.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
                        .image(image.handle)
                        .subresourceRange(pMipSubRange)

                val pFinalMemoryBarrier = VkImageMemoryBarrier.callocStack(1, mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                        .srcAccessMask(VK10.VK_ACCESS_TRANSFER_WRITE_BIT)
                        .dstAccessMask(VK10.VK_ACCESS_TRANSFER_READ_BIT)
                        .oldLayout(VK10.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
                        .newLayout(VK10.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
                        .image(image.handle)
                        .subresourceRange(pMipSubRange)

                VK10.vkCmdPipelineBarrier(this.handle, VK10.VK_PIPELINE_STAGE_TRANSFER_BIT, VK10.VK_PIPELINE_STAGE_TRANSFER_BIT, 0, null, null, pInitialMemoryBarrier)
                VK10.vkCmdBlitImage(this.handle, image.handle, VK10.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image.handle, VK10.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, pImageBlit, filter.value)
                VK10.vkCmdPipelineBarrier(this.handle, VK10.VK_PIPELINE_STAGE_TRANSFER_BIT, VK10.VK_PIPELINE_STAGE_TRANSFER_BIT, 0, null, null, pFinalMemoryBarrier)
            }
        }
    }

    actual fun copyBufferToImage(src: Buffer, dst: Image, layout: ImageLayout, regions: List<BufferImageCopy>): CommandBuffer = applyStack { mem ->
        val pRegions = VkBufferImageCopy.callocStack(regions.size, mem).apply {
            regions.forEachIndexed { index, bufferImageCopy -> Serialization.deserializeBufferImageCopy(bufferImageCopy, get(index)) }
        }

        VK10.vkCmdCopyBufferToImage(this.handle, src.handle, dst.handle, layout.value, pRegions)
    }

    actual fun copyBuffer(src: Buffer, dst: Buffer, regions: List<BufferCopy>): CommandBuffer = applyStack { mem ->
        val pRegions = VkBufferCopy.callocStack(regions.size, mem).apply {
            regions.forEachIndexed { index, bufferCopy -> get(index).set(bufferCopy.srcOffset, bufferCopy.dstOffset, bufferCopy.size) }
        }

        VK10.vkCmdCopyBuffer(this.handle, src.handle, dst.handle, pRegions)
    }

    actual fun fillBuffer(buffer: Buffer, offset: Long, size: Long, data: Int): CommandBuffer = apply {
        VK10.vkCmdFillBuffer(this.handle, buffer.handle, offset, size, data)
    }
}
