package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.getValue
import java.lang.ref.WeakReference

internal class SamplerCache internal constructor(device: Device) {

    private val _samplers = HashMap<SamplerCreateInfo, SamplerInstance>()
    private val _lock = Any()

    val device: Device by WeakReference(device)

    private data class SamplerInstance(val instance: Sampler, var references: Int = 0)

    fun allocate(createInfo: SamplerCreateInfo): Sampler = synchronized(this._lock) {
        val sampler = this._samplers.computeIfAbsent(createInfo) { SamplerInstance(Sampler(this, it)) }

        sampler.references += 1

        sampler.instance
    }

    fun release(sampler: Sampler) = synchronized(this._lock) {
        assert(sampler.samplerCache == this)

        val s = this._samplers[sampler.info] ?: throw IllegalStateException("Sampler does not belong to SamplerCache!")

        s.references -= 1

        if (0 == s.references) {
            s.instance.free()
            this._samplers.remove(sampler.info)
        }
    }

    internal fun free() = synchronized(this._lock) {
        this._samplers.values.forEach { sampler -> sampler.instance.free() }
        this._samplers.clear()
    }
}
