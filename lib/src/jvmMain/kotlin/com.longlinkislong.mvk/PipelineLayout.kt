package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkPipelineLayoutCreateInfo
import org.lwjgl.vulkan.VkPushConstantRange
import java.lang.ref.WeakReference

actual class PipelineLayout internal constructor(
        cache: PipelineLayoutCache,
        actual val info: PipelineLayoutCreateInfo) : ManagedResource, Handle {

    private val _setLayouts: MutableList<WeakReference<DescriptorSetLayout>>

    internal val pipelineLayoutCache: PipelineLayoutCache by WeakReference(cache)

    override val handle: Long

    actual val descriptorSetLayouts: List<DescriptorSetLayout>
        get() = this._setLayouts.map { it.get() ?: throw IllegalStateException("DescriptorSetLayout was lost!") }

    actual val device: Device
        get() = this.pipelineLayoutCache.device

    init {
        val setLayouts = info.setLayoutInfos.asSequence()
                .map { device.descriptorSetLayoutCache.allocateDescriptorSetLayout(it) }
                .toList()

        this._setLayouts = setLayouts.asSequence()
                .map { WeakReference(it) }
                .toMutableList()

        this.handle = stackLet { mem ->
            val pPushConstantRanges: VkPushConstantRange.Buffer? = if (info.pushConstantRanges.isEmpty()) {
                null
            } else {
                VkPushConstantRange.callocStack(info.pushConstantRanges.size, mem).apply {
                    info.pushConstantRanges.forEachIndexed { id, range -> Serialization.deserializePushConstantRange(range, get(id)) }
                }
            }

            val pSetLayouts = mem.callocLong(setLayouts.size).apply {
                setLayouts.forEach { put(it.handle) }
                flip()
            }

            val pvkPipelineLayoutCI = VkPipelineLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
                    .flags(info.flags)
                    .pSetLayouts(pSetLayouts)
                    .pPushConstantRanges(pPushConstantRanges)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreatePipelineLayout(device.handle, pvkPipelineLayoutCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun release() = this.pipelineLayoutCache.releasePipelineLayout(this)

    internal fun free() {
        VK10.vkDestroyPipelineLayout(this.device.handle, this.handle, null)

        this._setLayouts.forEach { it.get()?.release() }
        this._setLayouts.clear()
    }
}
