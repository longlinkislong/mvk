package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkSamplerCreateInfo
import java.lang.ref.WeakReference

actual class Sampler internal constructor(
        cache: SamplerCache,
        actual val info: SamplerCreateInfo) : ManagedResource, Handle {

    internal val samplerCache: SamplerCache by WeakReference(cache)

    actual val device: Device
        get() = this.samplerCache.device

    override val handle: Long

    init {
        this.handle = stackLet { mem ->
            val device = cache.device

            val pvkSamplerCI = VkSamplerCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
                    .magFilter(info.magFilter.value)
                    .minFilter(info.minFilter.value)
                    .mipmapMode(info.mipmapFilter.value)
                    .addressModeU(info.addressModeU.value)
                    .addressModeV(info.addressModeV.value)
                    .addressModeW(info.addressModeW.value)
                    .mipLodBias(info.mipLodBias)
                    .anisotropyEnable(info.anistropyEnable)
                    .maxAnisotropy(info.maxAnisotropy)
                    .compareEnable(info.compareEnable)
                    .compareOp(info.compareOp.value)
                    .minLod(info.minLod)
                    .maxLod(info.maxLod)
                    .borderColor(info.borderColor.value)
                    .unnormalizedCoordinates(info.unnormalizedCoordinates)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateSampler(device.handle, pvkSamplerCI, null, pHandle))

            pHandle[0]
        }
    }

    internal fun free() = VK10.vkDestroySampler(this.device.handle, this.handle, null)

    override fun release() = this.samplerCache.release(this)
}
