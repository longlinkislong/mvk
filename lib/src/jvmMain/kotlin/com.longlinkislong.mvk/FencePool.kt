package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkFenceCreateInfo
import java.lang.ref.WeakReference
import java.util.*

internal class FencePool internal constructor(device: Device) : Resource {
    private val _lock = Any()
    private val _allFences = HashSet<Fence>()
    private val _availableFences = ArrayDeque<Fence>()

    val device: Device by WeakReference(device)

    private fun allocateFence(): Fence = stackLet { mem ->
        val pvkFenceCI = VkFenceCreateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)

        val pHandle = mem.callocLong(1)

        vkAssert(VK10.vkCreateFence(this.device.handle, pvkFenceCI, null, pHandle))

        Fence(this, pHandle[0]).also {
            this._allFences.add(it)
        }
    }

    fun acquireFence(): Fence = synchronized(_lock) {
        if (this._availableFences.isEmpty()) {
            this.allocateFence()
        } else {
            this._availableFences.poll()
        }
    }

    fun releaseFence(fence: Fence) = synchronized(_lock) {
        assert(this._allFences.contains(fence))

        this._availableFences.offer(fence)
        Unit
    }

    override fun free() = synchronized(_lock) {
        this._allFences.forEach { fence -> VK10.vkDestroyFence(device.handle, fence.handle, null) }
        this._allFences.clear()
        this._availableFences.clear()
    }
}
