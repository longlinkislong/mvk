package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDescriptorPoolCreateInfo
import org.lwjgl.vulkan.VkDescriptorSetAllocateInfo
import java.lang.ref.WeakReference

internal class DescriptorPool(
        val info: DescriptorPoolCreateInfo,
        descriptorSetLayout: DescriptorSetLayout) : Resource {

    private val _lock = Any()
    private val _pools = ArrayList<Pool>()
    private val _allocatedDescriptorSets = HashSet<DescriptorSet>()
    private val _descriptorSetLayout: DescriptorSetLayout by WeakReference(descriptorSetLayout)

    val device: Device
        get() = this._descriptorSetLayout.device

    private data class Pool internal constructor(val handle: Long, val index: Int, var allocatedSets: Int = 0)

    internal fun releaseDescriptorSet(set: DescriptorSet) = synchronized(_lock) {
        assert(this._allocatedDescriptorSets.contains(set))

        val pool = this._pools[set.poolIndex]

        VK10.vkFreeDescriptorSets(this.device.handle, pool.handle, set.handle)

        pool.allocatedSets -= 1

        this._allocatedDescriptorSets.remove(set)
        Unit
    }

    private fun allocatePool(device: Device): Pool = stackLet { mem ->
        val pvkDescriptorPoolSizes = Serialization.deserializeDescriptorPoolSizes(this.info.poolSizes, mem)
        val pvkDescriptorPoolCI = VkDescriptorPoolCreateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO)
                .flags(info.flags or VK10.VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT)
                .pPoolSizes(pvkDescriptorPoolSizes)
                .maxSets(info.maxSets)

        val pHandle = mem.callocLong(1)

        vkAssert(VK10.vkCreateDescriptorPool(device.handle, pvkDescriptorPoolCI, null, pHandle))

        val index = this._pools.size

        Pool(pHandle[0], index).also {
            this._pools.add(it)
        }
    }

    fun allocate(): DescriptorSet = stackLet { mem ->
        val pvkDescriptorSetAI = VkDescriptorSetAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                .pSetLayouts(mem.longs(_descriptorSetLayout.handle))

        val pHandle = mem.callocLong(1)

        synchronized(_lock) {
            val selectedPool = this._pools
                    .firstOrNull { pool -> pool.allocatedSets < this.info.maxSets }
                    ?: run { this.allocatePool(device) }

            pvkDescriptorSetAI.descriptorPool(selectedPool.handle)

            vkAssert(VK10.vkAllocateDescriptorSets(device.handle, pvkDescriptorSetAI, pHandle))

            selectedPool.allocatedSets += 1

            DescriptorSet(this, selectedPool.index, pHandle[0]).also {
                this._allocatedDescriptorSets.add(it)
            }
        }
    }

    fun allocate(nSets: Int): List<DescriptorSet> = stackLet { mem ->
        val pvkDescriptorSetAI = VkDescriptorSetAllocateInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                .pSetLayouts(mem.longs(this._descriptorSetLayout.handle))

        val pHandle = mem.callocLong(1)
        val out = ArrayList<DescriptorSet>()

        synchronized(_lock) {
            while (out.size < nSets) {
                this._pools
                        .firstOrNull { pool -> pool.allocatedSets < this.info.maxSets }
                        ?.let { pool ->
                            pvkDescriptorSetAI.descriptorPool(pool.handle)

                            pHandle.clear()
                            vkAssert(VK10.vkAllocateDescriptorSets(device.handle, pvkDescriptorSetAI, pHandle))

                            val set = DescriptorSet(this, pool.index, pHandle[0])

                            this._allocatedDescriptorSets.add(set)

                            out.add(set)

                            pool.allocatedSets += 1
                        }
                        ?: run {
                            this.allocatePool(device)
                        }
            }
        }

        out
    }

    override fun free() = synchronized(_lock) {
        this._allocatedDescriptorSets.forEach { set ->
            val pool = this._pools[set.poolIndex]

            VK10.vkFreeDescriptorSets(device.handle, pool.handle, set.handle)
        }

        this._allocatedDescriptorSets.clear()
        this._pools.forEach { pool -> VK10.vkDestroyDescriptorPool(device.handle, pool.handle, null) }
        this._pools.clear()
    }
}
