package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkComputePipelineCreateInfo
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo
import java.lang.ref.WeakReference

actual class ComputePipeline internal constructor(
        cache: PipelineCache,
        actual val info: ComputePipelineCreateInfo) : Pipeline {

    private val pipelineCache: PipelineCache by WeakReference(cache)

    override val bindPoint: PipelineBindPoint = PipelineBindPoint.COMPUTE
    override val pipelineLayout: PipelineLayout by WeakReference(cache.device.pipelineLayoutCache.allocatePipelineLayout(info.layoutInfo))
    override val handle: Long

    override val descriptorSetLayouts: List<DescriptorSetLayout>
        get() = this.pipelineLayout.descriptorSetLayouts

    override val device: Device
        get() = this.pipelineCache.device

    init {
        this.handle = stackLet { mem ->
            val device = cache.device

            val pvkComputePipelineCI = VkComputePipelineCreateInfo.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO)
                    .flags(info.flags)
                    .layout(pipelineLayout.handle)
                    .stage(Serialization.deserializePipelineShaderStageCreateInfo(info.stage, VkPipelineShaderStageCreateInfo.callocStack(mem), device, mem))

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateComputePipelines(device.handle, cache.handle, pvkComputePipelineCI, null, pHandle))

            pHandle[0]
        }
    }

    internal fun free() {
        this.pipelineLayout.release()
        VK10.vkDestroyPipeline(this.device.handle, this.handle, null)
    }

    override fun release() = this.pipelineCache.release(this)
}
