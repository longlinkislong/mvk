package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference

actual class Queue internal constructor(
        device: Device,
        private val _queueFamilyIndex: Int,
        actual val queueIndex: Int) {

    val handle: VkQueue

    actual val device: Device by WeakReference(device)

    actual val queueFamily: QueueFamily
        get() = this.device.queueFamilies[this._queueFamilyIndex]

    init {
        this.handle = stackLet { mem ->
            val pHandle = mem.callocPointer(1)

            VK10.vkGetDeviceQueue(device.handle, _queueFamilyIndex, queueIndex, pHandle)

            VkQueue(pHandle[0], device.handle)
        }
    }

    actual fun waitIdle() = vkAssert(VK10.vkQueueWaitIdle(this.handle))

    actual fun submit(submitInfo: QueueSubmitInfo, fence: Fence?) = stackLet { mem ->
        val pCommandBuffers = mem.callocPointer(submitInfo.commandBuffers.size).apply {
            submitInfo.commandBuffers.forEach { put(it.handle) }
            flip()
        }

        val pWaitSemaphores = mem.callocLong(submitInfo.waitInfos.size).apply {
            submitInfo.waitInfos.forEach { put(it.semaphore.handle) }
            flip()
        }

        val pWaitDstStageMask = mem.callocInt(submitInfo.waitInfos.size).apply {
            submitInfo.waitInfos.forEach { put(PipelineStageFlag.toBitfield(it.stageFlags)) }
            flip()
        }

        val pSignalSemaphores = mem.callocLong(submitInfo.signalSemaphores.size).apply {
            submitInfo.signalSemaphores.forEach { put(it.handle) }
            flip()
        }

        val pvkSubmitInfo = VkSubmitInfo.callocStack(mem)
                .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                .waitSemaphoreCount(submitInfo.waitInfos.size)
                .pWaitDstStageMask(pWaitDstStageMask)
                .pWaitSemaphores(pWaitSemaphores)
                .pSignalSemaphores(pSignalSemaphores)
                .pCommandBuffers(pCommandBuffers)

        vkAssert(VK10.vkQueueSubmit(this.handle, pvkSubmitInfo, fence?.handle ?: VK10.VK_NULL_HANDLE))
    }

    actual fun submit(submitInfos: List<QueueSubmitInfo>, fence: Fence?) = stackLet { mem ->
        val pvkSubmitInfos = VkSubmitInfo.callocStack(submitInfos.size, mem)

        submitInfos.forEachIndexed { index, info ->
            val pCommandBuffers = mem.callocPointer(info.commandBuffers.size).apply {
                info.commandBuffers.forEach { put(it.handle) }
                flip()
            }

            val pWaitSemaphores = mem.callocLong(info.waitInfos.size).apply {
                info.waitInfos.forEach { put(it.semaphore.handle) }
                flip()
            }

            val pWaitDstStageMask = mem.callocInt(info.waitInfos.size).apply {
                info.waitInfos.forEach { put(PipelineStageFlag.toBitfield(it.stageFlags)) }
                flip()
            }

            val pSignalSemaphores = mem.callocLong(info.signalSemaphores.size).apply {
                info.signalSemaphores.forEach { put(it.handle) }
                flip()
            }

            pvkSubmitInfos[index]
                    .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                    .waitSemaphoreCount(info.waitInfos.size)
                    .pWaitDstStageMask(pWaitDstStageMask)
                    .pWaitSemaphores(pWaitSemaphores)
                    .pSignalSemaphores(pSignalSemaphores)
                    .pCommandBuffers(pCommandBuffers)
        }

        vkAssert(VK10.vkQueueSubmit(this.handle, pvkSubmitInfos, fence?.handle ?: VK10.VK_NULL_HANDLE))
    }

    actual fun present(info: QueuePresentInfo) {
        stackLet { mem ->
            val pSwapchains = mem.callocLong(info.swapchains.size).apply {
                info.swapchains.forEach { put(it.handle ?: throw IllegalStateException("Invalid Swapchain!")) }
                flip()
            }

            val pIndices = mem.callocInt(info.imageIndices.size).apply {
                info.imageIndices.forEach { put(it) }
                flip()
            }

            val pWaitSemaphores = if (info.waitSemaphores.isNotEmpty()) mem.callocLong(info.waitSemaphores.size).apply {
                info.waitSemaphores.forEach { put(it.handle) }
                flip()
            } else null

            val pvkPresentInfo = VkPresentInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
                    .pWaitSemaphores(pWaitSemaphores)
                    .swapchainCount(info.swapchains.size)
                    .pSwapchains(pSwapchains)
                    .pImageIndices(pIndices)

            KHRSwapchain.vkQueuePresentKHR(this.handle, pvkPresentInfo)
        }
    }
}