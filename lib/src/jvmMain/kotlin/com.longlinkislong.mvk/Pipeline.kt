package com.longlinkislong.mvk

actual interface Pipeline : ManagedResource, Handle {
    override val handle: Long

    actual val bindPoint: PipelineBindPoint

    actual val pipelineLayout: PipelineLayout

    actual val device: Device

    actual val descriptorSetLayouts: List<DescriptorSetLayout>

    override fun release()
}
