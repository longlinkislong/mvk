package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkRenderPassCreateInfo
import java.lang.ref.WeakReference

actual class RenderPass(
        device: Device,
        actual val info: RenderPassCreateInfo) : Resource, Handle {

    actual val device: Device by WeakReference(device)

    override val handle: Long

    actual var userData: Any? = null

    init {
        this.handle = stackLet { mem ->
            val pvkRenderPassCI = VkRenderPassCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
                    .flags(info.flags)
                    .pAttachments(Serialization.deserializeAttachmentDescriptions(info.attachments, mem))
                    .pSubpasses(Serialization.deserializeSubpassDescriptions(info.subpasses, mem))
                    .pDependencies(Serialization.deserializeSubpassDependencies(info.dependencies, mem))

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateRenderPass(device.handle, pvkRenderPassCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun free() = VK10.vkDestroyRenderPass(this.device.handle, this.handle, null)

}
