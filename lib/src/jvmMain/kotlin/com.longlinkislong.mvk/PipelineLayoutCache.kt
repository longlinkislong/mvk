package com.longlinkislong.mvk

import com.longlinkislong.mvk.util.getValue
import java.lang.ref.WeakReference

internal class PipelineLayoutCache internal constructor(device: Device) : Resource {
    private val _lock = Any()
    private val _layouts = HashMap<PipelineLayoutCreateInfo, Layout>()

    val device: Device by WeakReference(device)

    private data class Layout(val instance: PipelineLayout, var references: Int = 0)

    override fun free() = synchronized(_lock) {
        this._layouts.values.forEach { layout -> layout.instance.free() }
        this._layouts.clear()
    }

    fun allocatePipelineLayout(createInfo: PipelineLayoutCreateInfo): PipelineLayout = synchronized(_lock) {
        val layout = this._layouts.computeIfAbsent(createInfo) { Layout(PipelineLayout(this, it)) }

        layout.references += 1

        layout.instance
    }

    fun releasePipelineLayout(layout: PipelineLayout) = synchronized(_lock) {
        assert(layout.pipelineLayoutCache == this)

        val pl = this._layouts[layout.info] ?: throw IllegalStateException("PipelineLayout does not belong to PipelineLayoutCache!")

        pl.references -= 1

        if (0 == pl.references) {
            layout.free()
            this._layouts.remove(layout.info)
        }
    }
}
