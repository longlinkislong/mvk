package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkShaderModuleCreateInfo
import java.lang.ref.WeakReference

class ShaderModule internal constructor(
        device: Device,
        val info: ShaderModuleCreateInfo) : Resource, Handle {

    val device: Device by WeakReference(device)

    override val handle: Long

    init {
        this.handle = stackLet { mem ->
            val pvkShaderModuleCI = VkShaderModuleCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
                    .flags(info.flags)
                    .pCode(MemoryUtil.memByteBuffer(info.pData, info.size))

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateShaderModule(device.handle, pvkShaderModuleCI, null, pHandle))

            pHandle[0]
        }
    }

    override fun free() = VK10.vkDestroyShaderModule(this.device.handle, this.handle, null)
}
