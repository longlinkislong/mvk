package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.VK10
import org.lwjgl.vulkan.VkDescriptorSetLayoutCreateInfo
import java.lang.ref.WeakReference

actual class DescriptorSetLayout internal constructor(
        cache: DescriptorSetLayoutCache,
        actual val info: DescriptorSetLayoutCreateInfo) : ManagedResource, Handle {

    private val pool: DescriptorPool

    internal val descriporSetLayoutCache: DescriptorSetLayoutCache by WeakReference(cache)

    override val handle: Long

    actual val device: Device
        get() = this.descriporSetLayoutCache.device

    init {
        val device = cache.device

        this.handle = stackLet { mem ->
            val pvkDescriptorSetCI = VkDescriptorSetLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO)
                    .flags(info.flags)
                    .pBindings(Serialization.deserializeDescriptorSetLayoutBindings(info.bindings, mem))

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateDescriptorSetLayout(device.handle, pvkDescriptorSetCI, null, pHandle))

            pHandle[0]
        }

        this.pool = run {
            val poolSizes = HashMap<DescriptorType, Int>()

            info.bindings.forEach { binding ->
                val count = poolSizes[binding.descriptorType] ?: 0

                poolSizes[binding.descriptorType] = count + binding.descriptorCount
            }

            val poolCI = DescriptorPoolCreateInfo(
                    maxSets = MAX_SETS,
                    poolSizes = poolSizes.entries.asSequence()
                            .map { DescriptorPoolSize(it.key.value, it.value * MAX_SETS) }
                            .toList())

            DescriptorPool(poolCI, this)
        }
    }

    actual fun allocate(): DescriptorSet = this.pool.allocate()

    actual fun allocate(nSets: Int): List<DescriptorSet> = this.pool.allocate(nSets)

    internal fun free() {
        this.pool.free()
        VK10.vkDestroyDescriptorSetLayout(this.device.handle, this.handle, null)
    }

    override fun release() = this.descriporSetLayoutCache.releaseDescriptorSetLayout(this)

    companion object {
        private const val MAX_SETS = 32
    }
}
