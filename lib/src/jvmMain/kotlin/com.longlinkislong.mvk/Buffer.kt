package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.memory.MemoryBlock
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.system.MemoryUtil
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference
import java.nio.ByteBuffer

actual class Buffer internal constructor(
        device: Device,
        actual val info: BufferCreateInfo,
        memoryProperties: Int) : Resource, ExternalHandle {

    actual val device: Device by WeakReference(device)
    actual var userData: Any? = null

    override val fd: Int by lazy {
        stackLet { mem ->
            val pvkMemoryGetFdInfo = VkMemoryGetFdInfoKHR.callocStack(mem)
                    .sType(KHRExternalMemoryFd.VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR)
                    .memory(this.memory.handle)
                    .handleType(this.info.handleType!!.value)

            val pFd = mem.callocInt(1)

            vkAssert(KHRExternalMemoryFd.vkGetMemoryFdKHR(this.device.handle, pvkMemoryGetFdInfo, pFd))

            pFd[0]
        }
    }

    override val win32Handle: Long by lazy {
        stackLet { mem ->
            val pvkMemoryGetWin32Info = VkMemoryGetWin32HandleInfoKHR.callocStack(mem)
                    .sType(KHRExternalMemoryWin32.VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR)
                    .memory(this.memory.handle)
                    .handleType(this.info.handleType!!.value)

            val pHandle = mem.callocPointer(1)

            KHRExternalMemoryWin32.vkGetMemoryWin32HandleKHR(this.device.handle, pvkMemoryGetWin32Info, pHandle)

            pHandle[0]
        }
    }

    override val handle: Long

    val memory: MemoryBlock

    init {
        this.handle = stackLet { mem ->
            val pQueueFamilyIndices = mem.callocInt(info.queueFamilies.size).apply {
                info.queueFamilies.forEach { put(it.info.index) }
                flip()
            }

            val pvkBufferCI = VkBufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
                    .flags(info.flags)
                    .size(info.size)
                    .usage(info.usage.bitfield)
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndices)

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateBuffer(device.handle, pvkBufferCI, null, pHandle))

            pHandle[0]
        }

        this.memory = stackLet { mem ->
            val pvkMemoryRequirements = VkMemoryRequirements.callocStack(mem)

            VK10.vkGetBufferMemoryRequirements(device.handle, this.handle, pvkMemoryRequirements)

            if (null == info.handleType) {
                device.memoryManager.allocateBufferMemory(pvkMemoryRequirements, memoryProperties).apply {
                    this.bindToBuffer(this@Buffer.handle)
                }
            } else {
                device.memoryManager.allocateExportableBufferMemory(pvkMemoryRequirements, memoryProperties, info.handleType.value).apply {
                    this.bindToBuffer(this@Buffer.handle)
                }
            }
        }
    }

    actual fun mapAddress(): Long = MemoryUtil.memAddress(this.map())

    actual fun unmap() = this.memory.unmap()

    /**
     * Maps the Buffer and returns the mapped memory as a ByteBuffer.
     * @return the mapped memory
     */
    fun map(): ByteBuffer = this.memory.map()

    override fun free() {
        VK10.vkDestroyBuffer(this.device.handle, this.handle, null)
        this.memory.free()
    }
}
