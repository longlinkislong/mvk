package com.longlinkislong.mvk

import com.longlinkislong.mvk.Util.vkAssert
import com.longlinkislong.mvk.util.Serialization
import com.longlinkislong.mvk.util.getValue
import com.longlinkislong.mvk.util.stackLet
import org.lwjgl.vulkan.*
import java.lang.ref.WeakReference

actual class GraphicsPipeline internal constructor(
        cache: PipelineCache,
        actual val info: GraphicsPipelineCreateInfo,
        renderPass: RenderPass) : Pipeline {

    private val _layout: WeakReference<PipelineLayout>

    private val pipelineCache: PipelineCache by WeakReference(cache)

    override val bindPoint: PipelineBindPoint = PipelineBindPoint.GRAPHICS
    override val handle: Long

    override val device: Device
        get() = this.pipelineCache.device

    override val pipelineLayout: PipelineLayout
        get() = this._layout.get() ?: throw IllegalStateException("PipelineLayout was lost!")

    override val descriptorSetLayouts: List<DescriptorSetLayout>
        get() = this.pipelineLayout.descriptorSetLayouts

    init {
        val layout = device.pipelineLayoutCache.allocatePipelineLayout(info.layoutInfo)

        this._layout = WeakReference(layout)

        this.handle = stackLet { mem ->
            val pStages = VkPipelineShaderStageCreateInfo.callocStack(info.stages.size, mem)

            info.stages.forEachIndexed { index, stage -> Serialization.deserializePipelineShaderStageCreateInfo(stage, pStages[index], device, mem) }

            val pDynamicState = VkPipelineDynamicStateCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO)
                    .pDynamicStates(mem.ints(VK10.VK_DYNAMIC_STATE_SCISSOR, VK10.VK_DYNAMIC_STATE_VIEWPORT))

            val pInputAssemblyState = Serialization.deserializePipelineInputAssemblyStateCreateInfo(info.inputAssemblyState, VkPipelineInputAssemblyStateCreateInfo.callocStack(mem))

            val pViewportState = VkPipelineViewportStateCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
                    .viewportCount(1)
                    .scissorCount(1)

            val pMultisampleState = Serialization.deserializePipelineMultisampleStateCreateInfo(info.multisampleState, VkPipelineMultisampleStateCreateInfo.callocStack(mem))

            val pRasterizationState = Serialization.deserializePipelineRasterizationStateCreateInfo(info.rasterizationState, VkPipelineRasterizationStateCreateInfo.callocStack(mem))

            val pDepthStencilState = Serialization.deserializePipelineDepthStencilStateCreateInfo(info.depthStencilState, VkPipelineDepthStencilStateCreateInfo.callocStack(mem))

            val pVertexInputState = Serialization.DeserializePipelineVertexInputStateCreateInfo(info.vertexInputState, VkPipelineVertexInputStateCreateInfo.callocStack(mem), mem)

            val pvkGraphicsPipelineCI = VkGraphicsPipelineCreateInfo.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
                    .layout(layout.handle)
                    .pMultisampleState(pMultisampleState)
                    .pRasterizationState(pRasterizationState)
                    .pDepthStencilState(pDepthStencilState)
                    .pDynamicState(pDynamicState)
                    .pInputAssemblyState(pInputAssemblyState)
                    .pVertexInputState(pVertexInputState)
                    .pStages(pStages)
                    .pViewportState(pViewportState)
                    .subpass(info.subpass)
                    .renderPass(renderPass.handle)

            if (null != info.colorBlendState) {
                val pColorBlendState = Serialization.deserializePipelineColorBlendStateCreateInfo(info.colorBlendState, VkPipelineColorBlendStateCreateInfo.callocStack(mem), mem)

                pvkGraphicsPipelineCI.pColorBlendState(pColorBlendState)
            }

            if (null != info.tessellationState) {
                val pTessellationState = Serialization.deserializePipelineTessellationStateCreateInfo(info.tessellationState, VkPipelineTessellationStateCreateInfo.callocStack(mem))

                pvkGraphicsPipelineCI.pTessellationState(pTessellationState)
            }

            val pHandle = mem.callocLong(1)

            vkAssert(VK10.vkCreateGraphicsPipelines(device.handle, cache.handle, pvkGraphicsPipelineCI, null, pHandle))

            pHandle[0]
        }
    }

    internal fun free() = VK10.vkDestroyPipeline(this.device.handle, this.handle, null)

    override fun release() = this.pipelineCache.release(this)
}
